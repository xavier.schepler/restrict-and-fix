#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "rf/algo/RfRNF.hpp"
#include "rf/model/RfMod.hpp"
#include "rf/solver/RfMPS.hpp"

using namespace Rf::Model;
using namespace Rf::Algo;
using namespace Rf::Solver;
using std::string;
using std::to_string;

void usage()
{
  std::cout << "Usage: ./ls (solver|relax|restrict) instance [time window]" << std::endl;
  std::cout << "With relax or restrict time window must be given." << std::endl;
}

class InstanceLS
{
 public:
  int N;     // Product count
  int T;     // Period count
  RfNumM C;  // Capacity
  RfNumM l;  // Capacity loss when setting up
  RfNumM p;  // Production cost
  RfNumM f;  // Setup cost
  RfNumM h;  // Holding cost
  RfNumM d;  // Demand

 public:
  InstanceLS(std::string const& fn)
  {
    std::ifstream ifs(fn);
    if (ifs.fail())
    {
      usage();
      throw std::invalid_argument("couldn't open instance file");
    }
    std::cout << "Reading data";
    ifs >> N >> T;
    std::cout << ".";
    parseV(ifs, C);
    RfNumM MS[] = { C, l, p, f, h, d };
    for (auto M : MS)
    {
      std::cout << ".";
      parseM(ifs, M);
    }
    std::cout << std::endl;
    std::cout << "d_{1, i, t}" << std::endl;
    for (int i = 0; i < N; i++)
      for (int t = 0; t < T; t++)
      {
        int n(0);
        for (int tau = 0; tau <= t; tau++)
          n += d[{ i, tau }];
        d[{ 1, i, t }] = n;
      }
  }

 private:
  void parseM(std::ifstream& ifs, RfNumM M)
  {
    for (int i = 0; i < N; i++)
      for (int t = 0; t < T; t++)
      {
        double v;
        ifs >> v;
        M[{ i, t }] = v;
      }
  }
  void parseV(std::ifstream& ifs, RfNumM M)
  {
    for (int t = 0; t < T; t++)
    {
      double v;
      ifs >> v;
      M[{ t }] = v;
    }
  }
};

RfMod createMod(RfVarM X, RfVarM Y, RfConM C1, RfConM C2, RfConM C3, InstanceLS& I)
{
  std::cout << "Creating model" << std::endl;
  std::cout << "X" << std::endl;
  // X
  for (int i = 0; i < I.N; i++)
    for (int t = 0; t < I.T; t++)
    {
      string n = string("X_") + to_string(i) + string(",") + to_string(t);
      X[{ i, t }] = RfVar(n, RfReal, 0);
    }
  std::cout << "Y" << std::endl;
  for (int i = 0; i < I.N; i++)
    for (int t = 0; t < I.T; t++)
    {
      string n = string("Y_") + to_string(i) + string(",") + to_string(t);
      Y[{ i, t }] = RfVar(n, RfBin);
    }
  std::cout << "C1" << std::endl;
  for (int t = 0; t < I.T; t++)
  {
    RfExpLin e;
    for (int i = 0; i < I.N; i++)
      e += X[{ i, t }] + I.l[{ i, t }] * Y[{ i, t }];
    C1[{ t }] = e <= I.C[{ t }];
  }
  std::cout << "C2" << std::endl;
  for (int i = 0; i < I.N; i++)
    for (int t = 0; t < I.T; t++)
    {
      RfExpLin e;
      for (int tau = 0; tau <= t; tau++)
        e += X[{ i, tau }];
      C2[{ i, t }] = e >= I.d[{ 1, i, t }];
    }
  std::cout << "C3" << std::endl;
  for (int i = 0; i < I.N; i++)
    for (int t = 0; t < I.T; t++)
      C3[{ i, t }] = X[{ i, t }] <= (I.C[{ i, t }] - I.l[{ i, t }]) * Y[{ i, t }];
  std::cout << "Objective" << std::endl;
  RfExpLin o;
  for (int i = 0; i < I.N; i++)
    for (int t = 0; t < I.T; t++)
    {
      o += I.p[{ i, t }] * X[{ i, t }] + I.f[{ i, t }] * Y[{ i, t }];
      for (int tau = 0; tau <= t; tau++)
      {
        o += I.h[{ i, t }] * X[{ i, tau }];
      }
    }
  std::cout << "Model" << std::endl;
  RfMod M;
  M.setObj(RfMin, o).add(C1).add(C2).add(C3);
  return M;
}

RfRNF createRNF(RfMod M, RfVarM X, RfVarM Y, InstanceLS& I, int nt)
{
  std::cout << "RNF" << std::endl;
  RfRNF rnf(M, RfOmit /*, RfMPSolverCPLEX*/);
  RfVarPS L;
  for (int t = 0; t < I.T; t++)
  {
    if (t % nt == 0 && t > 0)
    {
      rnf.addLocal(L);
      L = RfVarPS();
    }
    for (int i = 0; i < I.N; i++)
    {
      L.insert(X[{ i, t }]);
      L.insert(Y[{ i, t }]);
    }
  }
  rnf.addLocal(L);
  return rnf;
}

bool solve(RfMod M)
{
  RfMPS S(M /*, RfMPSolverCPLEX*/);
  return S.solve();
}

bool solveRestrict(RfMod M, RfVarM X, RfVarM Y, InstanceLS& I, int nt)
{
  RfRNF rnf = createRNF(M, X, Y, I, nt);
  return rnf.solveAndFixAll();
}

bool solveRelax(RfMod M, RfVarM X, RfVarM Y, InstanceLS& I, int nt)
{
  RfRNF rnf = createRNF(M, X, Y, I, nt);
  rnf.setSPMode(RfRelax);
  rnf.setSolverRebuildModel(false);
  return rnf.solveAndFixAll();
}

int main(int argc, char* argv[])
{
  if (argc == 1) 
  {
    usage();
    return 1;
  }
  std::string algo(argv[1]);
  if (algo != "solver" && algo != "relax" && algo != "restrict")
  {
    usage();
    return 1;
  }
  if (algo != "solver")
  {
    if (argc != 4)
    {
      usage();
      return 1;
    }
  }
  InstanceLS I(argv[2]);
  RfVarM X, Y;
  RfConM C1, C2, C3;
  RfMod M = createMod(X, Y, C1, C2, C3, I);
  if (algo == "solver")
    solve(M);
  else
  {
    int nt = std::stoi(argv[3]);
    if (algo == "relax")
      solveRelax(M, X, Y, I, nt);
    else
      solveRestrict(M, X, Y, I, nt);
  }
  return 0;
}
