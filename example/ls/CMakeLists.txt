cmake_minimum_required(VERSION 3.15)

project(LS)


find_library(ORTOOLS ortools)
if(NOT ORTOOLS)
message(STATUS "or tools included from /opt/or-tools")
include_directories(LS PRIVATE "/opt/or-tools/include")
link_directories("/opt/or-tools/lib")
endif()

add_executable(LS ls.cpp)

target_link_libraries(LS RF)

target_link_libraries(LS ortools)


