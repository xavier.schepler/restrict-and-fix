#include <fstream>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <vector>

#include "rf/algo/RfRNF.hpp"
#include "rf/model/RfMod.hpp"
#include "rf/solver/RfMPS.hpp"

using namespace Rf::Model;
using namespace Rf::Algo;
using namespace Rf::Solver;
using std::string;
using std::to_string;

void usage() 
{
  std::cout << "Usage: ./bp (solver|relax|restrict) instance [nitems]" << std::endl;
  std::cout << "With relax or restrict nitems must be given." << std::endl;
}

int firstFitDecreasing(std::vector<double> w, double C)
{
  if (w.size() == 0) return 0;
  std::sort(w.begin(), w.end(), std::greater<double>());
  std::vector<double> B;
  B.push_back(0);
  for (int i = 0; i < w.size(); i++)
  {
    bool p = false;
    for (auto& b : B) {
      if (b + w[i] <= C) 
      {
        b += w[i];
        p = true;
        break;
      }
    }
    if (! p)
    {
      B.push_back(w[i]);
    }
  }
  return B.size();
}

class InstanceBP
{
 public:
  vector<int> B;
  double C;  // Bin capacity
  int N;     // Item count
  int S;
  int UB;    // Upper bound  
  std::vector<double> w;  // Item weights

 public:
  InstanceBP(std::string const& fn, int ni)
  {
    std::ifstream ifs(fn);
    if (ifs.fail())
    {
      usage();
      throw std::invalid_argument("couldn't open instance file");
    }
    std::cout << "Reading data" << std::endl;
    ifs >> N >> C;
    if (ni == -1) ni = N;
    w.reserve(N);
    for (int i = 0; i < N; i++)
    {
      double v;
      ifs >> v;
      w.push_back(v);
    }
    std::sort(w.begin(), w.end(), std::greater<double>());
    S = ceil(double(N) / double(ni));
    B.reserve(S);
    auto it1 = w.begin(), it2 = w.begin();
    UB = 0;
    for (int i = 0; i < S; i++) {
      std::advance(it2, ni);
      std::vector _w(it1, it2);
      UB += firstFitDecreasing(_w, C);
      B.push_back(UB);
      std::advance(it1, ni);  
    }
  }
};

RfMod createMod(RfVarM X, RfVarM Y, RfConM C1, RfConM C2, InstanceBP& I)
{
  std::cout << "Creating model" << std::endl;
  std::cout << "X" << std::endl;
  // X
  for (int i = 0; i < I.N; i++)
    for (int j = 0; j < I.UB; j++)
    {
      string n = string("X_") + to_string(i) + string(",") + to_string(j);
      X[{ i, j }] = RfVar(n, RfBin);
    }
  std::cout << "Y" << std::endl;
  for (int j = 0; j < I.UB; j++)
  {
    string n = string("Y_") + to_string(j);
    Y[{ j }] = RfVar(n, RfBin);
  }
  std::cout << "C1" << std::endl;
  for (int j = 0; j < I.UB; j++)
  {
    RfExpLin e;
    for (int i = 0; i < I.N; i++)
      e += I.w[i] * X[{ i, j }];
    C1[{ j }] = e <= I.C * Y[{ j }];
  }
  std::cout << "C2" << std::endl;
  for (int i = 0; i < I.N; i++)
  {
    RfExpLin e;
    for (int j = 0; j < I.UB; j++)
    {
      e += X[{ i, j }];
    }
    C2[{ i }] = e == 1;
  }
  std::cout << "Objective" << std::endl;
  RfExpLin o;
  for (int j = 0; j < I.UB; j++)
    o += Y[{ j }];
  std::cout << "Model" << std::endl;
  RfMod M;
  M.setObj(RfMin, o).add(C1).add(C2);
  return M;
}

RfRNF createRNF(RfMod M, RfVarM X, RfVarM Y, InstanceBP& I, int ni)
{
  std::cout << "RNF" << std::endl;
  RfRNF rnf(M, RfIncludeNoArtVars /*, RfMPSolverCPLEX*/);
  RfVarPS L, G;
  int UB;
  int s = 0;
  for (int i = 0; i < I.N; i++)
  {
    if (i % ni == 0 && i > 0)
    {
      rnf.addLocal(L);
      L = RfVarPS();
      s++;
    }
    for (int j = 0; j < I.B[s]; j++) {
      L.insert(X[{i, j}]);
    }
  }
  for (int j = 0; j < I.UB; j++)
  {
    G.insert(Y[{ j }]);
  }
  rnf.addLocal(L).setGlobal(G);  
  return rnf;
}

bool solve(RfMod M)
{
  RfMPS S(M /*, RfMPSolverCPLEX*/);
  return S.solve();
}

bool solveRestrict(RfMod M, RfVarM X, RfVarM Y, InstanceBP& I, int ni)
{
  RfRNF rnf = createRNF(M, X, Y, I, ni);
  std::cout << "Restrict" << std::endl;  
  return rnf.solveAndFixAll();
}

bool solveRelax(RfMod M, RfVarM X, RfVarM Y, InstanceBP& I, int ni)
{
  RfRNF rnf = createRNF(M, X, Y, I, ni);
  std::cout << "Relax" << std::endl;  
  rnf.setSPMode(RfRelax);
  rnf.setSolverRebuildModel(false);
  return rnf.solveAndFixAll();
}

int main(int argc, char* argv[])
{
  if (argc == 1) {
    usage();
    return 1;
  }
  std::string algo(argv[1]);
  if (algo != "solver" && algo != "relax" && algo != "restrict")
  {
    usage();
    return 1;
  }
  int ni = -1;
  if (algo != "solver")
  {
    if (argc != 4)
    {
      usage();
      return 1;
    }
    ni = std::stoi(argv[3]);    
  }
  InstanceBP I(argv[2], ni);
  RfVarM X, Y;
  RfConM C1, C2;
  RfMod M = createMod(X, Y, C1, C2, I);
  if (algo == "solver")
    solve(M);
  else
  {
    if (algo == "relax")
      solveRelax(M, X, Y, I, ni);
    else
      solveRestrict(M, X, Y, I, ni);
  }
  std::cout << std::endl << "FFD UB: " << firstFitDecreasing(I.w, I.C) << std::endl;
  return 0;
}
