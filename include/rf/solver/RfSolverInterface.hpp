#ifndef RFSOLVERINTERFACE_HPP_
#define RFSOLVERINTERFACE_HPP_

#include <functional>
#include <unordered_map>

#include "rf/model/RfSol.hpp"
#include "rf/model/RfSolvable.hpp"

namespace Rf
{

  namespace Solver
  {

    using Rf::Model::RfSol;
    using Rf::Model::RfSolvable;
    using Rf::Model::RfVarP;

    typedef enum
    {
      RfTimeLimit,
      RfRelGapTol
    } RfParamD;

    typedef enum
    {
      RfMPSolverCBC,
      RfMPSolverCPLEX,
      RfMPSolverGLPK,
      RfMPSolverSCIP
    } RfSolver;

    class RfSolverInterface
    {
     protected:
      /**
       * Linear Relaxation, a.k.a. continuous relaxation
       */
      bool LR;
      std::unordered_map<RfParamD, double> paramsD;
      RfSolver solver;
      RfSol solution;
      std::reference_wrapper<RfSolvable> toSolve;

     public:
      RfSolverInterface(RfSolvable& toSolve, RfSolver solver, bool LR);
      virtual void exportLP(const std::string& path) = 0;
      virtual double getBestBound() = 0;
      virtual double getGapRelative() = 0;
      double getParamD(RfParamD param);
      virtual void* getSolverRaw() = 0;
      virtual double getValue() = 0;
      virtual double getValue(RfVarP v) = 0;
      virtual RfSol getSolution() = 0;
      virtual double getTimeSolving() = 0;
      void setLR(bool lr);
      void setParamD(RfParamD param, double val);
      virtual void setVerbose(bool v) = 0;
      virtual bool solve() = 0;
      virtual bool solve(RfSolvable& toSolve) = 0;
      virtual ~RfSolverInterface() = default;
    };

  }  // namespace Solver

}  // namespace Rf

#endif
