#ifndef RFMPSOLVER_HPP_
#define RFMPSOLVER_HPP_

#include <memory>
#include <unordered_map>

#include "ortools/linear_solver/linear_solver.h"
#include "rf/exp/RfVar.hpp"
#include "rf/model/RfCon.hpp"
#include "rf/solver/RfSolverInterface.hpp"

namespace Rf
{

  namespace Solver
  {

    using namespace operations_research;
    using namespace Rf::Model;

    class RfMPSolver : public RfSolverInterface
    {
     private:
      std::unique_ptr<MPSolver> solverMPS;
      double timeSolving;
      std::unique_ptr<std::unordered_map<RfConP, MPConstraint*>> RfConsToMPCons;
      std::unique_ptr<std::unordered_map<RfVarP, MPVariable*>> RfVarsToMPVars;
      void createCons();
      void createObj();
      void createSol();
      void createVars();

     public:
      RfMPSolver(RfSolvable& toSolve, RfSolver solver, bool LR = false);
      void exportLP(const std::string& path);
      double getBestBound();
      double getGapRelative();
      RfSol getSolution();
      void* getSolverRaw();
      double getTimeSolving();
      double getValue();
      double getValue(RfVarP v);
      void setVerbose(bool b);
      bool solve();
      bool solve(RfSolvable& toSolve);
    };

  }  // namespace Solver

}  // namespace Rf

#endif /* RFMPSOLVER_HPP_ */
