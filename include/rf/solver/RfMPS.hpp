#ifndef RFMPS_HPP_
#define RFMPS_HPP_

#include <memory>

#include "rf/solver/RfMPSolver.hpp"

namespace Rf
{

  namespace Solver
  {

    using namespace operations_research;
    using namespace Rf::Model;

    class RfMPS
    {
     private:
      std::shared_ptr<RfMPSolver> solver;

     public:
      RfMPS(RfSolvable& toSolve, RfSolver solver = RfMPSolverSCIP, bool LR = false);
      RfMPS exportLP(const std::string& path);
      double getBestBound();
      double getGapRelative();
      double getParamD(RfParamD param);
      RfSol getSolution();
      void* getSolverRaw();
      double getTimeSolving();
      double getValue();
      double getValue(RfVarP v);
      RfMPS setLR(bool lr);
      RfMPS setParamD(RfParamD param, double val);
      RfMPS setVerbose(bool b);
      bool solve();
      bool solve(RfSolvable& toSolve);
    };

  }  // namespace Solver

}  // namespace Rf

#endif /* RFMPS_HPP_ */
