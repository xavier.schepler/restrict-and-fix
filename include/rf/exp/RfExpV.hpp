#ifndef RFEXPV_HPP_
#define RFEXPV_HPP_

#include <memory>
#include <vector>

#include "rf/model/RfSol.hpp"

namespace Rf
{

  namespace Model
  {

    using std::vector;

    class RfExpL;
    class RfExpLin;
    class RfExpVector;

    class RfExpV
    {
     public:
      std::shared_ptr<RfExpVector> expression;

     public:
      RfExpV();
      RfExpV(const RfExpL& e);
      RfExpV(const RfExpLin& e);
      RfExpV(
          const vector<double>& addendCoeffs,
          const vector<RfVarP>& addendVars,
          const vector<size_t>& counts,
          const vector<int>& degrees,
          const RfVarPS& vars);
      /**
       * Designed to add artificial variables to subproblem's objective.
       * Variables of the argument must not be found in addendVars.
       */
      RfExpV addArtVar(double coeff, RfVarP v);
      RfExpV clearArtVars();
      double getAddendCoeff(int degree, size_t pos) const;
      RfVarP getAddendVar(int degree, size_t pos, size_t elt) const;
      RfVarP getArtVar(size_t pos) const;
      double getArtVarCoeff(size_t pos) const;
      size_t getArtVarsCount() const;
      size_t getCount(int degree) const;
      int getDegree() const;
      std::string getName() const;
      double getValue(RfSol s, bool omitConstant = false) const;
      const RfVarPS& getVars() const;
      RfVarPS getVarsReal() const;
      RfVarPS getVarsInt() const;
      bool hasVarP(RfVarP v) const;
      operator std::string() const;
    };

  }  // namespace Model

}  // namespace Rf

#include "rf/exp/RfExpL.hpp"
#include "rf/exp/RfExpLin.hpp"
#include "rf/exp/RfExpVector.hpp"

#endif
