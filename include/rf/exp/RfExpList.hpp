#ifndef RFEXPLIST_HPP_
#define RFEXPLIST_HPP_

#include "rf/exp/RfExpListElement.hpp"

namespace Rf
{

  namespace Model
  {

    class RfExpL;

    class RfExpList
    {
     private:
      std::unique_ptr<RfExpLEL> list;
      bool reducedAndSorted;

     public:
      RfExpList();
      void additiveInverse();
      RfExpLEL::iterator begin() const;
      void clear();
      RfExpLEL::iterator end() const;
      double getConstant();
      int getDegree();
      RfExpL getPolyAddendsByDegree(int d);
      void insert(const RfExpList& l);
      void push_back(const RfExpLE& le);
      /**
       * Sum coefficients when possible,
       * sort the reduced list by ascending degrees and by adresses.
       */
      void sortAndReduce();
      size_t size() const;
      void splice(const RfExpList& l);
      operator std::string() const;
    };

  }  // namespace Model

}  // namespace Rf

#endif
