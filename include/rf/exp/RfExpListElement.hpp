#ifndef RFEXPLISTELEMENT_HPP_
#define RFEXPLISTELEMENT_HPP_

#include <memory>

#include "rf/exp/RfVar.hpp"

namespace Rf
{

  namespace Model
  {

    class RfExpListElement
    {
     public:
      double coeff;
      std::unique_ptr<RfVarPL> vars;

     public:
      RfExpListElement();
      RfExpListElement(const RfExpListElement& le);
      RfExpListElement(RfExpListElement&& le);
      RfExpListElement(double e);
      RfExpListElement(int e);
      RfExpListElement(RfVarP v);
      RfVarPL::iterator begin() const;
      RfVarPL::iterator end() const;
      double getCoeff() const;
      int getDegree() const;
      RfVarPL* getVarPL() const;
      void insertVars(const RfExpListElement& e);
      RfExpListElement& operator+=(const double& d);
      RfExpListElement& operator*=(const double& d);
      RfExpListElement& operator*=(const RfVarP& v);
      operator std::string() const;
      /**
       * Compare degrees, then adresses.
       */
      bool operator<(const RfExpListElement& e) const;
    };

    typedef RfExpListElement RfExpLE;
    typedef std::list<RfExpLE> RfExpLEL;

    RfExpLE operator*(RfExpLE& e, const double& d);
    RfExpLE operator*(const double& d, RfExpLE& e);
    RfExpLE operator*(RfExpLE& e1, RfExpLE& e2);
    bool operator==(const RfExpLE& e1, const RfExpLE& e2);

  }  // namespace Model

}  // namespace Rf

#endif
