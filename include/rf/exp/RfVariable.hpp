#ifndef RFVARIABLE_HPP_
#define RFVARIABLE_HPP_

#include <list>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "rf/RfTypes.hpp"

namespace Rf
{

  namespace Model
  {

    enum RfVarType
    {
      RfBin,
      RfInt,
      RfReal
    };

    class RfVariable
    {
     private:
      double LB;
      std::string name;
      RfVarType type;
      double UB;

     public:
      RfVariable();
      RfVariable(const char *name, RfVarType type, double lb = -RfInfinity, double ub = RfInfinity);
      RfVariable(const std::string &name, RfVarType type, double lb = -RfInfinity, double ub = RfInfinity);
      RfVariable(const RfVariable &v);
      double getLB() const;
      std::string getName() const;
      RfVarType getType() const;
      double getUB() const;
      bool hasChildren() const;
      bool isInt() const;
      bool isReal() const;
      void setType(RfVarType t);
      void setUB(double ub);
      operator std::string() const;
    };

    typedef RfVariable const *RfVarP;
    typedef std::list<RfVarP> RfVarPL;
    typedef std::unordered_set<RfVarP> RfVarPS;
    typedef std::unordered_map<RfVarP, double> RfVarPDouble;

  }  // namespace Model

}  // namespace Rf

#endif /* RFVARIABLE_HPP_ */
