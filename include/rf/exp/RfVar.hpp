#ifndef RFVAR_HPP_
#define RFVAR_HPP_

#include <list>
#include <memory>

#include "rf/RfTypes.hpp"
#include "rf/exp/RfVariable.hpp"

namespace Rf
{

  namespace Model
  {

    class RfVar
    {
     private:
      std::shared_ptr<RfVariable> variable;

     public:
      RfVar();
      RfVar(const char *name, RfVarType type, double lb = -RfInfinity, double ub = RfInfinity);
      RfVar(const std::string &name, RfVarType type, double lb = -RfInfinity, double ub = RfInfinity);
      RfVar(const RfVariable &v);
      RfVar(std::shared_ptr<RfVariable> &v);
      RfVariable *get() const;
      double getLB() const;
      std::string getName() const;
      RfVarType getType() const;
      double getUB() const;
      bool isInt() const;
      bool isReal() const;
      RfVar setType(RfVarType t);
      RfVar setUB(double ub);
      operator std::string() const;
      operator RfVarP() const;
    };

    typedef std::list<RfVar> RfVarL;

  }  // namespace Model

}  // namespace Rf

#endif /* RFVAR_HPP_ */
