#ifndef RFEXPVECTOR_HPP_
#define RFEXPVECTOR_HPP_

#include <memory>

#include "rf/exp/RfExpL.hpp"
#include "rf/exp/RfExpLin.hpp"
#include "rf/exp/RfExpV.hpp"

namespace Rf
{

  namespace Model
  {

    using std::size_t;
    using std::vector;

    class RfExpVector
    {
     protected:
      std::unique_ptr<vector<double>> addendCoeffs;
      std::unique_ptr<vector<RfVarP>> addendVars;
      std::unique_ptr<vector<double>> artVarCoeffs;
      std::unique_ptr<vector<RfVarP>> artVars;
      /**
       * Count of poly addends of the degrees[i]-th, ascending.
       */
      std::unique_ptr<vector<size_t>> counts;
      /**
       * Degrees, ascending.
       */
      std::unique_ptr<vector<int>> degrees;

      std::unique_ptr<RfVarPS> vars;

      size_t getAddendCoeffIndex(int degree, size_t pos) const;
      size_t getAddendVarIndex(int degree, size_t pos, size_t elt) const;

     public:
      RfExpVector(const RfExpL& e);
      RfExpVector(const RfExpLin& e);
      RfExpVector(
          const vector<double>& addendCoeffs,
          const vector<RfVarP>& addendVars,
          const vector<size_t>& counts,
          const vector<int>& degrees,
          const RfVarPS& vars);
      /**
       * Designed to add artificial variables to subproblem's objective.
       * Variables of the argument must not be found in addendVars.
       */
      void addArtVar(double coeff, RfVarP v);
      void clearArtVars();
      double getAddendCoeff(int degree, size_t pos) const;
      RfVarP getAddendVar(int degree, size_t pos, size_t elt) const;
      RfVarP getArtVar(size_t pos) const;
      double getArtVarCoeff(size_t pos) const;
      size_t getArtVarsCount() const;
      size_t getCount(int degree) const;
      int getDegree() const;
      std::string getName() const;
      double getValue(RfSol s, bool omitConstant = false) const;
      const RfVarPS& getVars() const;
      RfVarPS getVarsReal() const;
      RfVarPS getVarsInt() const;
      bool hasVarP(RfVarP v) const;
      operator std::string() const;
    };

  }  // namespace Model

}  // namespace Rf

#endif
