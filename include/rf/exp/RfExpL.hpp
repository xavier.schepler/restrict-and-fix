#ifndef RFEXPL_HPP_
#define RFEXPL_HPP_

#include <memory>

#include "rf/exp/RfExpList.hpp"
#include "rf/exp/RfExpV.hpp"

namespace Rf
{

  namespace Model
  {

    enum RfOperator
    {
      RfPlus,
      RfTimes
    };

    class RfExpL
    {
     private:
      std::unique_ptr<RfExpList> list;

     public:
      RfExpL();
      RfExpL(const RfExpL &e);
      RfExpL(RfExpL &&e);
      RfExpL(double d);
      RfExpL(int i);
      RfExpL(RfOperator op, RfExpL &&e1, RfExpL &&e2);
      RfExpL(const RfVar &v);
      RfExpL(const RfVarP &v);

      const RfExpL &additiveInverse() const;
      RfExpLEL::iterator begin() const;
      const RfExpL &clear() const;
      RfExpLEL::iterator end() const;
      double get(RfVarP v);
      double getConstant() const;
      int getDegree() const;
      RfExpL getPolyAddendsByDegree(int d) const;
      const RfExpL &insert(const RfExpL &l) const;
      const RfExpL &push_back(const RfExpLE &le) const;
      /**
       * Sum coefficients when possible,
       * sort the reduced list by ascending degrees and by adresses.
       */
      const RfExpL &sortAndReduce() const;
      size_t size() const;
      const RfExpL &splice(const RfExpL &l) const;

      RfExpL &operator=(RfExpL &e);
      RfExpL &operator=(RfExpL &&e);
      RfExpL &operator+=(RfExpL &e);
      RfExpL &operator+=(RfExpL &&e);
      RfExpL &operator-=(RfExpL &e);
      RfExpL &operator-=(RfExpL &&e);
      RfExpL &operator*=(RfExpL &e);
      RfExpL &operator*=(RfExpL &&e);

      operator std::string() const;
    };

    typedef RfExpL RfExp;

    RfExpL operator+(RfExpL &e1, RfExpL &e2);
    RfExpL operator+(RfExpL &&e1, RfExpL &e2);
    RfExpL operator+(RfExpL &e1, RfExpL &&e2);
    RfExpL operator+(RfExpL &&e1, RfExpL &&e2);

    RfExpL operator-(RfExpL &e1, RfExpL &e2);
    RfExpL operator-(RfExpL &&e1, RfExpL &e2);
    RfExpL operator-(RfExpL &e1, RfExpL &&e2);
    RfExpL operator-(RfExpL &&e1, RfExpL &&e2);

    RfExpL operator*(RfExpL &e1, RfExpL &e2);
    RfExpL operator*(RfExpL &&e1, RfExpL &e2);
    RfExpL operator*(RfExpL &e1, RfExpL &&e2);
    RfExpL operator*(RfExpL &&e1, RfExpL &&e2);

    RfExpL operator-(const RfVar &v);

    RfExpL operator+(const RfVar &v, const int &i);
    RfExpL operator+(const int &i, const RfVar &v);
    RfExpL operator-(const RfVar &v, const int &i);
    RfExpL operator-(const int &i, const RfVar &v);

    RfExpL operator+(const RfVar &v, const double &d);
    RfExpL operator+(const double &d, const RfVar &v);
    RfExpL operator-(const RfVar &v, const double &d);
    RfExpL operator-(const double &d, const RfVar &v);

    RfExpL operator-(RfExpL &e);
    RfExpL operator-(RfExpL &&e);

  }  // namespace Model

}  // namespace Rf

#endif
