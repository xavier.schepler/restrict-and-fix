#ifndef RFEXPLIN_HPP_
#define RFEXPLIN_HPP_

#include <unordered_map>

#include "rf/exp/RfExpL.hpp"
#include "rf/exp/RfExpV.hpp"
#include "rf/exp/RfVar.hpp"

namespace Rf
{

  namespace Model
  {

    class RfExpL;

    class RfExpLin
    {
     private:
      std::unique_ptr<double> constant;
      std::unique_ptr<std::unordered_map<RfVarP, double>> varsAndCoeffs;

     public:
      RfExpLin();
      RfExpLin(double d);
      RfExpLin(int i);
      RfExpLin(const RfExpL &e);
      RfExpLin(const RfExpLin &e);
      RfExpLin(RfExpLin &&e);
      RfExpLin(const RfVar &v);
      RfExpLin(const RfVarP &v);
      const RfExpLin &add(double v) const;
      const RfExpLin &add(const RfExpLin &e) const;
      const RfExpLin &add(RfVarP v, double coeff) const;
      const RfExpLin &additiveInverse() const;
      std::unordered_map<RfVarP, double>::iterator begin() const;
      std::unordered_map<RfVarP, double>::iterator end() const;
      double get(RfVarP v) const;
      double getConstant() const;
      size_t size() const;
      const RfExpLin &substract(const RfExpLin &e) const;
      RfExpLin &operator=(RfExpLin &e);
      RfExpLin &operator=(RfExpLin &&e);
      RfExpLin &operator+=(const RfExpL &e);
      RfExpLin &operator-=(const RfExpL &e);
      operator std::string() const;
    };

  }  // namespace Model

}  // namespace Rf

#endif
