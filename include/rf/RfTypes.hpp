#ifndef TYPES_HPP_
#define TYPES_HPP_

#include <cmath>

#define RfInfinity HUGE_VAL
#define RfEpsilon  1e-6

#endif
