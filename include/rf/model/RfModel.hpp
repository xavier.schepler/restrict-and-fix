#ifndef RFMODEL_HPP_
#define RFMODEL_HPP_

#include <memory>

#include "rf/exp/RfExpV.hpp"
#include "rf/model/RfCon.hpp"
#include "rf/model/RfMap.hpp"
#include "rf/model/RfSol.hpp"
#include "rf/model/RfSolvable.hpp"

namespace Rf
{

  namespace Model
  {

    enum RfObjType
    {
      RfMin,
      RfMax
    };

    class RfModel : public RfSolvable
    {
     private:
      std::unique_ptr<RfConPS> cons;
      int degree;
      RfExpV obj;
      RfObjType type;
      std::unique_ptr<RfVarPS> vars;
      std::unique_ptr<RfVarPS> varsInt;
      std::unique_ptr<RfVarPS> varsReal;

     public:
      RfModel();
      void add(RfCon& c);
      void add(RfConM C);
      RfConPS createCons();
      RfVarPS createVars();
      RfConPS getCons() const;
      int getDegree() const;
      double getLB(RfVarP v) const;
      RfExpV getObj();
      RfObjType getObjType() const;
      RfVarPS getVars() const;
      std::size_t getVarsCount() const;
      RfVarPS getVarsInt() const;
      RfVarPS getVarsReal() const;
      double getRHS(RfConP v) const;
      double getUB(RfVarP v) const;
      bool hasCon(RfConP v) const;
      bool hasVar(RfVarP v) const;
      bool isArtificial(RfVarP v) const;
      bool isFeasible(RfSol s, double epsilon = RfEpsilon) const;
      bool isInt(RfVarP v) const;
      bool isMax() const;
      bool isMin() const;
      void setObj(RfObjType t, const RfExpV& obj);
      operator std::string() const;
    };

  }  // namespace Model

}  // namespace Rf

#endif /* RFMODEL_HPP_ */
