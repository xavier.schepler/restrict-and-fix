#ifndef RFSOL_HPP_
#define RFSOL_HPP_

#include <memory>

#include "rf/model/RfSolution.hpp"

namespace Rf
{

  namespace Model
  {

    class RfSol
    {
     private:
      std::shared_ptr<RfSolution> solution;

     public:
      RfSol();
      RfSol(double v);
      RfSolution* get();
      double getValue() const;
      bool hasVar(RfVarP v) const;
      double& operator[](RfVarP v);
    };

  }  // namespace Model

}  // namespace Rf

#endif /* RFSOL_HPP_ */
