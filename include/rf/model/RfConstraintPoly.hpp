#ifndef RFCONSTRAINTPOLY_HPP_
#define RFCONSTRAINTPOLY_HPP_

#include <unordered_map>
#include <unordered_set>

#include "rf/exp/RfExpL.hpp"
#include "rf/exp/RfExpVector.hpp"
#include "rf/exp/RfVariable.hpp"
#include "rf/model/RfSol.hpp"

namespace Rf
{

  namespace Model
  {

    enum RfConType
    {
      RfEqual,
      RfGTE,
      RfLTE
    };
    using std::size_t;
    using std::vector;

    class RfConstraintPoly : public RfExpVector
    {
     public:
      static int number;

     private:
      std::string name;
      double RHS;
      RfConType type;

     public:
      RfConstraintPoly(RfConstraintPoly const* c);
      RfConstraintPoly(RfConType type, const RfExpL& LHS, double RHS);
      RfConstraintPoly(RfConType type, const RfExpLin& LHS, double RHS);
      RfConstraintPoly(RfConType type, const RfExpL& LHS, const RfExpL& RHS);
      RfConstraintPoly(RfConType type, const RfExpLin& LHS, const RfExpLin& RHS);
      std::string getName() const;
      double getRHS() const;
      RfConType getType() const;
      bool isFeasible(RfSol s, double epsilon = RfEpsilon) const;
      void setName(const std::string& n);
      operator std::string() const;
    };

    typedef RfConstraintPoly const* RfConP;
    typedef std::unordered_set<RfConP> RfConPS;
    typedef std::unordered_map<RfConP, double> RfConsNums;

  }  // namespace Model

}  // namespace Rf

#endif /* RFCONSTRAINT_HPP_ */
