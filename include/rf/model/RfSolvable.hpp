#ifndef RFSOLVABLE_HPP_
#define RFSOLVABLE_HPP_

#include "rf/exp/RfVar.hpp"
#include "rf/model/RfCon.hpp"

namespace Rf
{

  namespace Model
  {

    class RfSolvable
    {
     public:
      virtual RfConPS getCons() const = 0;
      virtual int getDegree() const = 0;
      virtual double getLB(RfVarP v) const = 0;
      virtual RfExpV getObj() = 0;
      virtual double getRHS(RfConP c) const = 0;
      virtual double getUB(RfVarP v) const = 0;
      virtual RfVarPS getVars() const = 0;
      virtual bool hasCon(RfConP c) const = 0;
      virtual bool hasVar(RfVarP v) const = 0;
      virtual bool isArtificial(RfVarP v) const = 0;
      virtual bool isInt(RfVarP v) const = 0;
      virtual bool isMax() const = 0;
      virtual bool isMin() const = 0;
      virtual ~RfSolvable() = default;
    };

  }  // namespace Model

}  // namespace Rf

#endif /* RFSOLVABLE_HPP_ */
