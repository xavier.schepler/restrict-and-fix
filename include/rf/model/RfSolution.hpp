#ifndef RFSOLUTION_HPP_
#define RFSOLUTION_HPP_

#include "rf/exp/RfVariable.hpp"

namespace Rf
{

  namespace Model
  {

    using namespace Rf::Model;

    class RfSolution
    {
     private:
      double value;
      RfVarPDouble varsVals;

     public:
      RfSolution(double v);
      double getValue() const;
      bool hasVar(RfVarP v) const;
      double& operator[](RfVarP v);
    };

  }  // namespace Model

}  // namespace Rf

#endif /* RFSOLUTION_HPP_ */
