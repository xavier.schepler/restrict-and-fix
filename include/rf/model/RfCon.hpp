#ifndef RFCONS_HPP_
#define RFCONS_HPP_

#include <list>
#include <memory>

#include "rf/exp/RfExpL.hpp"
#include "rf/model/RfConstraintPoly.hpp"

namespace Rf
{

  namespace Model
  {

    class RfCon
    {
     private:
      std::shared_ptr<RfConstraintPoly> constraint;

     public:
      RfCon();
      RfCon(RfConP c);
      RfCon(RfConType type, const RfExpL &LHS, const RfExpL &RHS);
      RfCon(RfConType type, const RfExpLin &LHS, const RfExpLin &RHS);
      RfConstraintPoly *get() const;
      double getAddendCoeff(int degree, size_t pos) const;
      RfVarP getAddendVar(int degree, size_t pos, size_t elt) const;
      size_t getCount(int degree) const;
      int getDegree() const;
      std::string getName() const;
      double getRHS() const;
      RfConType getType() const;
      const RfVarPS &getVars() const;
      RfVarPS getVarsReal() const;
      RfVarPS getVarsInt() const;
      bool hasVarP(RfVarP v) const;
      bool isFeasible(RfSol s, double epsilon = RfEpsilon) const;
      void setName(const std::string &n);
      operator std::string() const;
    };

    typedef std::list<RfCon> RfConL;

    RfCon operator==(const RfExp &LHS, const int &RHS);
    RfCon operator<=(const RfExp &LHS, const int &RHS);
    RfCon operator>=(const RfExp &LHS, const int &RHS);
    RfCon operator==(const int &LHS, const RfExp &RHS);
    RfCon operator<=(const int &LHS, const RfExp &RHS);
    RfCon operator>=(const int &LHS, const RfExp &RHS);

    RfCon operator==(const RfExp &LHS, const double &RHS);
    RfCon operator<=(const RfExp &LHS, const double &RHS);
    RfCon operator>=(const RfExp &LHS, const double &RHS);
    RfCon operator==(const double &LHS, const RfExp &RHS);
    RfCon operator<=(const double &LHS, const RfExp &RHS);
    RfCon operator>=(const double &LHS, const RfExp &RHS);

    RfCon operator==(const RfExp &LHS, const RfExp &RHS);
    RfCon operator<=(const RfExp &LHS, const RfExp &RHS);
    RfCon operator>=(const RfExp &LHS, const RfExp &RHS);

    RfCon operator==(const RfExpLin &LHS, const int &RHS);
    RfCon operator<=(const RfExpLin &LHS, const int &RHS);
    RfCon operator>=(const RfExpLin &LHS, const int &RHS);
    RfCon operator==(const int &LHS, const RfExpLin &RHS);
    RfCon operator<=(const int &LHS, const RfExpLin &RHS);
    RfCon operator>=(const int &LHS, const RfExpLin &RHS);

    RfCon operator==(const RfExpLin &LHS, const double &RHS);
    RfCon operator<=(const RfExpLin &LHS, const double &RHS);
    RfCon operator>=(const RfExpLin &LHS, const double &RHS);
    RfCon operator==(const double &LHS, const RfExpLin &RHS);
    RfCon operator<=(const double &LHS, const RfExpLin &RHS);
    RfCon operator>=(const double &LHS, const RfExpLin &RHS);

    RfCon operator==(const RfExpLin &LHS, const RfExpLin &RHS);
    RfCon operator<=(const RfExpLin &LHS, const RfExpLin &RHS);
    RfCon operator>=(const RfExpLin &LHS, const RfExpLin &RHS);

  }  // namespace Model

}  // namespace Rf

#endif /* RFCONSTRAINT_HPP_ */
