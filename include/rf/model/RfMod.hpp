#ifndef RFMOD_HPP_
#define RFMOD_HPP_

#include <memory>

#include "rf/model/RfModel.hpp"

namespace Rf
{

  namespace Model
  {

    class RfMod : public RfSolvable
    {
     private:
      std::shared_ptr<RfModel> model;

     public:
      RfMod();
      RfMod add(RfCon& c);
      RfMod add(RfConM C);
      RfConPS createCons();
      RfVarPS createVars();
      RfConPS getCons() const;
      int getDegree() const;
      double getLB(RfVarP v) const;
      RfExpV getObj();
      RfObjType getObjType() const;
      double getRHS(RfConP v) const;
      double getUB(RfVarP v) const;
      RfVarPS getVars() const;
      std::size_t getVarsCount() const;
      RfVarPS getVarsInt() const;
      RfVarPS getVarsReal() const;
      bool hasCon(RfConP v) const;
      bool hasVar(RfVarP v) const;
      bool isArtificial(RfVarP v) const;
      bool isFeasible(RfSol s, double epsilon = RfEpsilon) const;
      bool isInt(RfVarP v) const;
      bool isMax() const;
      bool isMin() const;
      RfMod setObj(RfObjType t, const RfExpV& obj);
      operator std::string() const;
    };

  }  // namespace Model

}  // namespace Rf

#endif /* RFMOD_HPP_ */
