#ifndef RFMAP_HPP_
#define RFMAP_HPP_

#include <memory>
#include <sstream>
#include <unordered_map>

#include "rf/exp/RfVar.hpp"
#include "rf/model/RfCon.hpp"

namespace Rf
{

  namespace Model
  {

    template<class T>
    class RfMap
    {
     private:
      std::shared_ptr<std::unordered_map<std::string, T>> objects;

     private:
      std::string getKey(std::vector<int> &idxs)
      {
        std::ostringstream oss;
        for (auto i : idxs)
        {
          oss << i << ',';
        }
        return oss.str();
      }

     public:
      RfMap() : objects(std::make_shared<std::unordered_map<std::string, T>>())
      {
      }

      T &at(std::vector<int> idxs)
      {
        return objects->at(getKey(idxs));
      }

      typename std::unordered_map<std::string, T>::iterator begin()
      {
        return objects->begin();
      }

      typename std::unordered_map<std::string, T>::iterator end()
      {
        return objects->end();
      }

      std::unordered_map<std::string, T> &getMap()
      {
        return *objects;
      }

      T &operator[](std::vector<int> idxs)
      {
        std::string idx(getKey(idxs));
        if (objects->find(idx) == objects->end())
        {
          (*objects)[idx] = T();
        }
        return (*objects)[idx];
      }
    };

    typedef RfMap<double> RfNumM;
    typedef RfMap<RfVar> RfVarM;
    typedef RfMap<RfCon> RfConM;

  }  // namespace Model

}  // namespace Rf

#endif /* RFMAP_HPP_ */
