#ifndef RFRESTRICTANDFIX_HPP_
#define RFRESTRICTANDFIX_HPP_

#include <iostream>
#include <memory>
#include <unordered_map>

#include "rf/algo/RfSubpro.hpp"
#include "rf/model/RfSol.hpp"
#include "rf/solver/RfMPSolver.hpp"

namespace Rf
{

  namespace Algo
  {

    using namespace Rf::Model;
    using namespace Rf::Solver;
    using std::size_t;

    class RfRestrictAndFix
    {
     private:
      RfSubpro current;
      bool currentHasSol;
      size_t currentNum;
      RfSol currentSol;
      bool exportLP;
      std::string exportLPBaseName;
      std::vector<double> gapsRelative;
      RfSol LRSol;
      double LRTimeSolving;
      RfSPMode mode;
      RfMod model;
      RfSolver solver;
      std::unique_ptr<RfSolverInterface> solverInterface;
      std::unordered_map<RfParamD, double> solverParamsD;
      bool solverRebuildModel;
      std::vector<double> timesFixing;
      std::vector<double> timesInit;
      std::vector<double> timesSolving;
      std::vector<double> values;
      RfVarPS varsGlobal;
      std::vector<RfVarPS> varsLocals;
      RfVarPDouble varsVals;
      bool verbose;

     private:
      void createSolverInterface(RfSolvable &toSolve, bool continuousRelaxation = false);
      void fixVars();
      void p(const std::string &s, int width = -1, std::ostream &o = std::cout);
      void pn(const std::string &s, int width = -1, std::ostream &o = std::cout);
      void solvingSummary();

     public:
      RfRestrictAndFix(
          RfMod model,
          RfSPMode SPmode = RfInclude,
          RfSolver solver = RfMPSolverSCIP,
          double SPTimeLimit = 3600,
          double SPRelGapTol = 0.01,
          bool solverRebuildModel = true,
          bool verbose = true);
      /**
       * Define a subproblem by adding a set of local variables.
       */
      void addLocal(RfVarPS &L);
      /**
       * Are all variables fixed?
       */
      bool areAllFixed() const;
      /**
       * Using first unprocessed set of local variables, create current subproblem.
       * Create residual subproblem if all sets of local variables have been processed.
       */
      void createCurrent();
      void fix(RfVarP var, double val);
      /**
       * Number of subproblems, equal to the provided number of RfVarPS
       * with RfRestrictAndFix::addLocal().
       */
      size_t getCount() const;
      RfSubpro getCurrent() const;
      size_t getCurrentNum() const;
      /**
       * Solution to the current subproblem.
       * RfRestrictAndFix::solveAndFix() must have been called before.
       */
      RfSol getCurrentSol() const;
      RfVarPDouble getFixeds() const;
      /**
       * Relative gap between current solution value
       * and current subproblem's best bound.
       */
      double getGapRelative() const;
      /**
       * Relative gap between current solution value
       * and i th subproblem's best bound.
       */
      double getGapRelative(size_t i) const;
      /**
       * Relative gap between current solution value
       * and complete model's linear relaxation value.
       * RfRestrictAndFix::solveLR() and RfRestrictAndFix::solveAndFix()
       * must have been called before.
       */
      double getGapRelativeLR() const;
      /**
       * Linear Relaxation (a.k.a. continuous relaxation) solution,
       * RfRestrictAndFix::solveLR() must have been called before.
       */
      RfSol getLRSol() const;
      /**
       * Linear Relaxation (a.k.a. continuous relaxation) solving time,
       * RfRestrictAndFix::solveLR() must have been called before.
       */
      double getLRTimeSolving() const;
      /**
       * Linear Relaxation (a.k.a. continuous relaxation) value,
       * RfRestrictAndFix::solveLR() must have been called before.
       */
      double getLRValue() const;
      double getSolverParamD(RfParamD p) const;
      void *getSolverRaw();
      double getTimeFixing() const;
      double getTimeFixing(size_t i) const;
      double getTimeFixingTotal() const;
      double getTimeInit() const;
      double getTimeInit(size_t i) const;
      double getTimeInitTotal() const;
      double getTimeSolving() const;
      double getTimeSolving(size_t i) const;
      double getTimeSolvingTotal() const;
      double getTimeTotal() const;
      double getTimeTotal(size_t i) const;
      double getValue() const;
      double getValue(size_t i) const;
      double getValue(RfVarP v) const;
      std::size_t getVarsCount() const;
      std::size_t getVarsFixedCount() const;
      bool hasCurrentSol() const;
      bool hasSolverRebuildModel() const;
      bool isCurrentSolFeasible() const;
      void setExportLP(bool b, const std::string &base_name = "sp_");
      void setFixeds(RfVarPDouble &V);
      /**
       * Define the set of global variables.
       */
      void setGlobal(RfVarPS &V);
      /**
       * Available:
       * - RfTimeLimit, time limit.
       * - RfRelGapTol, relative gap tolerance.
       */
      void setSolverParamD(RfParamD p, double v);
      /**
       * Whether to build a new model within the solver for each subproblem,
       * or to update a unique model, adding / removing columns and rows,
       * updating variables' bounds and types.
       */
      void setSolverRebuildModel(bool b);
      void setSPMode(RfSPMode m);
      void setVerbose(bool b);
      bool solveAndFix();
      bool solveAndFixAll(bool solveLR = true, double timeLimitLR = 3600);
      /**
       * Solve Linear Relaxation (a.k.a. continuous relaxation)
       */
      bool solveLR(double timeLimit = 3600);
      void unfix(RfVarP v);
    };

  }  // namespace Algo

}  // namespace Rf

#endif /* RFRESTRICTANDFIX_HPP_ */
