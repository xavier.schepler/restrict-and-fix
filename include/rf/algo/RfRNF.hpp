#ifndef RFRNF_HPP_
#define RFRNF_HPP_

#include <memory>

#include "rf/algo/RfRestrictAndFix.hpp"

namespace Rf
{

  namespace Algo
  {

    class RfRNF
    {
     private:
      std::shared_ptr<RfRestrictAndFix> rnf;

     public:
      RfRNF(
          RfMod model,
          RfSPMode SPmode = RfInclude,
          RfSolver solver = RfMPSolverSCIP,
          double SPTimeLimit = 3600,
          double SPRelGapTol = 0.01,
          bool verbose = true);
      /**
       * Define a subproblem by adding a set of local variables.
       */
      RfRNF addLocal(RfVarPS &L);
      /**
       * Are all variables fixed?
       */
      bool areAllFixed() const;
      /**
       * Using first unprocessed set of local variables, create current subproblem.
       * Create residual subproblem if all sets of local variables have been processed.
       */
      RfRNF createCurrent();
      RfRNF fix(RfVarP var, double val);
      /**
       * Number of subproblems, equal to the provided number of RfVarPS
       * with RfRestrictAndFix::addLocal().
       */
      size_t getCount() const;
      RfSubpro getCurrent() const;
      size_t getCurrentNum() const;
      /**
       * Solution to the current subproblem.
       * RfRestrictAndFix::solveAndFix() must have been called before.
       */
      RfSol getCurrentSol() const;
      RfVarPDouble getFixeds() const;
      /**
       * Relative gap between current solution value
       * and current subproblem's best bound.
       */
      double getGapRelative() const;
      /**
       * Relative gap between current solution value
       * and i th subproblem's best bound.
       */
      double getGapRelative(size_t i) const;
      /**
       * Relative gap between current solution value
       * and complete model's linear relaxation value.
       * RfRestrictAndFix::solveLR() and RfRestrictAndFix::solveAndFix()
       * must have been called before.
       */
      double getGapRelativeLR() const;
      /**
       * Linear Relaxation (a.k.a. continuous relaxation) solution,
       * RfRestrictAndFix::solveLR() must have been called before.
       */
      RfSol getLRSol() const;
      /**
       * Linear Relaxation (a.k.a. continuous relaxation) solving time,
       * RfRestrictAndFix::solveLR() must have been called before.
       */
      double getLRTimeSolving() const;
      /**
       * Linear Relaxation (a.k.a. continuous relaxation) value,
       * RfRestrictAndFix::solveLR() must have been called before.
       */
      double getLRValue() const;
      double getSolverParamD(RfParamD p) const;
      void *getSolverRaw();
      double getTimeFixing() const;
      double getTimeFixing(size_t i) const;
      double getTimeFixingTotal() const;
      double getTimeInit() const;
      double getTimeInit(size_t i) const;
      double getTimeInitTotal() const;
      double getTimeSolving() const;
      double getTimeSolving(size_t i) const;
      double getTimeSolvingTotal() const;
      double getTimeTotal() const;
      double getTimeTotal(size_t i) const;
      double getValue() const;
      double getValue(RfVarP v) const;
      double getValue(size_t i) const;
      std::size_t getVarsCount() const;
      std::size_t getVarsFixedCount() const;
      bool hasCurrentSol() const;
      bool hasSolverRebuildModel() const;
      bool isCurrentSolFeasible() const;
      RfRNF setExportLP(bool b, const std::string &base_name = "sp_");
      RfRNF setFixeds(RfVarPDouble &V);
      /**
       * Define the set of global variables.
       */
      RfRNF setGlobal(RfVarPS &V);
      RfRNF setSolverParamD(RfParamD p, double v);
      /**
       * Whether to build a new model within the solver for each subproblem,
       * or to update a unique model, adding / removing columns and rows,
       * updating variables' bounds and types.
       */
      RfRNF setSolverRebuildModel(bool b);
      RfRNF setSPMode(RfSPMode m);
      RfRNF setVerbose(bool b);
      bool solveAndFix();
      bool solveAndFixAll(bool solveLR = true, double timeLimitLR = 3600);
      /**
       * Solve Linear Relaxation (a.k.a. continuous relaxation)
       */
      bool solveLR(double timeLimit = 3600);
      RfRNF unfix(RfVarP v);
    };

  }  // namespace Algo

}  // namespace Rf

#endif /* RFRNF_HPP_ */
