#ifndef RFSUBPRO_HPP_
#define RFSUBPRO_HPP_

#include <memory>

#include "rf/algo/RfSubproblem.hpp"

namespace Rf
{

  namespace Algo
  {

    class RfSubpro : public RfSolvable
    {
     private:
      std::shared_ptr<RfSubproblem> subpro;

     public:
      RfSubpro();
      RfSubpro(RfMod ml, RfVarPS Vlocal, RfVarPS Vglobal, RfSPMode m = RfInclude);
      RfConPS createCons();
      RfVarPS createVars();
      RfSubpro fix(RfVarP var, double val);
      double getBigM(RfConP c) const;
      RfConPS getCons() const;
      int getDegree() const;
      RfVarPDouble getFixeds() const;
      double getLB(RfVarP v) const;
      RfMod getModel() const;
      RfExpV getObj();
      RfObjType getObjType() const;
      double getRHS(RfConP c) const;
      RfVarType getType(RfVarP v) const;
      double getUB(RfVarP v) const;
      RfVarPS getVars() const;
      RfVarPS getVarsArtificial() const;
      RfVarPS getVarsFixed() const;
      RfVarPS getVarsGlobal() const;
      RfVarPS getVarsLocal() const;
      RfVarPS getVarsRelaxed() const;
      RfVarPS getVarsZeroed() const;
      bool hasCon(RfConP v) const;
      bool hasGlobalVar(RfConP c) const;
      bool hasPresentVar(RfConP c) const;
      bool hasVar(RfVarP v) const;
      bool isArtificial(RfVarP v) const;
      bool isComplete(RfConP c) const;
      bool isFixed(RfVarP v) const;
      bool isGlobal(RfVarP v) const;
      bool isIncludedWithArtVar(RfConP c) const;
      bool isInt(RfVarP v) const;
      bool isLocal(RfVarP v) const;
      bool isMax() const;
      bool isMin() const;
      void init();
      bool isNotEmpty(RfConP c) const;
      bool isPresent(RfVarP v) const;
      bool isRelaxed(RfConP c) const;
      bool isRelaxed(RfVarP v) const;
      bool isVolatile(RfVarP v) const;
      bool isZeroed(RfVarP v) const;
      RfSubpro relax(RfVarP v);
      RfSubpro relax(RfVarPS &V);
      RfSubpro relax(RfConP c);
      RfSubpro relax(RfConPS &C);
      RfSubpro setBigM(RfConP c, double b);
      RfSubpro setLB(RfVarP var, double val);
      RfSubpro setRHS(RfConP c, double r);
      RfSubpro setUB(RfVarP var, double val);
      RfSubpro setVolatile(RfVarP v);
      RfSubpro setVolatile(RfVarPS &V);
      operator std::string();
    };

  }  // namespace Algo

}  // namespace Rf

#endif /* RFSUBPRO_HPP_ */
