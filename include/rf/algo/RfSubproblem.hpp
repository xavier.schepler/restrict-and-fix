#ifndef RFSUBPROBLEM_HPP_
#define RFSUBPROBLEM_HPP_

#include <memory>

#include "rf/model/RfMod.hpp"

namespace Rf
{

  namespace Algo
  {

    using namespace Rf::Model;

    typedef enum
    {
      /*
       * Omit incomplete cons
       */
      RfOmit,
      /*
       * Include incomplete cons adding artificial vars
       */
      RfInclude,
      /*
       * Include incomplete cons adding without artificial vars
       */      
      RfIncludeNoArtVars,
      /*
       * Relax-and-fix mode: all constraints are included,
       * global variables are continuous
       */
      RfRelax
    } RfSPMode;

    class RfSubproblem : public RfSolvable
    {
     private:
      std::unique_ptr<RfConPS> cons;
      std::unique_ptr<RfConsNums> consBigMs;
      std::unique_ptr<RfConsNums> consRHSs;
      std::unique_ptr<RfConPS> consGlobalPartial;
      std::unique_ptr<RfConL> consGlobalPartialStorage;
      std::unique_ptr<RfConPS> consRelaxed;
      std::unique_ptr<RfVarPDouble> LBs;
      RfSPMode mode;
      RfMod model;
      RfExpV obj;
      std::unique_ptr<RfVarPDouble> UBs;
      std::unique_ptr<RfVarPDouble> varsArtificialBigMs;
      std::unique_ptr<RfVarPS> vars;
      std::unique_ptr<RfVarL> varsArtificialStorage;
      std::unique_ptr<RfVarPS> varsArtificial;
      std::unique_ptr<RfVarPS> varsFixed;
      std::unique_ptr<RfVarPS> varsGlobal;
      std::unique_ptr<RfVarPS> varsLocal;
      std::unique_ptr<RfVarPS> varsRelaxed;
      std::unique_ptr<RfVarPDouble> varsVals;
      std::unique_ptr<RfVarPS> varsVolatile;
      std::unique_ptr<RfVarPS> varsZeroed;

     private:
      RfVarPS createZeroedVars(RfConPS &C);
      void handleModeRelax();
      void includeWithArtVars(RfConP c);

     public:
      RfSubproblem(RfMod ml, RfVarPS Vlocal, RfVarPS Vglobal, RfSPMode m = RfInclude);
      RfConPS createCons();
      RfVarPS createVars();
      void fix(RfVarP var, double val);
      double getBigM(RfConP c) const;
      RfConPS getCons() const;
      int getDegree() const;
      RfVarPDouble getFixeds() const;
      double getLB(RfVarP v) const;
      RfMod getModel() const;
      RfExpV getObj();
      RfObjType getObjType() const;
      double getRHS(RfConP c) const;
      RfVarType getType(RfVarP v) const;
      double getUB(RfVarP v) const;
      RfVarPS getVars() const;
      RfVarPS getVarsArtificial() const;
      RfVarPS getVarsFixed() const;
      RfVarPS getVarsGlobal() const;
      RfVarPS getVarsLocal() const;
      RfVarPS getVarsRelaxed() const;
      RfVarPS getVarsZeroed() const;
      bool hasCon(RfConP v) const;
      bool hasGlobalVar(RfConP c) const;
      bool hasPresentVar(RfConP c) const;
      bool hasVar(RfVarP v) const;
      void init();
      bool isArtificial(RfVarP v) const;
      bool isComplete(RfConP c) const;
      bool isFixed(RfVarP v) const;
      bool isGlobal(RfVarP v) const;
      bool isIncludedWithArtVar(RfConP c) const;
      bool isInt(RfVarP v) const;
      bool isLocal(RfVarP v) const;
      bool isMax() const;
      bool isMin() const;
      bool isNotEmpty(RfConP c) const;
      bool isPresent(RfVarP v) const;
      bool isRelaxed(RfConP c) const;
      bool isRelaxed(RfVarP v) const;
      bool isVolatile(RfVarP v) const;
      bool isZeroed(RfVarP v) const;
      void relax(RfVarP v);
      void relax(RfVarPS &V);
      void relax(RfConP c);
      void relax(RfConPS &C);
      void setBigM(RfConP c, double b);
      void setLB(RfVarP var, double val);
      void setRHS(RfConP c, double r);
      void setUB(RfVarP var, double val);
      void setVolatile(RfVarP v);
      void setVolatile(RfVarPS &V);
      operator std::string();
    };

  }  // namespace Algo

}  // namespace Rf

#endif /* RFSUBPROBLEM_HPP_ */
