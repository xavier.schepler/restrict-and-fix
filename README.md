# Restrict-and-Fix

* Builds a solution step by step by solving smaller mixed-integer subproblems. 
* Requires a partition of variables into subsets of local variables + one subset of global variables.
* Each subset of local variables corresponds to one subproblem. Subsequent subsets are zeroed.
* Incomplete constraints can either be omitted, or included with artificial variables.
* Global variables are present in each subproblem. 

The heuristic was proven to be competitive for production planning (e.g. lot sizing).
It also provided good results with vehicle routing, bin packing and portfolio optimization.

Examples are provided in this [folder](https://gitlab.com/xavier.schepler/restrict-and-fix/-/tree/master/example/), currently lot sizing and bin packing.

### Prerequisites

You will need:

* **Google OR Tools** - found at [https://developers.google.com/optimization/install](https://developers.google.com/optimization/install)

* **CMake v3.15+** - found at [https://cmake.org/](https://cmake.org/)

* **C++ Compiler** - needs to support at least the **C++17** standard, i.e. *MSVC*,
*GCC*, *Clang*

> ***Note:*** *You also need to be able to provide ***CMake*** a supported
[generator](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html).*


## Building the project with Ubuntu

First clone the repository, then use the following commands into its directory.

```
sudo apt-get update && sudo apt-get upgrade -y
sudo apt install -y build-essential ccache clang clang-format clang-tidy cmake cppcheck curl doxygen gcc graphviz make libgtest-dev lsb-release tar wget
wget https://github.com/google/or-tools/releases/download/v9.5/or-tools_amd64_ubuntu-22.04_cpp_v9.5.2237.tar.gz
tar zxvf or-tools_amd64_ubuntu-22.04_cpp_v9.5.2237.tar.gz
sudo mv or-tools_x86_64_Ubuntu-22.04_cpp_v9.5.2237 /opt/or-tools
mkdir build; cd build
cmake ..
sudo cmake --build . --target install
ctest -VV
```

More options that you can set for the project can be found in the
[`cmake/StandardSettings.cmake` file](cmake/StandardSettings.cmake). 

## Authors

* **Xavier Schepler**

## License

This project is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) - see the
[LICENSE](LICENSE) file for details
