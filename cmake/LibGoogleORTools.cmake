find_library(ORTOOLS ortools)
if(NOT ORTOOLS)
message(STATUS "or tools included from /opt/or-tools")
include_directories(${PROJECT_NAME} PRIVATE "/opt/or-tools/include")
link_directories("/opt/or-tools/lib")
include_directories(${PROJECT_NAME} PRIVATE "/usr/local/include")
link_directories("/usr/local/lib")
endif()
target_link_libraries(${PROJECT_NAME} ortools)
