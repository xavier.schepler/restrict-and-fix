#include "rf/solver/RfMPS.hpp"

#include <gtest/gtest.h>

#include "rf/model/RfMod.hpp"

namespace Rf
{

  namespace Solver
  {

    TEST(TestRfMPS, TestSimpleMIP)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= 0;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 0);
    }

    TEST(TestRfMPS, TestAddConReOpt)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 1);
      C[{ 1 }] = 1 * X[{ 0 }] <= 0;
      M.add(C[{ 1 }]);
      EXPECT_TRUE(S.solve(M));
      EXPECT_EQ(S.getValue(), 0);
    }

    TEST(TestRfMPS, TestRemConReOpt)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= 0;
      RfMod M;
      M.add(C).setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 0);
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      M = RfMod();
      M.add(C).setObj(RfMax, 1 * X[{ 0 }]);
      EXPECT_TRUE(S.solve(M));
      EXPECT_EQ(S.getValue(), 1);
    }

    TEST(TestRfMPS, TestChgVarBndReOpt)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      X[{ 0 }].setUB(0);
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      RfMod M;
      M.add(C).setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 0);
      X[{ 0 }].setUB(1);
      EXPECT_TRUE(S.solve(M));
      EXPECT_EQ(S.getValue(), 1);
    }

    TEST(TestRfMPS, TestChgVarTypeReOpt)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= 0.5;
      RfMod M;
      M.add(C).setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 0);
      X[{ 0 }].setType(RfReal);
      EXPECT_TRUE(S.solve(M));
      EXPECT_EQ(S.getValue(), 0.5);
    }

    TEST(TestRfMPS, TestAddVarReOpt)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      RfMod M;
      M.add(C).setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 1);
      X[{ 1 }] = RfVar("X1", RfBin);
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 1;
      M = RfMod();
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] + 2 * X[{ 1 }]);
      EXPECT_TRUE(S.solve(M));
      EXPECT_EQ(S.getValue(), 2);
      EXPECT_EQ(S.getValue(X[{ 0 }]), 0);
      EXPECT_EQ(S.getValue(X[{ 1 }]), 1);
    }

    TEST(TestRfMPS, TestChgLR)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= 0.5;
      RfMod M;
      M.add(C).setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 0);
      S.setLR(true);
      EXPECT_TRUE(S.solve(M));
      EXPECT_EQ(S.getValue(), 0.5);
      S.setLR(false);
      EXPECT_TRUE(S.solve(M));
      EXPECT_EQ(S.getValue(), 0);
    }

    TEST(TestRfMPS, TestInfeas)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= -1;
      RfMod M;
      M.add(C).setObj(RfMax, 1 * X[{ 0 }]);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_FALSE(S.solve());
    }

    TEST(TestRfMPS, TestOffset)
    {
      RfVarM X;
      RfConM C;
      X[{ 0 }] = RfVar("X0", RfBin);
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      RfMod M;
      M.add(C).setObj(RfMax, 1 + X[{ 0 }] + 2);
      RfMPS S(M, RfMPSolverSCIP);
      EXPECT_TRUE(S.solve());
      EXPECT_EQ(S.getValue(), 4);
    }

  }  // namespace Solver

}  // namespace Rf
