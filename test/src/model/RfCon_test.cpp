#include "rf/model/RfCon.hpp"

#include <gtest/gtest.h>

namespace Rf
{

  namespace Model
  {

    TEST(TestRfCon, TestRfCon)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfBin);
      RfExp e = -x0;
      RfCon c;
      e = (5 + x1) * 30 - 10 + (5 + x1) * (3 + x0) * 3;
      c = e == 0;
      EXPECT_EQ(e.getConstant(), c.getRHS() * -1);
    }

    TEST(TestRfCon, TestLTE)
    {
      RfVar x0("x0", RfBin);
      RfCon c = 1 * x0 <= 1;
      EXPECT_EQ(c.getType(), RfLTE);
      c = 1 * x0 <= 1.5;
      EXPECT_EQ(c.getType(), RfLTE);
    }

    TEST(TestRfCon, TestGTE)
    {
      RfVar x0("x0", RfBin);
      RfCon c = 1 * x0 >= 1;
      EXPECT_EQ(c.getType(), RfGTE);
      c = 1 * x0 >= 1.5;
      EXPECT_EQ(c.getType(), RfGTE);
    }

    TEST(TestRfCon, TestEqual)
    {
      RfVar x0("x0", RfBin);
      RfCon c = 1 * x0 == 1;
      EXPECT_EQ(c.getType(), RfEqual);
      c = 10 ==  1 *x0;
      EXPECT_EQ(c.getType(), RfEqual);
      c = 1 * x0 == 1.5;
      EXPECT_EQ(c.getType(), RfEqual);
      c = 10.5 == 1 * x0;
      EXPECT_EQ(c.getType(), RfEqual);
    }

    TEST(TestRfCon, TestDegree1)
    {
      RfVar x0("x0", RfBin);
      RfCon c = 1 * x0 == 1;
      EXPECT_EQ(c.getDegree(), 1);
    }

    TEST(TestRfCon, TestDegree2)
    {
      RfVar x0("x0", RfBin);
      RfCon c = 1 + x0 + 2 * x0 * x0 + 3 * x0 * x0 * x0 + 4 + 5 * x0 * x0 * x0 * x0 * x0 == 1;
      EXPECT_EQ(c.getDegree(), 5);
    }

    TEST(TestRfCon, TestDegree3)
    {
      RfVar x0("x0", RfBin);
      RfCon c = (1 + x0 + 2 * x0 * x0 + 3 * x0 * x0 * x0 + 4 + 5 * x0 * x0 * x0 * x0 * x0) * x0 * x0 == 1;
      EXPECT_EQ(c.getDegree(), 7);
    }

    TEST(TestRfCon, TestGetVariablesReal1)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfCon c = x0 * x1 == 1;
      EXPECT_EQ(c.getVarsReal().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfCon, TestGetVariablesReal2)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfCon c = x0 * x1 * x1 == 1;
      EXPECT_EQ(c.getVarsReal().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfCon, TestGetVariablesInt1)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfCon c = x0 * x1 == 1;
      EXPECT_EQ(c.getVarsInt().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfCon, TestGetVariablesInt2)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfCon c = x0 * x0 * x1 == 1;
      EXPECT_EQ(c.getVarsInt().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfCon, TestRfConWithNegativeCoefficients)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfCon c = -x0 - 2 * x1 - 3 * x0 * x1 == 1;
      std::string sc = static_cast<std::string>(c);
      EXPECT_FALSE(std::string::npos == sc.find("-1"));
      EXPECT_FALSE(std::string::npos == sc.find("-2"));
      EXPECT_FALSE(std::string::npos == sc.find("-3"));
    }

  }  // namespace Model

}  // namespace Rf
