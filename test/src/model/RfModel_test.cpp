#include "rf/model/RfModel.hpp"

#include <gtest/gtest.h>

namespace Rf
{

  namespace Model
  {

    TEST(TestRfModel, TestRfModel1)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfReal);
      X[{ 2 }] = RfVar("x2", RfInt);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 2;
      RfModel M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] * X[{ 1 }] + X[{ 2 }] * X[{ 3 }] * X[{ 4 }]);
      EXPECT_EQ(M.getVarsInt().size(), 4);
      EXPECT_EQ(M.getVarsReal().size(), 1);
      EXPECT_EQ(M.getVars().size(), 5);
      EXPECT_EQ(M.getDegree(), 3);
      EXPECT_EQ(M.getCons().size(), 1);
      EXPECT_TRUE(M.isMax());
      EXPECT_FALSE(M.isMin());
    }

    TEST(TestRfModel, TestRfModel2)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfReal);
      X[{ 2 }] = RfVar("x2", RfInt);
      X[{ 3 }] = RfVar("x3", RfReal);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 2;
      C[{ 1 }] = X[{ 1 }] * X[{ 1 }] * X[{ 2 }] * X[{ 3 }] * X[{ 4 }] <= 2;
      RfModel M;
      M.add(C);
      M.setObj(RfMin, 1 * X[{ 0 }] + 2 * X[{ 1 }]);
      EXPECT_EQ(M.getVarsInt().size(), 3);
      EXPECT_EQ(M.getVarsReal().size(), 2);
      EXPECT_EQ(M.getVars().size(), 5);
      EXPECT_EQ(M.getDegree(), 5);
      EXPECT_EQ(M.getCons().size(), 2);
      EXPECT_TRUE(M.isMin());
      EXPECT_FALSE(M.isMax());
    }

  }  // namespace Model

}  // namespace Rf
