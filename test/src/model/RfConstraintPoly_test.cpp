#include "rf/model/RfConstraintPoly.hpp"

#include <gtest/gtest.h>

#include "rf/exp/RfExpL.hpp"

namespace Rf
{

  namespace Model
  {

    TEST(TestRfConstraintPoly, TestRfConstraintPoly)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfBin);
      RfExp e = -x0;
      e = (5 + x1) * 30 - 10 + (5 + x1) * (3 + x0) * 3;
      RfConstraintPoly c(RfEqual, e, 0);
      EXPECT_EQ(e.getConstant(), c.getRHS() * -1);
    }

    TEST(TestRfConstraintPoly, TestLTE)
    {
      RfVar x0("x0", RfBin);
      RfExp e = x0;
      RfConstraintPoly c(RfLTE, e, 1);
      EXPECT_EQ(c.getType(), RfLTE);
    }

    TEST(TestRfConstraintPoly, TestGTE)
    {
      RfVar x0("x0", RfBin);
      RfExp e = x0;
      RfConstraintPoly c(RfGTE, e, 1);
      EXPECT_EQ(c.getType(), RfGTE);
    }

    TEST(TestRfConstraintPoly, TestEqual)
    {
      RfVar x0("x0", RfBin);
      RfExp e = x0;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getType(), RfEqual);
    }

    TEST(TestRfConstraintPoly, TestDegree1)
    {
      RfVar x0("x0", RfBin);
      RfExp e = x0;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getDegree(), 1);
    }

    TEST(TestRfConstraintPoly, TestDegree2)
    {
      RfVar x0("x0", RfBin);
      RfExp e = 1 + x0 + 2 * x0 * x0 + 3 * x0 * x0 * x0 + 4 + 5 * x0 * x0 * x0 * x0 * x0;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getDegree(), 5);
    }

    TEST(TestRfConstraintPoly, TestDegree3)
    {
      RfVar x0("x0", RfBin);
      RfExp e = (1 + x0 + 2 * x0 * x0 + 3 * x0 * x0 * x0 + 4 + 5 * x0 * x0 * x0 * x0 * x0) * x0 * x0;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getDegree(), 7);
    }

    TEST(TestRfConstraintPoly, TestGetVariablesReal1)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfExp e = x0 * x1;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getVarsReal().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfConstraintPoly, TestGetVariablesReal2)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfExp e = x0 * x1 * x1;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getVarsReal().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfConstraintPoly, TestGetVariablesInt1)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfExp e = x0 * x1;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getVarsInt().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfConstraintPoly, TestGetVariablesInt2)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfExp e = x0 * x0 * x1;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_EQ(c.getVarsInt().size(), 1);
      EXPECT_EQ(c.getVars().size(), 2);
    }

    TEST(TestRfConstraintPoly, TestNegativeCoefficients)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfExp e = -x0 - 2 * x1 - 3 * x0 * x1;
      RfConstraintPoly c(RfEqual, e, 1);
      std::string sc = static_cast<std::string>(c);
      EXPECT_FALSE(std::string::npos == sc.find("-1"));
      EXPECT_FALSE(std::string::npos == sc.find("-2"));
      EXPECT_FALSE(std::string::npos == sc.find("-3"));
    }

    TEST(TestRfConstraintPoly, TestHasVar1)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfExp e = x0;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_TRUE(c.hasVarP(x0));
      EXPECT_FALSE(c.hasVarP(x1));
    }

    TEST(TestRfConstraintPoly, TestHasVar2)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfVar x2("x2", RfReal);
      RfVar x3("x3", RfReal);
      RfExp e = x0 + x1 + x2 * x3;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_TRUE(c.hasVarP(x0));
      EXPECT_TRUE(c.hasVarP(x1));
      EXPECT_TRUE(c.hasVarP(x2));
      EXPECT_TRUE(c.hasVarP(x3));
    }

    TEST(TestRfConstraintPoly, TestHasVar3)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfVar x2("x2", RfReal);
      RfVar x3("x3", RfReal);
      RfExp e = x0 + x1 * x2;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_TRUE(c.hasVarP(x0));
      EXPECT_TRUE(c.hasVarP(x1));
      EXPECT_TRUE(c.hasVarP(x2));
      EXPECT_FALSE(c.hasVarP(x3));
    }

    TEST(TestRfConstraintPoly, TestHasVar4)
    {
      RfVar x0("x0", RfBin);
      RfVar x1("x1", RfReal);
      RfVar x2("x2", RfReal);
      RfVar x3("x3", RfReal);
      RfVar x4("x4", RfBin);
      RfVar x5("x5", RfReal);
      RfVar x6("x6", RfReal);
      RfVar x7("x7", RfReal);
      RfExp e = x0 + x1 * x2 + x0 + x1 * x2 * x3 + x4;
      RfConstraintPoly c(RfEqual, e, 1);
      EXPECT_TRUE(c.hasVarP(x0));
      EXPECT_TRUE(c.hasVarP(x1));
      EXPECT_TRUE(c.hasVarP(x2));
      EXPECT_TRUE(c.hasVarP(x3));
      EXPECT_TRUE(c.hasVarP(x4));
      EXPECT_FALSE(c.hasVarP(x5));
      EXPECT_FALSE(c.hasVarP(x6));
      EXPECT_FALSE(c.hasVarP(x7));
    }

  }  // namespace Model

}  // namespace Rf
