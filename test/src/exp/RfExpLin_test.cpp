#include "rf/exp/RfExpLin.hpp"

#include <gtest/gtest.h>

namespace Rf
{

  namespace Model
  {

    TEST(TestRfExpLin, TestIntConstructor)
    {
      int i = 1;
      RfExpLin el;
      el = i;
      EXPECT_EQ(el.getConstant(), i);
    }

    TEST(TestRfExpLin, TestDoubleConstructor)
    {
      double d = 3.14;
      RfExpLin e;
      e = d;
      EXPECT_EQ(e.getConstant(), d);
    }

    TEST(TestRfExpLin, TestVarConstructor)
    {
      RfVar x("x", RfBin);
      RfExpLin e(x);
      EXPECT_EQ(e.get(x), 1);
    }

    TEST(TestRfExpLin, TestListConstructor1)
    {
      int i = 1;
      RfExp e = i;
      RfExpLin el = e;
      EXPECT_EQ(el.getConstant(), i);
    }

    TEST(TestRfExpLin, TestListConstructor2)
    {
      RfVar x("x", RfBin);
      RfExp e = x;
      RfExpLin el = e;
      EXPECT_EQ(el.get(x), 1);
    }

    TEST(TestRfExpLin, TestListConstructor3)
    {
      RfVar x("x", RfBin);
      RfExp e = x + 1;
      RfExpLin el = e;
      EXPECT_EQ(el.get(x), 1);
      EXPECT_EQ(el.getConstant(), 1);
    }

    TEST(TestRfExpLin, TestCompoundPlusOperator1)
    {
      int i = 1;
      RfExpLin el;
      el += i;
      EXPECT_EQ(el.getConstant(), i);
    }

    TEST(TestRfExpLin, TestCompoundPlusOperator2)
    {
      double d = 3.14;
      RfExpLin el;
      el += d;
      EXPECT_EQ(el.getConstant(), d);
    }

    TEST(TestRfExpLin, TestCompoundPlusOperator3)
    {
      int i = 1;
      RfExpLin el;
      el += i;
      EXPECT_EQ(el.getConstant(), 1);
    }

    TEST(TestRfExpLin, TestCompoundMinusOperator1)
    {
      int i = 1;
      RfExpLin el;
      el -= i;
      EXPECT_EQ(el.getConstant(), -i);
    }

    TEST(TestRfExpLin, TestCompoundMinusOperator2)
    {
      double d = 3.14;
      RfExpLin el;
      el -= d;
      EXPECT_EQ(el.getConstant(), -d);
    }

    TEST(TestRfExpLin, TestCompoundMinusOperator3)
    {
      int i = 1;
      RfExpLin el;
      el -= i;
      EXPECT_EQ(el.getConstant(), -1);
    }

    TEST(TestRfExpLin, TestAdd)
    {
      RfVar x0("x0", RfBin), x1("x1", RfBin);
      RfExpLin el1 = 1 * x0 + 2, el2 = 3 * x1 + 4;
      el1.add(el2);
      EXPECT_EQ(el1.getConstant(), 6);
      EXPECT_EQ(el1.get(x0), 1);
      EXPECT_EQ(el1.get(x1), 3);
    }

    TEST(TestRfExpLin, TestSubstract)
    {
      RfVar x0("x0", RfBin), x1("x1", RfBin);
      RfExpLin el1 = 1 * x0 + 2, el2 = 3 * x1 + 4;
      el1.substract(el2);
      EXPECT_EQ(el1.getConstant(), -2);
      EXPECT_EQ(el1.get(x0), 1);
      EXPECT_EQ(el1.get(x1), -3);
    }

  }  // namespace Model

  // namespace Model

}  // namespace Rf
