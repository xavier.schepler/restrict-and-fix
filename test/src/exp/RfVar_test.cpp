#include "rf/exp/RfVar.hpp"

#include <gtest/gtest.h>

#include "rf/model/RfMap.hpp"

namespace Rf
{

  namespace Model
  {

    TEST(TestRfVar, TestReal1)
    {
      std::string name("x0");
      RfVar x(name, RfReal);
      EXPECT_EQ(x.getLB(), -RfInfinity);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfReal);
      EXPECT_EQ(x.getName(), name);
      EXPECT_TRUE(x.isReal());
      EXPECT_FALSE(x.isInt());
    }

    TEST(TestRfVar, TestReal2)
    {
      double lb = 3.14;
      std::string name("x0");
      RfVar x(name, RfReal, lb);
      EXPECT_EQ(x.getLB(), lb);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfReal);
      EXPECT_EQ(x.getName(), name);
      EXPECT_TRUE(x.isReal());
      EXPECT_FALSE(x.isInt());
    }

    TEST(TestRfVar, TestInteger1)
    {
      std::string name("x0");
      RfVar x(name, RfInt);
      EXPECT_EQ(x.getLB(), -RfInfinity);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfInt);
      EXPECT_EQ(x.getName(), name);
      EXPECT_FALSE(x.isReal());
      EXPECT_TRUE(x.isInt());
    }

    TEST(TestRfVar, TestInteger2)
    {
      std::string name("x0");
      RfVar x(name, RfInt);
      EXPECT_EQ(x.getLB(), -RfInfinity);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfInt);
      EXPECT_EQ(x.getName(), name);
      EXPECT_FALSE(x.isReal());
      EXPECT_TRUE(x.isInt());
    }

    TEST(TestRfVar, TestBin)
    {
      RfVar x("x0", RfBin);
      EXPECT_EQ(x.getLB(), 0);
      EXPECT_EQ(x.getUB(), 1);
      EXPECT_EQ(x.getType(), RfBin);
      EXPECT_FALSE(x.isReal());
      EXPECT_TRUE(x.isInt());
    }

    TEST(TestRfVar, TestRfVarM1)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfInt);
      EXPECT_TRUE(X[{ 0 }].isInt());
      EXPECT_TRUE(X[{ 1 }].isInt());
    }

    TEST(TestRfVar, TestRfVarM2)
    {
      RfVarM X;
      X[{ 0, 0 }] = RfVar("x0", RfBin);
      X[{ 0, 1 }] = RfVar("x1", RfInt);
      RfVar v;
      v = X[{ 0, 0 }];
      EXPECT_TRUE(v.isInt());
      v = X[{ 0, 1 }];
      EXPECT_TRUE(v.isInt());
    }

  }  // namespace Model

}  // namespace Rf
