#include "rf/exp/RfExpL.hpp"

#include <gtest/gtest.h>

#include <iostream>
#include <sstream>

#include "rf/model/RfMap.hpp"

namespace Rf
{

  namespace Model
  {

    int count_occurences(const std::string &str, const std::string &substr)
    {
      int o = 0;
      std::string::size_type pos = 0;
      while ((pos = str.find(substr, pos)) != std::string::npos)
      {
        ++o;
        pos += substr.length();
      }
      return o;
    }

    TEST(TestRfExp, TestDouble)
    {
      double d = 3.14;
      RfExp e = d;
      EXPECT_EQ(e.getConstant(), d);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestInt)
    {
      int i = 1;
      RfExp e = i;
      EXPECT_EQ(e.getConstant(), 1);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestVariable)
    {
      RfVar x("x", RfBin);
      RfExp e = x;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestNullptr)
    {
      RfExp e;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 0);
    }

    TEST(TestRfExp, TestTree)
    {
      RfVar x("x", RfBin);
      RfExp e = 1 * x;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestOperatorPlus1)
    {
      RfVar x("x0", RfBin);
      RfExp e;
      e = e + x;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestOperatorPlus2)
    {
      double d(3.14);
      RfExp e;
      e = e + d;
      EXPECT_EQ(e.getConstant(), d);
      EXPECT_EQ(e.size(), 1);
      RfExp f;
      f = d + f;
      EXPECT_EQ(f.getConstant(), d);
      EXPECT_EQ(f.size(), 1);
    }

    TEST(TestRfExp, TestOperatorPlus3)
    {
      int i(1);
      RfExp e;
      e = e + i;
      EXPECT_EQ(e.getConstant(), i);
      EXPECT_EQ(e.size(), 1);
      RfExp f;
      f = i + f;
      EXPECT_EQ(f.getConstant(), i);
      EXPECT_EQ(f.size(), 1);
    }

    TEST(TestRfExp, TestOperatorMinus1)
    {
      RfVar x("x0", RfBin);
      RfExp e;
      e = e - x;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 1);
      RfExp f;
      f = x - f;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestOperatorMinus2)
    {
      double d(3.14);
      RfExp e;
      e = e - d;
      EXPECT_EQ(e.getConstant(), -d);
      EXPECT_EQ(e.size(), 1);
      RfExp f;
      f = d - f;
      EXPECT_EQ(f.getConstant(), d);
      EXPECT_EQ(f.size(), 1);
    }

    TEST(TestRfExp, TestOperatorMinus3)
    {
      int i(1);
      RfExp e;
      e = e - i;
      EXPECT_EQ(e.getConstant(), -i);
      EXPECT_EQ(e.size(), 1);
      RfExp f;
      f = i - f;
      EXPECT_EQ(f.getConstant(), i);
      EXPECT_EQ(f.size(), 1);
    }

    TEST(TestRfExp, TestOperatorTimes1)
    {
      RfVar x("x0", RfBin);
      RfExp e;
      e = e * x;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 0);
      RfExp f;
      f = x * f;
      EXPECT_EQ(f.getConstant(), 0);
      EXPECT_EQ(f.size(), 0);
    }

    TEST(TestRfExp, TestOperatorTimes2)
    {
      double d(3.14);
      RfExp e;
      e = e * d;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 0);
      RfExp f;
      f = d * f;
      EXPECT_EQ(f.getConstant(), 0);
      EXPECT_EQ(f.size(), 0);
    }

    TEST(TestRfExp, TestOperatorTimes3)
    {
      int i(1);
      RfExp e;
      e = e * i;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 0);
      RfExp f;
      f = i * f;
      EXPECT_EQ(f.getConstant(), 0);
      EXPECT_EQ(f.size(), 0);
    }

    TEST(TestRfExp, TestRfCompoundOperatorPlus1)
    {
      RfVar x("x0", RfBin);
      RfExp e;
      e += x;
      EXPECT_EQ(e.getConstant(), 0);
      EXPECT_EQ(e.size(), 1);
      EXPECT_EQ(e.getDegree(), 1);
    }

    TEST(TestRfExp, TestRfCompoundOperatorPlus2)
    {
      int i = 1;
      RfExp e;
      e += i;
      EXPECT_EQ(e.getConstant(), i);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestRfCompoundOperatorPlus3)
    {
      double d = 3.14;
      RfExp e;
      e += d;
      EXPECT_EQ(e.getConstant(), d);
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestRfCompoundOperatorPlus4)
    {
      RfVar x("x0", RfBin);
      RfExp e = x;
      e += 1;
      EXPECT_EQ(e.getConstant(), 1);
      EXPECT_EQ(e.size(), 2);
    }

    TEST(TestRfExp, TestDegree1)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 0 }];
      EXPECT_EQ(e.getDegree(), 1);
    }

    TEST(TestRfExp, TestDegree2)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 0 }] * X[{ 1 }];
      EXPECT_EQ(e.getDegree(), 2);
    }

    TEST(TestRfExp, TestDegree3)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 0 }] * X[{ 1 }] + X[{ 0 }];
      EXPECT_EQ(e.getDegree(), 2);
    }

    TEST(TestRfExp, TestDegree4)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 0 }] + 2 * X[{ 1 }] + 3 + X[{ 0 }];
      EXPECT_EQ(e.getDegree(), 1);
    }

    TEST(TestRfExp, TestDegree5)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 0 }];
      for (int i = 1; i < 10; i++)
      {
        std::ostringstream oss;
        oss << "x_" << i;
        X[{ i }] = RfVar(oss.str(), RfBin);
        e += e * X[{ i }];
        EXPECT_EQ(e.getDegree(), i + 1);
      }
    }

    TEST(TestRfExp, TestDegree6)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = X[{ 0 }];
      e *= X[{ 1 }];
      e *= (X[{ 0 }] * X[{ 1 }]);
      e = e * X[{ 0 }];
      EXPECT_EQ(e.getDegree(), 5);
    }

    TEST(TestRfExp, TestDegree7)
    {
      RfExp e = 1;
      EXPECT_EQ(e.getDegree(), 0);
    }

    TEST(TestRfExp, TestDegree8)
    {
      RfExp e = 3.14;
      EXPECT_EQ(e.getDegree(), 0);
    }

    TEST(TestRfExp, TestDegree9)
    {
      RfVar v = RfVar("x0", RfBin);
      RfExp e = v;
      EXPECT_EQ(e.getDegree(), 1);
    }

    TEST(TestRfExp, TestStringConversion)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 2 * X[{ 0 }] + 3 * X[{ 1 }] + 4 * X[{ 0 }] * X[{ 1 }] + 5 + 6 + 7 * X[{ 0 }] * X[{ 1 }];
      std::string s = e;
      EXPECT_FALSE(std::string::npos == s.find("2"));
      EXPECT_FALSE(std::string::npos == s.find("3"));
      EXPECT_FALSE(std::string::npos == s.find("4"));
      EXPECT_FALSE(std::string::npos == s.find("5"));
      EXPECT_FALSE(std::string::npos == s.find("6"));
      EXPECT_FALSE(std::string::npos == s.find("7"));
      EXPECT_EQ(count_occurences(s, "x0"), 3);
      EXPECT_EQ(count_occurences(s, "x1"), 3);
      EXPECT_EQ(count_occurences(s, "*"), 6);
      EXPECT_EQ(count_occurences(s, "+"), 5);
    }

    TEST(TestRfExp, TestSize1)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 1 }] * X[{ 0 }];
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestSize2)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 0 }] + 2 * X[{ 1 }] + X[{ 0 }] + 3;
      EXPECT_EQ(e.size(), 4);
    }

    TEST(TestRfExp, TestSize3)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 0 * X[{ 1 }] + 1 + 2 * X[{ 0 }] + 3 * X[{ 0 }] + 4;
      EXPECT_EQ(e.size(), 5);
    }

    TEST(TestRfExp, TestSize4)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1 * X[{ 0 }] + 2 + 3 * X[{ 1 }] + X[{ 0 }];
      EXPECT_EQ(e.size(), 4);
    }

    TEST(TestRfExp, TestSize5)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      RfExp e = 0 * X[{ 0 }];
      for (int i = 1; i < 10; i++)
      {
        std::ostringstream oss;
        oss << "x_" << i;
        X[{ i }] = RfVar(oss.str(), RfBin);
        e = e + i * X[{ i }];
        EXPECT_EQ(e.size(), i + 1);
      }
    }

    TEST(TestRfExp, TestSize6)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = (1 * X[{ 0 }] + 2) * (3 * X[{ 1 }] + 4) + 5;
      EXPECT_EQ(e.size(), 5);
    }

    TEST(TestRfExp, TestSize7)
    {
      RfExp e = 1;
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestSize8)
    {
      RfVar v("x0", RfBin);
      RfExp e = v;
      EXPECT_EQ(e.size(), 1);
    }

    TEST(TestRfExp, TestSizeByDegree1)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 3 * X[{ 1 }] * X[{ 0 }];
      RfExpL elts = e.getPolyAddendsByDegree(2);
      EXPECT_EQ(elts.size(), 1);
      elts = e.getPolyAddendsByDegree(0);
      EXPECT_EQ(elts.size(), 0);
      elts = e.getPolyAddendsByDegree(10);
      EXPECT_EQ(elts.size(), 0);
    }

    TEST(TestRfExp, TestSizeByDegree2)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 1;
      e += 1 * X[{ 0 }] + X[{ 1 }];
      e += 2 * X[{ 0 }] * X[{ 1 }] + 2 * X[{ 1 }] * X[{ 0 }];
      e *= X[{ 0 }];
      RfExpL elts = e.getPolyAddendsByDegree(3);
      EXPECT_EQ(elts.size(), 1);
      elts = e.getPolyAddendsByDegree(1);
      EXPECT_EQ(elts.size(), 1);
      e += 2 * X[{ 1 }];
      elts = e.getPolyAddendsByDegree(1);
      EXPECT_EQ(elts.size(), 2);
      e *= X[{ 1 }];
      elts = e.getPolyAddendsByDegree(4);
      EXPECT_EQ(elts.size(), 1);
      elts = e.getPolyAddendsByDegree(100);
      EXPECT_EQ(elts.size(), 0);
    }

    TEST(TestRfExp, TestSizeByDegree3)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExpL elts;
      RfExp e = 1 * X[{ 0 }];
      for (int i = 1; i < 10; i++)
      {
        std::ostringstream oss;
        oss << "x_" << i;
        X[{ i }] = RfVar(oss.str(), RfBin);
        RfExp e2 = 1 * X[{ 0 }];
        for (int j = 1; j <= i; j++)
        {
          e2 *= X[{ j }];
        }
        e += e2;
        elts = e.getPolyAddendsByDegree(i + 1);
        EXPECT_EQ(elts.size(), 1);
        e2 = 1 * X[{ 0 }];
        for (int j = 1; j <= i; j++)
        {
          e2 *= X[{ j }];
        }
        e += e2;
        elts = e.getPolyAddendsByDegree(i + 1);
        EXPECT_EQ(elts.size(), 1);
      }
    }

    TEST(TestRfExp, TestGetConstant1)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 2 * X[{ 0 }] + 3 * X[{ 1 }] - 5;
      double c = e.getConstant();
      EXPECT_EQ(c, -5);
    }

    TEST(TestRfExp, TestGetConstant2)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 8 * X[{ 0 }] - 5 + 2 * X[{ 1 }] + 5;
      double c = e.getConstant();
      EXPECT_EQ(c, 0);
    }

    TEST(TestRfExp, TestGetConstant3)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e = 3 + 7 * X[{ 0 }] - 4 + 1 * X[{ 1 }] + 5;
      double c = e.getConstant();
      EXPECT_EQ(c, 4);
    }

    TEST(TestRfExp, TestGetConstant4)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfExp e1 = 1 * X[{ 0 }] + 2 * X[{ 0 }] * X[{ 1 }] + 3 + X[{ 0 }] + 4 * X[{ 1 }] * X[{ 0 }];
      e1 += 5;
      double c = e1.getConstant();
      EXPECT_EQ(c, 8);
      RfExp e2 = 1 * X[{ 0 }] + 2 * X[{ 0 }] * X[{ 1 }] + 3 + X[{ 0 }] + 4 * X[{ 1 }] * X[{ 0 }];
      e2 += 5;
      e2 -= 6;
      c = e2.getConstant();
      EXPECT_EQ(c, 2);
      c = (e1 * e2).getConstant();
      EXPECT_EQ(c, 16);
      c = (e1 * e2 - 10).getConstant();
      EXPECT_EQ(c, 6);
    }

    TEST(TestRfExp, TestGetConstant5)
    {
      double d(3.14);
      RfExp e = d;
      double c = e.getConstant();
      EXPECT_EQ(c, d);
    }

    TEST(TestRfExp, TestGetConstant6)
    {
      RfVar v("x0", RfBin);
      RfExp e = v;
      double c = e.getConstant();
      EXPECT_EQ(c, 0);
    }

    TEST(TestRfExp, TestSortAndReduce)
    {
      RfVar x0("x0", RfBin), x1("x1", RfBin);
      RfExp e1, e2;
      size_t s1, s2;
      for (int i = 0; i < 20; i++)
      {
        e1 += 1 * x0 + i + 1 * x1;
      }
      e1.sortAndReduce();
      s1 = e1.size();
      for (int i = 0; i < 20; i++)
      {
        e2 += 1 * x0 + i + 1 * x1;
        e2.sortAndReduce();
      }
      s2 = e2.size();
      ASSERT_EQ(s1, s2);
    }

  }  // namespace Model

}  // namespace Rf
