#include "rf/exp/RfVariable.hpp"

#include <gtest/gtest.h>

namespace Rf
{

  namespace Model
  {

    TEST(TestRfVariable, TestReal1)
    {
      std::string name("x0");
      RfVariable x(name, RfReal);
      EXPECT_EQ(x.getLB(), -RfInfinity);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfReal);
      EXPECT_EQ(x.getName(), name);
      EXPECT_TRUE(x.isReal());
      EXPECT_FALSE(x.isInt());
    }

    TEST(TestRfVariable, TestReal2)
    {
      double lb = 3.14;
      std::string name("x0");
      RfVariable x(name, RfReal, lb);
      EXPECT_EQ(x.getLB(), lb);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfReal);
      EXPECT_EQ(x.getName(), name);
      EXPECT_TRUE(x.isReal());
      EXPECT_FALSE(x.isInt());
    }

    TEST(TestRfVariable, TestInteger1)
    {
      std::string name("x0");
      RfVariable x(name, RfInt);
      EXPECT_EQ(x.getLB(), -RfInfinity);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfInt);
      EXPECT_EQ(x.getName(), name);
      EXPECT_FALSE(x.isReal());
      EXPECT_TRUE(x.isInt());
    }

    TEST(TestRfVariable, TestInteger2)
    {
      std::string name("x0");
      RfVariable x(name, RfInt);
      EXPECT_EQ(x.getLB(), -RfInfinity);
      EXPECT_EQ(x.getUB(), +RfInfinity);
      EXPECT_EQ(x.getType(), RfInt);
      EXPECT_EQ(x.getName(), name);
      EXPECT_FALSE(x.isReal());
      EXPECT_TRUE(x.isInt());
    }

    TEST(TestRfVariable, TestBin)
    {
      RfVariable x("x0", RfBin);
      EXPECT_EQ(x.getLB(), 0);
      EXPECT_EQ(x.getUB(), 1);
      EXPECT_EQ(x.getType(), RfBin);
      EXPECT_FALSE(x.isReal());
      EXPECT_TRUE(x.isInt());
    }

  }  // namespace Model

}  // namespace Rf
