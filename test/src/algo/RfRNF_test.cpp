#include "rf/algo/RfRNF.hpp"

#include <gtest/gtest.h>

namespace Rf
{

  namespace Algo
  {

    TEST(TestRfRNF, TestOmit)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      X[{ 2 }] = RfVar("x2", RfBin);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 3;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 4;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 5;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }]);
      RfVarPS Vloc, Vglob;
      Vloc.insert(X[{ 0 }]);
      RfRNF rnf(M, RfOmit, RfMPSolverSCIP);
      for (int i = 1; i <= 4; i++)
      {
        rnf.setSolverRebuildModel(false);
        Vloc.insert(X[{ i }]);
        rnf.addLocal(Vloc).createCurrent();
        RfSubpro SP(rnf.getCurrent());
        EXPECT_EQ(SP.createCons().size(), i);
        EXPECT_EQ(SP.getVarsArtificial().size(), 0);
        EXPECT_EQ(SP.getVarsGlobal().size(), 0);
        EXPECT_EQ(SP.getVarsLocal().size(), i + 1);
        EXPECT_EQ(SP.getVarsRelaxed().size(), 0);
        EXPECT_EQ(SP.getVarsZeroed().size(), 5 - i - 1);
        EXPECT_TRUE(rnf.solveAndFix());
        EXPECT_EQ(rnf.getGapRelative(), 0);
        EXPECT_TRUE(rnf.hasCurrentSol());
        EXPECT_EQ(rnf.getValue(), i + 1);
        EXPECT_EQ(rnf.getValue(X[{ i }]), 1);
        if (i < 4)
        {
          ASSERT_FALSE(rnf.areAllFixed());
        }
      }
      ASSERT_TRUE(rnf.areAllFixed());
    }

    TEST(TestRfRNF, TestInclude)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfReal);
      X[{ 2 }] = RfVar("x2", RfInt);
      X[{ 3 }] = RfVar("x3", RfReal);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 3;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 4;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 5;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, 1 * X[{ 0 }] + 2 * X[{ 1 }]);
      RfVarPS Vloc, Vglob;
      Vloc.insert(X[{ 0 }]);
      RfRNF rnf(M, RfInclude, RfMPSolverSCIP);
      rnf.addLocal(Vloc).createCurrent();
      RfSubpro SP(rnf.getCurrent());
      EXPECT_EQ(SP.createCons().size(), 4);
      EXPECT_EQ(SP.createVars().size(), 9);
      EXPECT_EQ(SP.getVarsArtificial().size(), 4);
      EXPECT_EQ(SP.getVarsGlobal().size(), 0);
      EXPECT_EQ(SP.getVarsLocal().size(), 1);
      EXPECT_EQ(SP.getVarsRelaxed().size(), 0);
      EXPECT_EQ(SP.getVarsZeroed().size(), 4);
      EXPECT_EQ(SP.getLB(X[{ 0 }]), 0);
      EXPECT_EQ(SP.getUB(X[{ 0 }]), 1);
      EXPECT_EQ(SP.getLB(X[{ 1 }]), 0);
      EXPECT_EQ(SP.getUB(X[{ 1 }]), 0);
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_EQ(rnf.getGapRelative(), 0);
      EXPECT_TRUE(rnf.hasCurrentSol());
      EXPECT_EQ(rnf.getValue(), 1);
      ASSERT_FALSE(rnf.areAllFixed());
      rnf.createCurrent();
      EXPECT_TRUE(rnf.solveAndFix());
      ASSERT_TRUE(rnf.areAllFixed());
    }

    TEST(TestRfRNF, TestRelax)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfReal);
      X[{ 2 }] = RfVar("x2", RfInt);
      X[{ 3 }] = RfVar("x3", RfReal);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 3;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 4;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 5;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, 1 * X[{ 0 }] + 2 * X[{ 1 }]);
      RfVarPS Vloc, Vglob;
      Vloc.insert(X[{ 0 }]);
      RfRNF rnf(M, RfRelax, RfMPSolverSCIP);
      rnf.addLocal(Vloc).createCurrent();
      EXPECT_EQ(rnf.getCurrentNum(), 0);
      EXPECT_EQ(rnf.getCount(), 1);
      RfSubpro SP(rnf.getCurrent());
      EXPECT_EQ(SP.createCons().size(), 4);
      EXPECT_EQ(SP.createVars().size(), 5);
      EXPECT_EQ(SP.getVarsArtificial().size(), 0);
      EXPECT_EQ(SP.getVarsGlobal().size(), 4);
      EXPECT_EQ(SP.getVarsLocal().size(), 1);
      EXPECT_EQ(SP.getVarsRelaxed().size(), 4);
      EXPECT_EQ(SP.getVarsZeroed().size(), 0);
      EXPECT_EQ(SP.getLB(X[{ 0 }]), 0);
      EXPECT_EQ(SP.getUB(X[{ 0 }]), 1);
      EXPECT_TRUE(SP.isInt(X[{ 0 }]));
      EXPECT_FALSE(SP.isInt(X[{ 1 }]));
      EXPECT_FALSE(SP.isInt(X[{ 2 }]));
      EXPECT_FALSE(SP.isInt(X[{ 3 }]));
      EXPECT_FALSE(SP.isInt(X[{ 4 }]));
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_EQ(rnf.getGapRelative(), 0);
      EXPECT_TRUE(rnf.hasCurrentSol());
      EXPECT_EQ(rnf.getValue(), 4);
      rnf.createCurrent();
      EXPECT_EQ(rnf.getCurrentNum(), 1);
      ASSERT_FALSE(rnf.areAllFixed());
    }

    TEST(TestRfRNF, TestValues)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      X[{ 2 }] = RfVar("x2", RfBin);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 3;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 4;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 4;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] + 2 * X[{ 1 }] + 3 * X[{ 2 }] + 4 * X[{ 3 }] + 5 * X[{ 4 }]);
      RfVarPS Vloc, Vglob;
      Vloc.insert(X[{ 0 }]);
      RfRNF rnf(M, RfOmit, RfMPSolverSCIP);
      int v = 1;
      for (int i = 1; i <= 4; i++)
      {
        if (i <= 3)
          v += i + 1;
        Vloc.insert(X[{ i }]);
        rnf.addLocal(Vloc).createCurrent();
        EXPECT_EQ(rnf.getCurrentNum(), static_cast<size_t>(i - 1));
        RfSubpro SP(rnf.getCurrent());
        EXPECT_EQ(SP.createCons().size(), i);
        EXPECT_EQ(SP.getVarsArtificial().size(), 0);
        EXPECT_EQ(SP.getVarsGlobal().size(), 0);
        EXPECT_EQ(SP.getVarsLocal().size(), i + 1);
        EXPECT_EQ(SP.getVarsRelaxed().size(), 0);
        EXPECT_EQ(SP.getVarsZeroed().size(), 5 - i - 1);
        EXPECT_TRUE(rnf.solveAndFix());
        EXPECT_EQ(rnf.getGapRelative(), 0);
        EXPECT_TRUE(rnf.hasCurrentSol());
        EXPECT_EQ(rnf.getValue(), v);
      }

      for (int i = 0; i <= 3; i++)
        EXPECT_EQ(rnf.getValue(X[{ i }]), 1);
      EXPECT_EQ(rnf.getValue(X[{ 4 }]), 0);
      v = 1;
      for (int i = 0; i < 4; i++)
      {
        if (i <= 2)
          v += i + 2;
        EXPECT_EQ(rnf.getValue(static_cast<size_t>(i)), v);
      }
    }

    TEST(TestRfRNF, TestSolveAll)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      X[{ 2 }] = RfVar("x2", RfBin);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = 1 *X[{ 0 }] <= 1;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 3;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 4;
      C[{ 4 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 5;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }]);
      RfRNF rnf(M, RfOmit, RfMPSolverSCIP);
      for (int i = 0; i <= 4; i++)
      {
        RfVarPS Vloc;
        Vloc.insert(X[{ i }]);
        rnf.addLocal(Vloc);
      }
      EXPECT_TRUE(rnf.solveAndFixAll());
      EXPECT_EQ(rnf.getGapRelative(), 0);
      EXPECT_EQ(rnf.getValue(), 5);
      for (int i = 0; i <= 4; i++)
      {
        EXPECT_EQ(rnf.getValue(static_cast<size_t>(i)), i + 1);
        EXPECT_EQ(rnf.getValue(X[{ i }]), 1);
      }
    }

    TEST(TestRfRNF, TestSolveLR)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      X[{ 2 }] = RfVar("x2", RfBin);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 3;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 4;
      C[{ 4 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 4.5;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }]);
      RfRNF rnf(M, RfOmit, RfMPSolverSCIP);
      EXPECT_TRUE(rnf.solveLR());
      EXPECT_EQ(rnf.getLRValue(), 4.5);
    }

    TEST(TestRfRNF, TestGlobalVars)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      X[{ 2 }] = RfVar("x2", RfBin);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 3;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 4;
      C[{ 4 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 5;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }]);
      RfRNF rnf(M, RfOmit, RfMPSolverSCIP);
      RfVarPS L, G;
      L.insert(X[{ 0 }]);
      G.insert(X[{ 1 }]);
      rnf.addLocal(L).setGlobal(G).createCurrent();
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_EQ(rnf.getVarsFixedCount(), 1);
      EXPECT_EQ(rnf.getValue(), 2);
      EXPECT_EQ(rnf.getGapRelative(), 0);
      rnf.createCurrent();
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_EQ(rnf.getValue(), 5);
      EXPECT_EQ(rnf.getGapRelative(), 0);
    }

    TEST(TestRfRNF, TestKnapsack)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      X[{ 2 }] = RfVar("x2", RfBin);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = 5 * X[{ 0 }] + 3 * X[{ 1 }] + 4 * X[{ 2 }] + 6 * X[{ 3 }] + 7 * X[{ 4 }] <= 15;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, 10 * X[{ 0 }] + 7 * X[{ 1 }] + 8 * X[{ 2 }] + 13 * X[{ 3 }] + 16 * X[{ 4 }]);
      RfRNF rnf(M, RfInclude, RfMPSolverSCIP);
      RfVarPS L;
      L.insert(X[{ 0 }]);
      L.insert(X[{ 2 }]);
      L.insert(X[{ 3 }]);
      rnf.addLocal(L).createCurrent();
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_EQ(rnf.getVarsFixedCount(), 3);
      EXPECT_EQ(rnf.getValue(), 31);
      EXPECT_EQ(rnf.getGapRelative(), 0);
      EXPECT_TRUE(rnf.solveAndFixAll());
      EXPECT_EQ(rnf.getValue(), 31);
      EXPECT_EQ(rnf.getGapRelative(), 0);
      EXPECT_TRUE(rnf.areAllFixed());
      EXPECT_EQ(1, rnf.getValue(X[{ 0 }]));
      EXPECT_EQ(1, rnf.getValue(X[{ 2 }]));
      EXPECT_EQ(1, rnf.getValue(X[{ 3 }]));
      EXPECT_EQ(0, rnf.getValue(X[{ 1 }]));
      EXPECT_EQ(0, rnf.getValue(X[{ 4 }]));
    }

    TEST(TestRfRNF, TestInfeas)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      RfConM C;
      C[{ 0 }] = 1 * X[{ 0 }] <= -1;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] * 1);
      RfVarPS L;
      L.insert(X[{ 0 }]);
      RfRNF rnf(M);
      rnf.addLocal(L).createCurrent();
      EXPECT_FALSE(rnf.solveAndFix());
      rnf.createCurrent();
      RfSubpro P = rnf.getCurrent().setLB(X[{ 0 }], -1);
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_TRUE(rnf.areAllFixed());
      EXPECT_EQ(rnf.getValue(), -1);
    }

    TEST(TestRfRestrictAndFix, TestIsCurrSolFeas)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfBin);
      RfConM C;
      C[{ 0 }] = 1 * X[{ 0 }] <= 1;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] <= 0;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] + X[{ 1 }]);
      RfVarPS L;
      L.insert(X[{ 0 }]);
      RfRNF rnf(M, RfOmit);
      rnf.addLocal(L);
      rnf.createCurrent();
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_EQ(rnf.getValue(), 1);
      EXPECT_FALSE(rnf.areAllFixed());
      EXPECT_FALSE(rnf.isCurrentSolFeasible());
      rnf.unfix(X[{ 0 }]);
      L.insert(X[{ 1 }]);
      rnf.addLocal(L);
      rnf.createCurrent();
      EXPECT_TRUE(rnf.solveAndFix());
      EXPECT_EQ(rnf.getValue(), 0);
      EXPECT_TRUE(rnf.areAllFixed());
      EXPECT_TRUE(rnf.isCurrentSolFeasible());
    }

  }  // namespace Algo

}  // namespace Rf
