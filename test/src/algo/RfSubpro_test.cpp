#include "rf/algo/RfSubpro.hpp"

#include <gtest/gtest.h>

namespace Rf
{

  namespace Algo
  {

    TEST(TestRfSubpro, TestRfOmit)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfReal);
      X[{ 2 }] = RfVar("x2", RfInt);
      X[{ 3 }] = RfVar("x3", RfBin);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 2;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 2;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 2;
      RfMod M;
      M.add(C);
      M.setObj(RfMax, X[{ 0 }] * X[{ 1 }] + X[{ 2 }] * X[{ 3 }] * X[{ 4 }]);
      RfVarPS Vloc, Vglob;
      Vloc.insert(X[{ 0 }]);
      for (int i = 1; i <= 4; i++)
      {
        Vloc.insert(X[{ i }]);
        RfSubpro SP(M, Vloc, Vglob, RfOmit);
        EXPECT_EQ(SP.createCons().size(), i);
        EXPECT_EQ(SP.getVarsArtificial().size(), 0);
        EXPECT_EQ(SP.getVarsGlobal().size(), 0);
        EXPECT_EQ(SP.getVarsLocal().size(), i + 1);
        EXPECT_EQ(SP.getVarsRelaxed().size(), 0);
        EXPECT_EQ(SP.getVarsZeroed().size(), 5 - i - 1);
      }
    }

    TEST(TestRfModel, TestRfInclude)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfReal);
      X[{ 2 }] = RfVar("x2", RfInt);
      X[{ 3 }] = RfVar("x3", RfReal);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 2;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 2;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 2;
      RfMod M;
      M.add(C);
      M.setObj(RfMin, 1 * X[{ 0 }] + 2 * X[{ 1 }]);
      RfVarPS Vloc, Vglob;
      Vloc.insert(X[{ 0 }]);
      RfSubpro SP(M, Vloc, Vglob, RfInclude);
      EXPECT_EQ(SP.createCons().size(), 4);
      EXPECT_EQ(SP.createVars().size(), 9);
      EXPECT_EQ(SP.getVarsArtificial().size(), 4);
      EXPECT_EQ(SP.getVarsGlobal().size(), 0);
      EXPECT_EQ(SP.getVarsLocal().size(), 1);
      EXPECT_EQ(SP.getVarsRelaxed().size(), 0);
      EXPECT_EQ(SP.getVarsZeroed().size(), 4);
      EXPECT_EQ(SP.getLB(X[{ 0 }]), 0);
      EXPECT_EQ(SP.getUB(X[{ 0 }]), 1);
      EXPECT_EQ(SP.getLB(X[{ 1 }]), 0);
      EXPECT_EQ(SP.getUB(X[{ 1 }]), 0);
    }

    TEST(TestRfModel, TestRfRelax)
    {
      RfVarM X;
      X[{ 0 }] = RfVar("x0", RfBin);
      X[{ 1 }] = RfVar("x1", RfReal);
      X[{ 2 }] = RfVar("x2", RfInt);
      X[{ 3 }] = RfVar("x3", RfReal);
      X[{ 4 }] = RfVar("x4", RfBin);
      RfConM C;
      C[{ 0 }] = X[{ 0 }] + X[{ 1 }] <= 2;
      C[{ 1 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] <= 2;
      C[{ 2 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] <= 2;
      C[{ 3 }] = X[{ 0 }] + X[{ 1 }] + X[{ 2 }] + X[{ 3 }] + X[{ 4 }] <= 2;
      RfMod M;
      M.add(C);
      M.setObj(RfMin, 1 * X[{ 0 }] + 2 * X[{ 1 }]);
      RfVarPS Vloc, Vglob;
      Vloc.insert(X[{ 0 }]);
      RfSubpro SP(M, Vloc, Vglob, RfRelax);
      EXPECT_EQ(SP.createCons().size(), 4);
      EXPECT_EQ(SP.createVars().size(), 5);
      EXPECT_EQ(SP.getVarsArtificial().size(), 0);
      EXPECT_EQ(SP.getVarsGlobal().size(), 4);
      EXPECT_EQ(SP.getVarsLocal().size(), 1);
      EXPECT_EQ(SP.getVarsRelaxed().size(), 4);
      EXPECT_EQ(SP.getVarsZeroed().size(), 0);
      EXPECT_EQ(SP.getLB(X[{ 0 }]), 0);
      EXPECT_EQ(SP.getUB(X[{ 0 }]), 1);
      EXPECT_TRUE(SP.isInt(X[{ 0 }]));
      EXPECT_FALSE(SP.isInt(X[{ 1 }]));
      EXPECT_FALSE(SP.isInt(X[{ 2 }]));
      EXPECT_FALSE(SP.isInt(X[{ 3 }]));
      EXPECT_FALSE(SP.isInt(X[{ 4 }]));
    }

  }  // namespace Algo

}  // namespace Rf
