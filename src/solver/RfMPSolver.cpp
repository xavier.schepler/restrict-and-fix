#include "rf/solver/RfMPSolver.hpp"

#include <cstdlib>
#include <fstream>

namespace Rf
{

  namespace Solver
  {

    void RfMPSolver::createCons()
    {
      const double infinity = solverMPS->infinity();
      for (auto it = RfConsToMPCons->begin(); it != RfConsToMPCons->end(); it++)
      {
        if (!toSolve.get().hasCon(it->first))
        {
          it->second->SetLB(-infinity);
          it->second->SetUB(infinity);
        }
      }
      RfConPS C = toSolve.get().getCons();
      for (auto c : C)
      {
        double lb, ub;
        if (c->getType() == RfEqual)
        {
          lb = ub = toSolve.get().getRHS(c);
        }
        else if (c->getType() == RfLTE)
        {
          lb = -infinity;
          ub = toSolve.get().getRHS(c);
        }
        else
        {
          lb = toSolve.get().getRHS(c);
          ub = infinity;
        }
        if (RfConsToMPCons->find(c) == RfConsToMPCons->end())
        {
          (*RfConsToMPCons)[c] = solverMPS->MakeRowConstraint(lb, ub);
          for (size_t pos = 0; pos < c->getCount(1); pos++)
          {
            (*RfConsToMPCons)[c]->SetCoefficient((*RfVarsToMPVars)[c->getAddendVar(1, pos, 0)], c->getAddendCoeff(1, pos));
          }
          for (size_t pos = 0; pos < c->getCount(1); pos++)
          {
            (*RfConsToMPCons)[c]->SetCoefficient((*RfVarsToMPVars)[c->getAddendVar(1, pos, 0)], c->getAddendCoeff(1, pos));
          }
          for (size_t pos = 0; pos < c->getArtVarsCount(); pos++)
          {
            (*RfConsToMPCons)[c]->SetCoefficient((*RfVarsToMPVars)[c->getArtVar(pos)], c->getArtVarCoeff(pos));
          }
        }
        else
        {
          if ((*RfConsToMPCons)[c]->lb() != lb)
            (*RfConsToMPCons)[c]->SetLB(lb);
          if ((*RfConsToMPCons)[c]->ub() != ub)
            (*RfConsToMPCons)[c]->SetUB(ub);
        }
      }
    }

    void RfMPSolver::createObj()
    {
      RfExpV ep(toSolve.get().getObj());
      MPObjective* const mpo = solverMPS->MutableObjective();
      if (toSolve.get().isMin())
      {
        mpo->SetMinimization();
      }
      else
      {
        mpo->SetMaximization();
      }
      double o = 0;
      for (size_t pos = 0; pos < ep.getCount(0); pos++)
      {
        o += ep.getAddendCoeff(0, pos);
      }
      if (o > 0)
      {
        mpo->SetOffset(o);
      }
      for (size_t pos = 0; pos < ep.getCount(1); pos++)
      {
        if (mpo->GetCoefficient((*RfVarsToMPVars)[ep.getAddendVar(1, pos, 0)]) != ep.getAddendCoeff(1, pos))
        {
          mpo->SetCoefficient((*RfVarsToMPVars)[ep.getAddendVar(1, pos, 0)], ep.getAddendCoeff(1, pos));
        }
      }
      for (size_t pos = 0; pos < ep.getArtVarsCount(); pos++)
      {
        mpo->SetCoefficient((*RfVarsToMPVars)[ep.getArtVar(pos)], ep.getArtVarCoeff(pos));
      }
    }

    void RfMPSolver::createSol()
    {
      RfVarPS V = toSolve.get().getVars();
      solution = RfSol(solverMPS->Objective().Value());
      for (auto v : V)
      {
        if (!toSolve.get().isArtificial(v))
          solution[v] = (*RfVarsToMPVars)[v]->solution_value();
      }
    }

    void RfMPSolver::createVars()
    {
      const double infinity = solverMPS->infinity();
      for (auto it = RfVarsToMPVars->begin(); it != RfVarsToMPVars->end(); it++)
      {
        if (!toSolve.get().hasVar(it->first))
        {
          it->second->SetLB(0);
          it->second->SetUB(0);
        }
      }
      RfVarPS V = toSolve.get().getVars();
      for (auto v : V)
      {
        double lb, ub;
        bool isInt;
        if (toSolve.get().getLB(v) == -RfInfinity)
          lb = -infinity;
        else
          lb = toSolve.get().getLB(v);
        if (toSolve.get().getUB(v) == RfInfinity)
          ub = infinity;
        else
          ub = toSolve.get().getUB(v);
        isInt = toSolve.get().isInt(v) && !LR;
        if (RfVarsToMPVars->find(v) == RfVarsToMPVars->end())
        {
          if (isInt)
          {
            (*RfVarsToMPVars)[v] = solverMPS->MakeIntVar(lb, ub, v->getName());
          }
          else
          {
            (*RfVarsToMPVars)[v] = solverMPS->MakeNumVar(lb, ub, v->getName());
          }
        }
        else
        {
          if ((*RfVarsToMPVars)[v]->lb() != lb)
            (*RfVarsToMPVars)[v]->SetLB(lb);
          if ((*RfVarsToMPVars)[v]->ub() != ub)
            (*RfVarsToMPVars)[v]->SetUB(ub);
          if (isInt && !(*RfVarsToMPVars)[v]->integer())
            (*RfVarsToMPVars)[v]->SetInteger(true);
          if (!isInt && (*RfVarsToMPVars)[v]->integer())
            (*RfVarsToMPVars)[v]->SetInteger(false);
        }
      }
    }

    RfMPSolver::RfMPSolver(RfSolvable& ts, RfSolver s, bool lr)
        : RfSolverInterface(ts, s, lr),
          timeSolving(0),
          RfConsToMPCons(std::make_unique<std::unordered_map<RfConP, MPConstraint*>>()),
          RfVarsToMPVars(std::make_unique<std::unordered_map<RfVarP, MPVariable*>>())
    {
      if (ts.getDegree() > 1)
        throw std::runtime_error(
            std::string("model's degree is ") + std::to_string(ts.getDegree()) +
            std::string(" but Google MPSolver can only solve linear problems"));
      std::string sn;
      switch (s)
      {
        case RfMPSolverCBC: sn = "CBC"; break;
        case RfMPSolverCPLEX: sn = "CPLEX"; break;
        case RfMPSolverGLPK: sn = "GLPK"; break;
        case RfMPSolverSCIP: sn = "SCIP"; break;
      }
      solverMPS = std::unique_ptr<MPSolver>(MPSolver::CreateSolver(sn.c_str()));
      if (!solverMPS)
        throw std::runtime_error(sn + std::string(" solver unavailable."));
      createVars();
      createCons();
      createObj();
      setVerbose(true);
    }

    void RfMPSolver::exportLP(const std::string& path)
    {
      std::string mdl;
      solverMPS->ExportModelAsLpFormat(false, &mdl);
      std::ofstream out(path.c_str());
      out << mdl;
      out.close();
    }

    double RfMPSolver::getBestBound()
    {
      return solverMPS->Objective().BestBound();
    }

    double RfMPSolver::getGapRelative()
    {
      return abs(solverMPS->Objective().BestBound() - solverMPS->Objective().Value()) /
             abs(1e-10 + solverMPS->Objective().Value());
    }

    RfSol RfMPSolver::getSolution()
    {
      return solution;
    }

    void* RfMPSolver::getSolverRaw()
    {
      return solverMPS->underlying_solver();
    }
  
    double RfMPSolver::getTimeSolving()
    {
      return timeSolving;
    }

    double RfMPSolver::getValue()
    {
      return solverMPS->Objective().Value();
    }

    double RfMPSolver::getValue(RfVarP v)
    {
      return solution[v];
    }

    void RfMPSolver::setVerbose(bool b)
    {
      if (b)
        solverMPS->EnableOutput();
      else
        solverMPS->SuppressOutput();
    }

    bool RfMPSolver::solve()
    {
      auto start = std::chrono::system_clock::now();
      MPSolverParameters p;
      if (paramsD.find(RfRelGapTol) != paramsD.end())
        p.SetDoubleParam(MPSolverParameters::RELATIVE_MIP_GAP, paramsD[RfRelGapTol]);
      if (paramsD.find(RfTimeLimit) != paramsD.end())
        solverMPS->SetTimeLimit(absl::Seconds(1) * paramsD[RfTimeLimit]);
      const MPSolver::ResultStatus st = solverMPS->Solve(p);
      bool solved = (st == MPSolver::OPTIMAL || st == MPSolver::FEASIBLE);
      if (solved)
        createSol();
      else
        solution = RfSol();
      auto end = std::chrono::system_clock::now();
      timeSolving = std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
      return solved;
    }

    bool RfMPSolver::solve(RfSolvable& ts)
    {
      toSolve = ts;
      createVars();
      createCons();
      createObj();
      return solve();
    }

  }  // namespace Solver

}  // namespace Rf
