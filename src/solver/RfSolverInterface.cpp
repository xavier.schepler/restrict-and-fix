#include "rf/solver/RfSolverInterface.hpp"

namespace Rf
{

  namespace Solver
  {

    RfSolverInterface::RfSolverInterface(RfSolvable& t, RfSolver s, bool lr) : LR(lr), solver(s), toSolve(t)
    {
    }

    double RfSolverInterface::getParamD(RfParamD p)
    {
      return paramsD.at(p);
    }

    void RfSolverInterface::setLR(bool lr)
    {
      LR = lr;
    }

    void RfSolverInterface::setParamD(RfParamD p, double v)
    {
      paramsD[p] = v;
    }

  }  // namespace Solver

}  // namespace Rf
