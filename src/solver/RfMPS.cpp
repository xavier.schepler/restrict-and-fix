#include "rf/solver/RfMPS.hpp"

namespace Rf
{

  namespace Solver
  {

    RfMPS::RfMPS(RfSolvable& ts, RfSolver s, bool lr) : solver(std::make_shared<RfMPSolver>(ts, s, lr))
    {
    }

    RfMPS RfMPS::exportLP(const std::string& path)
    {
      solver->exportLP(path);
      return *this;
    }

    double RfMPS::getBestBound()
    {
      return solver->getBestBound();
    }

    double RfMPS::getGapRelative()
    {
      return solver->getGapRelative();
    }

    double RfMPS::getParamD(RfParamD p)
    {
      return solver->getParamD(p);
    }

    RfSol RfMPS::getSolution()
    {
      return solver->getSolution();
    }

    void* RfMPS::getSolverRaw()
    {
      return solver->getSolverRaw();
    }

    double RfMPS::getTimeSolving()
    {
      return solver->getTimeSolving();
    }

    double RfMPS::getValue()
    {
      return solver->getValue();
    }

    double RfMPS::getValue(RfVarP v)
    {
      return solver->getValue(v);
    }

    RfMPS RfMPS::setLR(bool b)
    {
      solver->setLR(b);
      return *this;
    }

    RfMPS RfMPS::setParamD(RfParamD p, double v)
    {
      solver->setParamD(p, v);
      return *this;
    }

    RfMPS RfMPS::setVerbose(bool b)
    {
      solver->setVerbose(b);
      return *this;
    }

    bool RfMPS::solve()
    {
      return solver->solve();
    }

    bool RfMPS::solve(RfSolvable& ts)
    {
      return solver->solve(ts);
    }

  }  // namespace Solver

}  // namespace Rf
