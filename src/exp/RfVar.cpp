#include "rf/exp/RfVar.hpp"

namespace Rf
{

  namespace Model
  {

    RfVar::RfVar() : variable(nullptr)
    {
    }

    RfVar::RfVar(const char *name, RfVarType type, double lb, double ub)
        : variable(std::make_shared<RfVariable>(name, type, lb, ub))
    {
    }

    RfVar::RfVar(const std::string &name, RfVarType type, double lb, double ub)
        : variable(std::make_shared<RfVariable>(name, type, lb, ub))
    {
    }

    RfVar::RfVar(const RfVariable &v) : variable(std::make_shared<RfVariable>(v))
    {
    }

    RfVar::RfVar(std::shared_ptr<RfVariable> &v) : variable(v)
    {
    }

    RfVariable *RfVar::get() const
    {
      return variable.get();
    }

    double RfVar::getLB() const
    {
      return variable->getLB();
    }

    std::string RfVar::getName() const
    {
      return variable->getName();
    }

    RfVarType RfVar::getType() const
    {
      return variable->getType();
    }

    double RfVar::getUB() const
    {
      return variable->getUB();
    }

    bool RfVar::isInt() const
    {
      return variable->isInt();
    }

    bool RfVar::isReal() const
    {
      return variable->isReal();
    }

    RfVar RfVar::setType(RfVarType t)
    {
      variable->setType(t);
      return *this;
    }

    RfVar RfVar::setUB(double ub)
    {
      variable->setUB(ub);
      return *this;
    }

    RfVar::operator std::string() const
    {
      return static_cast<std::string>(*get());
    }

    RfVar::operator RfVarP() const
    {
      return get();
    }

  }  // namespace Model

}  // namespace Rf
