#include "rf/exp/RfExpListElement.hpp"

#include <sstream>

namespace Rf
{

  namespace Model
  {

    RfExpListElement::RfExpListElement() : coeff(1), vars(std::make_unique<RfVarPL>())
    {
    }

    RfExpListElement::RfExpListElement(const RfExpListElement& le) : coeff(le.coeff), vars(std::make_unique<RfVarPL>())
    {
      insertVars(le);
    }

    RfExpListElement::RfExpListElement(RfExpListElement&& le) : coeff(le.coeff), vars(std::move(le.vars))
    {
    }

    RfExpListElement::RfExpListElement(double e) : coeff(e), vars(std::make_unique<RfVarPL>())
    {
    }
    RfExpListElement::RfExpListElement(int e) : coeff(double(e)), vars(std::make_unique<RfVarPL>())
    {
    }
    RfExpListElement::RfExpListElement(RfVarP v) : coeff(1), vars(std::make_unique<RfVarPL>())
    {
      vars->push_back(v);
    }

    RfVarPL::iterator RfExpListElement::begin() const
    {
      return vars->begin();
    }

    RfVarPL::iterator RfExpListElement::end() const
    {
      return vars->end();
    }

    double RfExpListElement::getCoeff() const
    {
      return coeff;
    }

    int RfExpListElement::getDegree() const
    {
      return static_cast<int>(vars->size());
    }

    RfVarPL* RfExpListElement::getVarPL() const
    {
      return vars.get();
    }

    void RfExpListElement::insertVars(const RfExpListElement& e)
    {
      vars->insert(vars->begin(), e.vars->begin(), e.vars->end());
    }

    RfExpListElement& RfExpListElement::operator+=(const double& d)
    {
      coeff += d;
      return *this;
    }

    RfExpListElement& RfExpListElement::operator*=(const double& d)
    {
      coeff *= d;
      return *this;
    }

    RfExpListElement& RfExpListElement::operator*=(const RfVarP& v)
    {
      vars->push_back(v);
      return *this;
    }

    RfExpListElement::operator std::string() const
    {
      std::ostringstream oss;
      oss << coeff;
      if (vars->size() > 0)
        oss << " * ";
      std::size_t i = 0;
      for (auto v : *vars)
      {
        oss << static_cast<std::string>(*v);
        if (i < vars->size() - 1)
          oss << " * ";
        i++;
      }
      return oss.str();
    }

    bool RfExpListElement::operator<(const RfExpListElement& e) const
    {
      auto l1 = getVarPL();
      auto l2 = e.getVarPL();
      if (l1->size() > l2->size())
        return false;
      if (l1->size() < l2->size())
        return true;
      auto it1 = l1->begin();
      auto it2 = l2->begin();
      for (size_t i = 0; i < l1->size(); i++)
      {
        if (*it1 > *it2)
          return false;
        it1++;
        it2++;
      }
      return true;
    }

    RfExpLE operator*(RfExpLE& e, const double& d)
    {
      RfExpLE le;
      le.insertVars(e);
      le *= d;
      return le;
    }

    RfExpLE operator*(const double& d, RfExpLE& e)
    {
      return e * d;
    }

    RfExpLE operator*(RfExpLE& e1, RfExpLE& e2)
    {
      RfExpLE le;
      le.insertVars(e1);
      le *= e1.getCoeff();
      le.insertVars(e2);
      le *= e2.getCoeff();
      return le;
    }

    bool operator==(const RfExpLE& e1, const RfExpLE& e2)
    {
      auto l1 = e1.getVarPL();
      auto l2 = e2.getVarPL();
      if (l1->size() != l2->size())
        return false;
      auto it1 = l1->begin();
      auto it2 = l2->begin();
      while (it1 != l1->end())
      {
        if (*it1 != *it2)
          return false;
        it1++;
        it2++;
      }

      return true;
    }

  }  // namespace Model

}  // namespace Rf
