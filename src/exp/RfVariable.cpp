#include "rf/exp/RfVariable.hpp"

namespace Rf
{

  namespace Model
  {

    RfVariable::RfVariable() : LB(0), name(""), type(RfVarType::RfReal), UB(0)
    {
    }

    RfVariable::RfVariable(const char *n, RfVarType t, double lb, double ub) : LB(lb), name(n), type(t), UB(ub)
    {
      if (t == RfBin)
      {
        LB = 0;
        UB = 1;
      }
    }

    RfVariable::RfVariable(const std::string &n, RfVarType t, double lb, double ub) : LB(lb), name(n), type(t), UB(ub)
    {
      if (t == RfBin)
      {
        LB = 0;
        UB = 1;
      }
    }

    RfVariable::RfVariable(const RfVariable &v) : LB(v.getLB()), name(v.getName()), type(v.getType()), UB(v.getUB())
    {
    }

    double RfVariable::getLB() const
    {
      return LB;
    }

    std::string RfVariable::getName() const
    {
      return name;
    }

    RfVarType RfVariable::getType() const
    {
      return type;
    }

    double RfVariable::getUB() const
    {
      return UB;
    }

    bool RfVariable::isInt() const
    {
      return type == RfBin || type == RfInt;
    }

    bool RfVariable::isReal() const
    {
      return type == RfReal;
    }

    void RfVariable::setType(RfVarType t)
    {
      type = t;
    }

    void RfVariable::setUB(double ub)
    {
      UB = ub;
    }

    RfVariable::operator std::string() const
    {
      return name;
    }

  }  // namespace Model

}  // namespace Rf
