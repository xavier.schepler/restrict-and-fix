#include "rf/exp/RfExpV.hpp"

namespace Rf
{

  namespace Model
  {

    RfExpV::RfExpV() : expression(nullptr)
    {
    }

    RfExpV::RfExpV(const RfExpL& e) : expression(std::make_shared<RfExpVector>(e))
    {
    }

    RfExpV::RfExpV(const RfExpLin& e) : expression(std::make_shared<RfExpVector>(e))
    {
    }

    RfExpV::RfExpV(
        const vector<double>& AC,
        const vector<RfVarP>& AV,
        const vector<size_t>& C,
        const vector<int>& D,
        const RfVarPS& V)
        : expression(std::make_shared<RfExpVector>(AC, AV, C, D, V))
    {
    }

    RfExpV RfExpV::addArtVar(double coeff, RfVarP v)
    {
      expression->addArtVar(coeff, v);
      return *this;
    }

    RfExpV RfExpV::clearArtVars()
    {
      expression->clearArtVars();
      return *this;
    }

    double RfExpV::getAddendCoeff(int d, size_t p) const
    {
      return expression->getAddendCoeff(d, p);
    }

    RfVarP RfExpV::getAddendVar(int d, size_t p, size_t e) const
    {
      return expression->getAddendVar(d, p, e);
    }

    RfVarP RfExpV::getArtVar(size_t pos) const
    {
      return expression->getArtVar(pos);
    }

    double RfExpV::getArtVarCoeff(size_t pos) const
    {
      return expression->getArtVarCoeff(pos);
    }

    size_t RfExpV::getArtVarsCount() const
    {
      return expression->getArtVarsCount();
    }

    size_t RfExpV::getCount(int d) const
    {
      return expression->getCount(d);
    }

    int RfExpV::getDegree() const
    {
      return expression->getDegree();
    }

    double RfExpV::getValue(RfSol s, bool o) const
    {
      return expression->getValue(s, o);
    }

    const RfVarPS& RfExpV::getVars() const
    {
      return expression->getVars();
    }

    RfVarPS RfExpV::getVarsReal() const
    {
      return expression->getVarsReal();
    }

    RfVarPS RfExpV::getVarsInt() const
    {
      return expression->getVarsInt();
    }

    bool RfExpV::hasVarP(RfVarP v) const
    {
      return expression->hasVarP(v);
    }

    RfExpV::operator std::string() const
    {
      return static_cast<std::string>(*expression);
    }

  }  // namespace Model

}  // namespace Rf
