#include "rf/exp/RfExpL.hpp"

#include <iostream>
#include <sstream>

namespace Rf
{

  namespace Model
  {

    RfExpL::RfExpL() : list(std::make_unique<RfExpList>())
    {
    }

    RfExpL::RfExpL(const RfExpL &e) : list(std::make_unique<RfExpList>())
    {
      insert(e);
    }

    RfExpL::RfExpL(RfExpL &&e) : list(std::move(e.list))
    {
    }

    RfExpL::RfExpL(double d) : list(std::make_unique<RfExpList>())
    {
      RfExpLE e(d);
      list->push_back(e);
    }

    RfExpL::RfExpL(int i) : list(std::make_unique<RfExpList>())
    {
      RfExpLE e(i);
      list->push_back(e);
    }
    RfExpL::RfExpL(RfOperator op, RfExpL &&e1, RfExpL &&e2) : list(std::make_unique<RfExpList>())
    {
      if (op == RfPlus)
      {
        splice(e1);
        splice(e2);
      }
      if (op == RfTimes)
      {
        RfExpL pas;
        for (auto pa1 : e1)
          for (auto pa2 : e2)
            pas.push_back(pa1 * pa2);
        splice(pas);
      }
      e1.list = nullptr;
      e2.list = nullptr;
    }

    RfExpL::RfExpL(const RfVar &v) : list(std::make_unique<RfExpList>())
    {
      RfExpLE e(v.get());
      list->push_back(e);
    }

    RfExpL::RfExpL(const RfVarP &v) : list(std::make_unique<RfExpList>())
    {
      RfExpLE e(v);
      list->push_back(e);
    }

    const RfExpL &RfExpL::additiveInverse() const
    {
      list->additiveInverse();
      return *this;
    }

    RfExpLEL::iterator RfExpL::begin() const
    {
      return list->begin();
    }

    const RfExpL &RfExpL::clear() const
    {
      list->clear();
      return *this;
    }

    RfExpLEL::iterator RfExpL::end() const
    {
      return list->end();
    }

    double RfExpL::getConstant() const
    {
      return list->getConstant();
    }

    int RfExpL::getDegree() const
    {
      return list->getDegree();
    }

    RfExpL RfExpL::getPolyAddendsByDegree(int d) const
    {
      return list->getPolyAddendsByDegree(d);
    }

    const RfExpL &RfExpL::insert(const RfExpL &l) const
    {
      list->insert(*(l.list));
      return *this;
    }

    const RfExpL &RfExpL::push_back(const RfExpLE &le) const
    {
      list->push_back(le);
      return *this;
    }

    const RfExpL &RfExpL::sortAndReduce() const
    {
      list->sortAndReduce();
      return *this;
    }

    size_t RfExpL::size() const
    {
      return list->size();
    }

    const RfExpL &RfExpL::splice(const RfExpL &l) const
    {
      list->splice(*(l.list));
      return *this;
    }

    RfExpL &RfExpL::operator=(RfExpL &e)
    {
      *this = e;

      return *this;
    }

    RfExpL &RfExpL::operator=(RfExpL &&e)
    {
      list = std::move(e.list);

      return *this;
    }

    RfExpL &RfExpL::operator+=(RfExpL &e)
    {
      RfExpL l(e);
      splice(e);

      return *this;
    }

    RfExpL &RfExpL::operator+=(RfExpL &&e)
    {
      splice(e);
      e.list = nullptr;

      return *this;
    }

    RfExpL &RfExpL::operator-=(RfExpL &e)
    {
      RfExpL l(e);
      splice(l.additiveInverse());

      return *this;
    }

    RfExpL &RfExpL::operator-=(RfExpL &&e)
    {
      splice(e.additiveInverse());
      e.list = nullptr;

      return *this;
    }

    RfExpL &RfExpL::operator*=(RfExpL &e)
    {
      RfExpL pas;
      for (auto pa1 : *this)
        for (auto pa2 : e)
          pas.push_back(pa1 * pa2);
      clear();
      splice(pas);

      return *this;
    }

    RfExpL &RfExpL::operator*=(RfExpL &&e)
    {
      RfExpL pas;
      for (auto pa1 : *this)
        for (auto pa2 : e)
          pas.push_back(pa1 * pa2);
      clear();
      splice(pas);
      e.list = nullptr;

      return *this;
    }

    RfExpL::operator std::string() const
    {
      return static_cast<std::string>(*list);
    }

    RfExpL operator+(RfExpL &e1, RfExpL &e2)
    {
      RfExpL ep1 = e1, ep2 = e2;
      return RfExpL(RfPlus, std::move(ep1), std::move(ep2));
    }

    RfExpL operator+(RfExpL &&e1, RfExpL &e2)
    {
      RfExpL ep2 = e2;
      return RfExpL(RfPlus, std::move(e1), std::move(ep2));
    }

    RfExpL operator+(RfExpL &e1, RfExpL &&e2)
    {
      RfExpL ep1 = e1;
      return RfExpL(RfPlus, std::move(ep1), std::move(e2));
    }

    RfExpL operator+(RfExpL &&e1, RfExpL &&e2)
    {
      return RfExpL(RfPlus, std::move(e1), std::move(e2));
    }

    RfExpL operator-(RfExpL &e1, RfExpL &e2)
    {
      RfExpL ep1 = e1, ep2 = e2.additiveInverse();
      return RfExpL(RfPlus, std::move(ep1), std::move(ep2));
    }

    RfExpL operator-(RfExpL &&e1, RfExpL &e2)
    {
      RfExpL ep2 = e2.additiveInverse();
      return RfExpL(RfPlus, std::move(e1), std::move(ep2));
    }

    RfExpL operator-(RfExpL &e1, RfExpL &&e2)
    {
      RfExpL ep1 = e1;
      e2.additiveInverse();
      return RfExpL(RfPlus, std::move(ep1), std::move(e2));
    }

    RfExpL operator-(RfExpL &&e1, RfExpL &&e2)
    {
      e2.additiveInverse();
      return RfExpL(RfPlus, std::move(e1), std::move(e2));
    }

    RfExpL operator*(RfExpL &e1, RfExpL &e2)
    {
      if (!e1.size() || !e2.size())
        return RfExpL();
      RfExpL ep1 = e1, ep2 = e2;
      return RfExpL(RfTimes, std::move(ep1), std::move(ep2));
    }

    RfExpL operator*(RfExpL &&e1, RfExpL &e2)
    {
      if (!e1.size() || !e2.size())
        return RfExpL();
      RfExpL ep2 = e2;
      return RfExpL(RfTimes, std::move(e1), std::move(ep2));
    }

    RfExpL operator*(RfExpL &e1, RfExpL &&e2)
    {
      if (!e1.size() || !e2.size())
        return RfExpL();
      RfExpL ep1 = e1;
      return RfExpL(RfTimes, std::move(ep1), std::move(e2));
    }

    RfExpL operator*(RfExpL &&e1, RfExpL &&e2)
    {
      if (!e1.size() || !e2.size())
      {
        return RfExpL();
      }
      return RfExpL(RfTimes, std::move(e1), std::move(e2));
    }

    RfExpL operator-(const RfVar &v)
    {
      return -1 * v;
    }

    RfExpL operator+(const RfVar &v, const int &i)
    {
      return RfExpL(v) + i;
    }

    RfExpL operator+(const int &i, const RfVar &v)
    {
      return v + i;
    }

    RfExpL operator-(const RfVar &v, const int &i)
    {
      return RfExpL(v) - i;
    }

    RfExpL operator-(const int &i, const RfVar &v)
    {
      return RfExpL(i) - v;
    }

    RfExpL operator+(const RfVar &v, const double &d)
    {
      return RfExpL(v) + d;
    }

    RfExpL operator+(const double &d, const RfVar &v)
    {
      return v + d;
    }

    RfExpL operator-(const RfVar &v, const double &d)
    {
      return RfExpL(v) - d;
    }

    RfExpL operator-(const double &d, const RfVar &v)
    {
      return RfExpL(d) - v;
    }

    RfExpL operator-(RfExpL &e)
    {
      RfExpL ep(e);
      ep.additiveInverse();
      return ep;
    }

    RfExpL operator-(RfExpL &&e)
    {
      e.additiveInverse();
      return std::move(e);
    }

  }  // namespace Model

}  // namespace Rf
