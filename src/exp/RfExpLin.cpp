#include "rf/exp/RfExpLin.hpp"

#include <memory>
#include <sstream>

namespace Rf
{

  namespace Model
  {

    RfExpLin::RfExpLin()
        : constant(std::make_unique<double>(0)),
          varsAndCoeffs(std::make_unique<std::unordered_map<RfVarP, double>>())
    {
    }

    RfExpLin::RfExpLin(double d) : RfExpLin()
    {
      *constant = d;
    }
    RfExpLin::RfExpLin(int i) : RfExpLin()
    {
      *constant = double(i);
    }

    RfExpLin::RfExpLin(const RfExpLin& e)
        : constant(std::make_unique<double>(*(e.constant))),
          varsAndCoeffs(std::make_unique<std::unordered_map<RfVarP, double>>(*(e.varsAndCoeffs)))
    {
    }

    RfExpLin::RfExpLin(RfExpLin&& e)
        : constant(std::make_unique<double>(*(e.constant))),
          varsAndCoeffs(std::move(e.varsAndCoeffs))
    {
    }

    RfExpLin::RfExpLin(const RfExpL& e)
        : constant(std::make_unique<double>(0)),
          varsAndCoeffs(std::make_unique<std::unordered_map<RfVarP, double>>())
    {
      add(e.getConstant());
      RfExpL pas = e.getPolyAddendsByDegree(1);
      for (auto& pa : pas)
        add(*(pa.getVarPL()->begin()), pa.getCoeff());
    }

    RfExpLin::RfExpLin(const RfVar& v) : RfExpLin()
    {
      add(v.get(), 1);
    }

    RfExpLin::RfExpLin(const RfVarP& v) : RfExpLin()
    {
      add(v, 1);
    }

    const RfExpLin& RfExpLin::add(double v) const
    {
      *constant += v;
      return *this;
    }

    const RfExpLin& RfExpLin::add(const RfExpLin& e) const
    {
      *constant += *(e.constant);
      for (auto& varAndCoeff : *(e.varsAndCoeffs))
        add(varAndCoeff.first, varAndCoeff.second);
      return *this;
    }

    const RfExpLin& RfExpLin::add(RfVarP v, double coeff) const
    {
      if (varsAndCoeffs->find(v) == varsAndCoeffs->end())
        (*varsAndCoeffs)[v] = 0;
      (*varsAndCoeffs)[v] += coeff;
      return *this;
    }

    const RfExpLin& RfExpLin::additiveInverse() const
    {
      *constant *= -1;
      for (auto& varAndCoeff : *varsAndCoeffs)
        varAndCoeff.second *= -1;
      return *this;
    }

    std::unordered_map<RfVarP, double>::iterator RfExpLin::begin() const
    {
      return varsAndCoeffs->begin();
    }

    std::unordered_map<RfVarP, double>::iterator RfExpLin::end() const
    {
      return varsAndCoeffs->end();
    }

    double RfExpLin::get(RfVarP v) const
    {
      return varsAndCoeffs->at(v);
    }

    double RfExpLin::getConstant() const
    {
      return *constant;
    }

    size_t RfExpLin::size() const
    {
      return varsAndCoeffs->size();
    }

    const RfExpLin& RfExpLin::substract(const RfExpLin& e) const
    {
      add(e.additiveInverse());
      return *this;
    }

    RfExpLin::operator std::string() const
    {
      std::ostringstream oss;
      oss << *constant;
      for (auto& vc : *this)
        oss << " + " << vc.second << " * " << std::string(*(vc.first));
      return oss.str();
    }

    RfExpLin& RfExpLin::operator=(RfExpLin& e)
    {
      *constant = *(e.constant);
      *varsAndCoeffs = (*e.varsAndCoeffs);
      return *this;
    }

    RfExpLin& RfExpLin::operator=(RfExpLin&& e)
    {
      *constant = *(e.constant);
      varsAndCoeffs = std::move(e.varsAndCoeffs);
      return *this;
    }

    RfExpLin& RfExpLin::operator+=(const RfExpL& e)
    {
      add(e.getConstant());
      RfExpL pas = e.getPolyAddendsByDegree(1);
      for (auto& pa : pas)
        add(*(pa.getVarPL()->begin()), pa.getCoeff());
      return *this;
    }

    RfExpLin& RfExpLin::operator-=(const RfExpL& e)
    {
      add(-1 * e.getConstant());
      RfExpL pas = e.getPolyAddendsByDegree(1);
      for (auto& pa : pas)
        add(*(pa.getVarPL()->begin()), -1 * pa.getCoeff());
      return *this;
    }

  }  // namespace Model

}  // namespace Rf
