#include <algorithm>

#include "rf/exp/RfExpV.hpp"

namespace Rf
{

  namespace Model
  {

    RfExpVector::RfExpVector(const RfExpL& e)
        : addendCoeffs(std::make_unique<vector<double>>()),
          addendVars(std::make_unique<vector<RfVarP>>()),
          artVarCoeffs(std::make_unique<vector<double>>()),
          artVars(std::make_unique<vector<RfVarP>>()),
          counts(std::make_unique<vector<size_t>>()),
          degrees(std::make_unique<vector<int>>()),
          vars(std::make_unique<RfVarPS>())
    {
      int degree = e.getDegree();
      for (int d = 0; d <= degree; d++)
      {
        RfExpL elts = e.getPolyAddendsByDegree(d);
        if (elts.size() == 0)
          continue;
        degrees->push_back(d);
        counts->push_back(elts.size());
        addendVars->reserve(elts.size() * static_cast<size_t>(d));
        addendCoeffs->reserve(elts.size());
        for (auto& elt : elts)
        {
          for (auto v : elt)
          {
            addendVars->push_back(v);
            vars->insert(v);
          }
          addendCoeffs->push_back(elt.getCoeff());
        }
      }
    }

    RfExpVector::RfExpVector(const RfExpLin& e)
        : addendCoeffs(std::make_unique<vector<double>>()),
          addendVars(std::make_unique<vector<RfVarP>>()),
          artVarCoeffs(std::make_unique<vector<double>>()),
          artVars(std::make_unique<vector<RfVarP>>()),
          counts(std::make_unique<vector<size_t>>()),
          degrees(std::make_unique<vector<int>>()),
          vars(std::make_unique<RfVarPS>())
    {
      degrees->push_back(0);
      counts->push_back(1);
      addendCoeffs->push_back(e.getConstant());
      degrees->push_back(1);
      counts->push_back(e.size());
      addendVars->reserve(e.size());
      addendCoeffs->reserve(e.size());
      for (auto varAndCoeff : e)
      {
        addendVars->push_back(varAndCoeff.first);
        vars->insert(varAndCoeff.first);
        addendCoeffs->push_back(varAndCoeff.second);
      }
    }

    RfExpVector::RfExpVector(
        const vector<double>& AC,
        const vector<RfVarP>& AV,
        const vector<size_t>& C,
        const vector<int>& D,
        const RfVarPS& V)
        : addendCoeffs(std::make_unique<vector<double>>(AC)),
          addendVars(std::make_unique<vector<RfVarP>>(AV)),
          artVarCoeffs(std::make_unique<vector<double>>()),
          artVars(std::make_unique<vector<RfVarP>>()),
          counts(std::make_unique<vector<size_t>>(C)),
          degrees(std::make_unique<vector<int>>(D)),
          vars(std::make_unique<RfVarPS>(V))
    {
    }

    void RfExpVector::addArtVar(double coeff, RfVarP v)
    {
      artVarCoeffs->push_back(coeff);
      artVars->push_back(v);
    }

    void RfExpVector::clearArtVars()
    {
      artVarCoeffs->clear();
      artVars->clear();
    }

    size_t RfExpVector::getAddendCoeffIndex(int degree, size_t pos) const
    {
      size_t i = 0;
      for (size_t j = 0; j < counts->size() && degrees->at(j) < degree; j++)
      {
        i += counts->at(j);
      }
      return i + pos;
    }

    size_t RfExpVector::getAddendVarIndex(int degree, size_t pos, size_t elt) const
    {
      size_t i = 0;
      for (size_t j = 0; j < counts->size() && degrees->at(j) < degree; j++)
      {
        i += counts->at(j) * static_cast<size_t>(degrees->at(j));
      }
      return i + pos * static_cast<size_t>(degree) + elt;
    }

    double RfExpVector::getAddendCoeff(int degree, size_t pos) const
    {
      return addendCoeffs->at(getAddendCoeffIndex(degree, pos));
    }

    RfVarP RfExpVector::getArtVar(size_t pos) const
    {
      return artVars->at(pos);
    }

    double RfExpVector::getArtVarCoeff(size_t pos) const
    {
      return artVarCoeffs->at(pos);
    }

    size_t RfExpVector::getArtVarsCount() const
    {
      return artVars->size();
    }

    size_t RfExpVector::getCount(int degree) const
    {
      size_t i;
      for (i = 0; i < degrees->size() && degrees->at(i) != degree; i++)
        ;
      if (i == degrees->size())
        return 0;
      return counts->at(i);
    }

    int RfExpVector::getDegree() const
    {
      return degrees->back();
    }

    RfVarP RfExpVector::getAddendVar(int degree, size_t pos, size_t elt) const
    {
      return addendVars->at(getAddendVarIndex(degree, pos, elt));
    }

    double RfExpVector::getValue(RfSol s, bool o) const
    {
      double t = 0;
      for (size_t i = 0; i < degrees->size(); i++)
      {
        if (degrees->at(i) == 0 && o)
          continue;
        int degree = degrees->at(i);
        for (size_t pos = 0; pos < counts->at(i); pos++)
        {
          double n = getAddendCoeff(degree, pos);
          for (size_t elt = 0; elt < static_cast<size_t>(degree); elt++)
          {
            RfVarP v = getAddendVar(degree, pos, elt);
            if (!s.hasVar(v))
            {
              n = 0;
              break;
            }
            else
            {
              n *= s[v];
            }
          }
          t += n;
        }
      }
      return t;
    }

    const RfVarPS& RfExpVector::getVars() const
    {
      return *vars;
    }

    RfVarPS RfExpVector::getVarsReal() const
    {
      RfVarPS V;
      for (auto v : *vars)
      {
        if (v->isReal())
        {
          V.insert(v);
        }
      }
      return V;
    }

    RfVarPS RfExpVector::getVarsInt() const
    {
      RfVarPS V;
      for (auto v : *vars)
      {
        if (v->isInt())
        {
          V.insert(v);
        }
      }
      return V;
    }

    bool RfExpVector::hasVarP(RfVarP v) const
    {
      return vars->find(v) != vars->end();
    }

    RfExpVector::operator std::string() const
    {
      std::string s;
      return s;
    }

  }  // namespace Model

}  // namespace Rf
