#include <iostream>
#include <sstream>
#include <vector>

#include "rf/exp/RfExpL.hpp"

namespace Rf
{

  namespace Model
  {

    RfExpList::RfExpList() : list(std::make_unique<RfExpLEL>()), reducedAndSorted(false)
    {
    }

    void RfExpList::additiveInverse()
    {
      for (auto& le : *list)
        le *= -1;
      reducedAndSorted = false;
    }

    RfExpLEL::iterator RfExpList::begin() const
    {
      return list->begin();
    }

    void RfExpList::clear()
    {
      list->clear();
      reducedAndSorted = false;
    }

    RfExpLEL::iterator RfExpList::end() const
    {
      return list->end();
    }

    double RfExpList::getConstant()
    {
      RfExpL l = getPolyAddendsByDegree(0);
      if (l.size() == 0)
        return 0.;
      return l.begin()->getCoeff();
    }

    int RfExpList::getDegree()
    {
      if (!reducedAndSorted)
        sortAndReduce();
      int degree = 0;
      for (auto& le : *list)
        if (le.getDegree() > degree)
          degree = le.getDegree();
      return degree;
    }

    RfExpL RfExpList::getPolyAddendsByDegree(int d)
    {
      if (!reducedAndSorted)
      {
        sortAndReduce();
      }
      RfExpL pas;
      for (auto& le : *list)
      {
        if (le.getDegree() == d)
          pas.push_back(le);
        if (le.getDegree() > d)
          break;
      }
      return pas;
    }

    void RfExpList::insert(const RfExpList& l)
    {
      list->insert(list->begin(), l.list->begin(), l.list->end());
      reducedAndSorted = false;
    }

    void RfExpList::push_back(const RfExpLE& le)
    {
      list->push_back(le);
      reducedAndSorted = false;
    }

    void RfExpList::sortAndReduce()
    {
      std::vector<RfExpLEL::iterator> D;
      for (auto& e : *list)
      {
        e.getVarPL()->sort();
      }
      list->sort();
      for (auto it1 = list->begin(); it1 != list->end();)
      {
        auto it2 = std::next(it1);
        for (; it2 != list->end() && *it1 == *it2; it2++)
        {
          *it1 += it2->getCoeff();
          D.push_back(it2);
        }
        it1 = it2;
      }
      for (auto it : D)
        list->erase(it);
      reducedAndSorted = true;
    }

    size_t RfExpList::size() const
    {
      return list->size();
    }

    void RfExpList::splice(const RfExpList& l)
    {
      list->splice(list->begin(), *(l.list));
      reducedAndSorted = false;
    }

    RfExpList::operator std::string() const
    {
      std::ostringstream oss;
      size_t i = 0;
      for (auto& it : *list)
      {
        oss << static_cast<std::string>(it);
        if (i < list->size() - 1)
          oss << " + ";
        i++;
      }
      return oss.str();
    }

  }  // namespace Model

}  // namespace Rf
