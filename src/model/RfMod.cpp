#include "rf/model/RfMod.hpp"

namespace Rf
{

  namespace Model
  {

    RfMod::RfMod() : model(std::make_shared<RfModel>())
    {
    }

    RfMod RfMod::add(RfCon& c)
    {
      model->add(c);
      return *this;
    }

    RfMod RfMod::add(RfConM C)
    {
      model->add(C);
      return *this;
    }

    RfConPS RfMod::createCons()
    {
      return model->createCons();
    }

    RfVarPS RfMod::createVars()
    {
      return model->createVars();
    }

    RfConPS RfMod::getCons() const
    {
      return model->getCons();
    }

    int RfMod::getDegree() const
    {
      return model->getDegree();
    }

    double RfMod::getLB(RfVarP v) const
    {
      return model->getLB(v);
    }

    RfExpV RfMod::getObj()
    {
      return model->getObj();
    }

    RfObjType RfMod::getObjType() const
    {
      return model->getObjType();
    }

    double RfMod::getRHS(RfConP c) const
    {
      return model->getRHS(c);
    }

    double RfMod::getUB(RfVarP v) const
    {
      return model->getUB(v);
    }

    RfVarPS RfMod::getVars() const
    {
      return model->getVars();
    }

    size_t RfMod::getVarsCount() const
    {
      return model->getVarsCount();
    }

    RfVarPS RfMod::getVarsInt() const
    {
      return model->getVarsInt();
    }

    RfVarPS RfMod::getVarsReal() const
    {
      return model->getVarsReal();
    }

    bool RfMod::hasCon(RfConP c) const
    {
      return model->hasCon(c);
    }

    bool RfMod::hasVar(RfVarP v) const
    {
      return model->hasVar(v);
    }

    bool RfMod::isArtificial(RfVarP v) const
    {
      return model->isArtificial(v);
    }

    bool RfMod::isFeasible(RfSol s, double e) const
    {
      return model->isFeasible(s, e);
    }

    bool RfMod::isInt(RfVarP v) const
    {
      return model->isInt(v);
    }

    bool RfMod::isMax() const
    {
      return model->isMax();
    }

    bool RfMod::isMin() const
    {
      return model->isMin();
    }

    RfMod RfMod::setObj(RfObjType t, const RfExpV& obj)
    {
      model->setObj(t, obj);
      return *this;
    }

    RfMod::operator std::string() const
    {
      return static_cast<std::string>(*model.get());
    }

  }  // namespace Model

}  // namespace Rf
