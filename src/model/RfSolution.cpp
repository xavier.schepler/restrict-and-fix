#include "rf/model/RfSolution.hpp"

namespace Rf
{

  namespace Model
  {

    RfSolution::RfSolution(double v) : value(v)
    {
      ;
    }

    double RfSolution::getValue() const
    {
      return value;
    }

    bool RfSolution::hasVar(RfVarP v) const
    {
      return varsVals.find(v) != varsVals.end();
    }

    double& RfSolution::operator[](RfVarP v)
    {
      if (varsVals.find(v) == varsVals.end())
      {
        varsVals[v] = 0;
      }
      return varsVals[v];
    }

  }  // namespace Model

}  // namespace Rf
