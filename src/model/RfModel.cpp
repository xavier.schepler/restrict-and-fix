#include "rf/model/RfModel.hpp"

#include <sstream>

namespace Rf
{

  namespace Model
  {

    RfModel::RfModel()
        : cons(std::make_unique<RfConPS>()),
          degree(0),
          vars(std::make_unique<RfVarPS>()),
          varsInt(std::make_unique<RfVarPS>()),
          varsReal(std::make_unique<RfVarPS>())
    {
    }

    void RfModel::add(RfCon& c)
    {
      RfVarPS V, VR, VI;
      V = c.getVars();
      VR = c.getVarsReal();
      VI = c.getVarsInt();
      vars->insert(V.begin(), V.end());
      varsReal->insert(VR.begin(), VR.end());
      varsInt->insert(VI.begin(), VI.end());
      cons->insert(c.get());
      int d = c.getDegree();
      degree = d > degree ? d : degree;
    }

    void RfModel::add(RfConM C)
    {
      for (auto& it : C)
      {
        add(it.second);
      }
    }

    RfConPS RfModel::createCons()
    {
      return getCons();
    }

    RfVarPS RfModel::createVars()
    {
      return getVars();
    }

    RfConPS RfModel::getCons() const
    {
      return *cons;
    }

    int RfModel::getDegree() const
    {
      return degree;
    }

    double RfModel::getLB(RfVarP v) const
    {
      return v->getLB();
    }

    RfExpV RfModel::getObj()
    {
      return obj;
    }

    RfObjType RfModel::getObjType() const
    {
      return type;
    }

    double RfModel::getRHS(RfConP c) const
    {
      return c->getRHS();
    }

    double RfModel::getUB(RfVarP v) const
    {
      return v->getUB();
    }

    RfVarPS RfModel::getVars() const
    {
      return *vars;
    }

    std::size_t RfModel::getVarsCount() const
    {
      return vars->size();
    }

    RfVarPS RfModel::getVarsInt() const
    {
      return *varsInt;
    }

    RfVarPS RfModel::getVarsReal() const
    {
      return *varsReal;
    }

    bool RfModel::hasCon(RfConP c) const
    {
      return cons->find(c) != cons->end();
    }

    bool RfModel::hasVar(RfVarP v) const
    {
      return vars->find(v) != vars->end();
    }

    bool RfModel::isArtificial(RfVarP v) const
    {
      return false;
    }

    bool RfModel::isFeasible(RfSol s, double e) const
    {
      int ci(0), vi(0);
      for (auto c : *cons)
        if (!c->isFeasible(s, e))
          ci++;
      for (auto v : *vars)
      {
        double x;
        if (!s.hasVar(v))
          x = 0;
        else
          x = s[v];
        if (!(v->getLB() - x <= e && x - v->getUB() <= e))
        {
          vi++;
        }
        double ip;
        if (modf(x, &ip) > e && v->isInt())
        {
          vi++;
        }
      }
      return ci == 0 && vi == 0;
    }

    bool RfModel::isInt(RfVarP v) const
    {
      return v->isInt();
    }

    bool RfModel::isMax() const
    {
      return type == RfMax;
    }

    bool RfModel::isMin() const
    {
      return type == RfMin;
    }

    void RfModel::setObj(RfObjType t, const RfExpV& o)
    {
      this->obj = o;
      type = t;
      RfVarPS V, VR, VI;
      V = obj.getVars();
      VI = obj.getVarsInt();
      VR = obj.getVarsReal();
      vars->insert(V.begin(), V.end());
      varsInt->insert(VI.begin(), VI.end());
      varsReal->insert(VR.begin(), VR.end());
      int d = obj.getDegree();
      degree = d > degree ? d : degree;
    }

    RfModel::operator std::string() const
    {
      std::ostringstream oss;
      std::string s;
      oss << (isMin() ? "minimize: " : "maximize: ");
      oss << static_cast<std::string>(obj);
      oss << std::endl;
      oss << "subject to:" << std::endl;
      for (auto c : *cons)
        oss << static_cast<std::string>(*c) << std::endl;
      oss << "vars:" << std::endl;
      for (auto v : *vars)
      {
        oss << v->getLB() << " <= " << v->getName() << " <= " << v->getUB();
        if (v->isInt())
          s = " int";
        else
          s = " real";
        oss << s << std::endl;
      }
      return oss.str();
    }

  }  // namespace Model

}  // namespace Rf
