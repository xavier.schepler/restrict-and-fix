#include "rf/model/RfCon.hpp"

#include "rf/exp/RfExpL.hpp"

namespace Rf
{

  namespace Model
  {

    RfCon::RfCon() : constraint(nullptr)
    {
    }

    RfCon::RfCon(RfConP c) : constraint(std::make_shared<RfConstraintPoly>(c))
    {
    }

    RfCon::RfCon(RfConType type, const RfExpL &LHS, const RfExpL &RHS)
        : constraint(std::make_shared<RfConstraintPoly>(type, LHS, RHS))
    {
    }

    RfCon::RfCon(RfConType type, const RfExpLin &LHS, const RfExpLin &RHS)
        : constraint(std::make_shared<RfConstraintPoly>(type, LHS, RHS))
    {
    }

    RfConstraintPoly *RfCon::get() const
    {
      return constraint.get();
    }

    double RfCon::getAddendCoeff(int degree, size_t pos) const
    {
      return constraint->getAddendCoeff(degree, pos);
    }

    RfVarP RfCon::getAddendVar(int degree, size_t pos, size_t elt) const
    {
      return constraint->getAddendVar(degree, pos, elt);
    }

    size_t RfCon::getCount(int degree) const
    {
      return constraint->getCount(degree);
    }

    int RfCon::getDegree() const
    {
      return constraint->getDegree();
    }

    std::string RfCon::getName() const
    {
      return constraint->getName();
    }

    double RfCon::getRHS() const
    {
      return constraint->getRHS();
    }

    RfConType RfCon::getType() const
    {
      return constraint->getType();
    }

    const RfVarPS &RfCon::getVars() const
    {
      return constraint->getVars();
    }

    RfVarPS RfCon::getVarsReal() const
    {
      return constraint->getVarsReal();
    }

    RfVarPS RfCon::getVarsInt() const
    {
      return constraint->getVarsInt();
    }

    bool RfCon::hasVarP(RfVarP v) const
    {
      return constraint->hasVarP(v);
    }

    bool RfCon::isFeasible(RfSol s, double e) const
    {
      return constraint->isFeasible(s, e);
    }

    void RfCon::setName(const std::string &n)
    {
      constraint->setName(n);
    }

    RfCon::operator std::string() const
    {
      return static_cast<std::string>(*get());
    }

    RfCon operator==(const RfExp &LHS, const int &RHS)
    {
      return LHS == RfExp(RHS);
    }

    RfCon operator<=(const RfExp &LHS, const int &RHS)
    {
      return LHS <= RfExp(RHS);
    }
    RfCon operator>=(const RfExp &LHS, const int &RHS)
    {
      return LHS >= RfExp(RHS);
    }

    RfCon operator==(const int &LHS, const RfExp &RHS)
    {
      return RfExp(LHS) == RHS;
    }

    RfCon operator<=(const int &LHS, const RfExp &RHS)
    {
      return RfExp(LHS) <= RHS;
    }

    RfCon operator>=(const int &LHS, const RfExp &RHS)
    {
      return RfExp(LHS) >= RHS;
    }

    RfCon operator==(const RfExp &LHS, const double &RHS)
    {
      return LHS == RfExp(RHS);
    }

    RfCon operator<=(const RfExp &LHS, const double &RHS)
    {
      return LHS <= RfExp(RHS);
    }
    RfCon operator>=(const RfExp &LHS, const double &RHS)
    {
      return LHS >= RfExp(RHS);
    }

    RfCon operator==(const double &LHS, const RfExp &RHS)
    {
      return RfExp(LHS) == RHS;
    }

    RfCon operator<=(const double &LHS, const RfExp &RHS)
    {
      return RfExp(LHS) <= RHS;
    }

    RfCon operator>=(const double &LHS, const RfExp &RHS)
    {
      return RfExp(LHS) >= RHS;
    }

    RfCon operator==(const RfExp &LHS, const RfExp &RHS)
    {
      return RfCon(RfEqual, LHS.sortAndReduce(), RHS.sortAndReduce());
    }

    RfCon operator<=(const RfExp &LHS, const RfExp &RHS)
    {
      return RfCon(RfLTE, LHS.sortAndReduce(), RHS.sortAndReduce());
    }

    RfCon operator>=(const RfExp &LHS, const RfExp &RHS)
    {
      return RfCon(RfGTE, LHS.sortAndReduce(), RHS.sortAndReduce());
    }

    RfCon operator==(const RfExpLin &LHS, const int &RHS)
    {
      RfExpLin e(RHS);
      return LHS == e;
    }

    RfCon operator<=(const RfExpLin &LHS, const int &RHS)
    {
      RfExpLin e(RHS);
      return LHS <= e;
    }
    RfCon operator>=(const RfExpLin &LHS, const int &RHS)
    {
      RfExpLin e(RHS);
      return LHS >= e;
    }

    RfCon operator==(const int &LHS, const RfExpLin &RHS)
    {
      RfExpLin e(LHS);
      return e == RHS;
    }

    RfCon operator<=(const int &LHS, const RfExpLin &RHS)
    {
      RfExpLin e(LHS);
      return e <= RHS;
    }

    RfCon operator>=(const int &LHS, const RfExpLin &RHS)
    {
      RfExpLin e(LHS);
      return e >= RHS;
    }

    RfCon operator==(const RfExpLin &LHS, const double &RHS)
    {
      RfExpLin e(RHS);
      return LHS == e;
    }

    RfCon operator<=(const RfExpLin &LHS, const double &RHS)
    {
      RfExpLin e(RHS);
      return LHS <= e;
    }
    RfCon operator>=(const RfExpLin &LHS, const double &RHS)
    {
      RfExpLin e(RHS);
      return LHS >= e;
    }

    RfCon operator==(const double &LHS, const RfExpLin &RHS)
    {
      RfExpLin e(LHS);
      return e == RHS;
    }

    RfCon operator<=(const double &LHS, const RfExpLin &RHS)
    {
      RfExpLin e(LHS);
      return e <= RHS;
    }

    RfCon operator>=(const double &LHS, const RfExpLin &RHS)
    {
      RfExpLin e(LHS);
      return e >= RHS;
    }

    RfCon operator==(const RfExpLin &LHS, const RfExpLin &RHS)
    {
      return RfCon(RfEqual, LHS, RHS);
    }

    RfCon operator<=(const RfExpLin &LHS, const RfExpLin &RHS)
    {
      return RfCon(RfLTE, LHS, RHS);
    }

    RfCon operator>=(const RfExpLin &LHS, const RfExpLin &RHS)
    {
      return RfCon(RfGTE, LHS, RHS);
    }

  }  // namespace Model

}  // namespace Rf
