#include "rf/model/RfConstraintPoly.hpp"

#include <sstream>

namespace Rf
{

  namespace Model
  {

    int RfConstraintPoly::number = 0;

    RfConstraintPoly::RfConstraintPoly(RfConP c)
        : RfExpVector(*c->addendCoeffs, *c->addendVars, *c->counts, *c->degrees, *c->vars)
    {
      std::ostringstream n;
      n << c->getName() << "_art_" << (number++);
      name = n.str();
      RHS = c->RHS;
      type = c->type;
    }

    RfConstraintPoly::RfConstraintPoly(RfConType t, const RfExpL& l, double r) : RfExpVector(l), RHS(r), type(t)
    {
      for (size_t i = 0; i < getCount(0); i++)
      {
        RHS -= getAddendCoeff(0, i);
      }
      std::ostringstream n;
      n << "c_" << (number++);
      this->name = n.str();
    }

    RfConstraintPoly::RfConstraintPoly(RfConType t, const RfExpLin& l, double r) : RfExpVector(l), RHS(r), type(t)
    {
      for (size_t i = 0; i < getCount(0); i++)
      {
        RHS -= getAddendCoeff(0, i);
      }
      std::ostringstream n;
      n << "c_" << (number++);
      this->name = n.str();
    }

    RfConstraintPoly::RfConstraintPoly(RfConType t, const RfExpL& l, const RfExpL& r)
        : RfConstraintPoly(t, l.splice(r.additiveInverse()), 0.)
    {
    }

    RfConstraintPoly::RfConstraintPoly(RfConType t, const RfExpLin& l, const RfExpLin& r)
        : RfConstraintPoly(t, l.substract(r), 0.)
    {
    }

    std::string RfConstraintPoly::getName() const
    {
      return name;
    }

    double RfConstraintPoly::getRHS() const
    {
      return RHS;
    }

    RfConType RfConstraintPoly::getType() const
    {
      return type;
    }

    bool RfConstraintPoly::isFeasible(RfSol s, double e) const
    {
      double v = getValue(s, true);
      switch (type)
      {
        case RfLTE: return v - RHS <= e; break;
        case RfGTE: return v - RHS >= -e; break;
        default:  // RfEqual
          double d(v - RHS);
          d = d < 0 ? -1 * d : d;
          return d <= e;
          break;
      }
    }

    void RfConstraintPoly::setName(const std::string& n)
    {
      this->name = n;
    }

    RfConstraintPoly::operator std::string() const
    {
      std::ostringstream oss;
      for (size_t i = 0; i < degrees->size(); i++)
      {
        if (degrees->at(i) == 0)
          continue;
        int degree = degrees->at(i);
        for (size_t pos = 0; pos < counts->at(i); pos++)
        {
          oss << "(" << getAddendCoeff(degree, pos);
          for (size_t elt = 0; elt < static_cast<size_t>(degree); elt++)
          {
            oss << " * " << std::string(*getAddendVar(degree, pos, elt));
          }
          oss << ")";
          if (pos < counts->at(i) - 1)
            oss << " + ";
        }
        if (i < degrees->size() - 1)
          oss << " + ";
      }
      if (type == RfEqual)
        oss << " = ";
      if (type == RfLTE)
        oss << " <= ";
      if (type == RfGTE)
        oss << " >= ";
      oss << RHS;
      return oss.str();
    }

  }  // namespace Model

}  // namespace Rf
