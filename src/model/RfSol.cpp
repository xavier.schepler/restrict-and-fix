#include "rf/model/RfSol.hpp"

namespace Rf
{

  namespace Model
  {

    RfSol::RfSol() : solution(nullptr)
    {
    }

    RfSol::RfSol(double v) : solution(std::make_shared<RfSolution>(v))
    {
      ;
    }

    RfSolution* RfSol::get()
    {
      return solution.get();
    }

    double RfSol::getValue() const
    {
      return solution->getValue();
    }

    bool RfSol::hasVar(RfVarP v) const
    {
      return solution->hasVar(v);
    }

    double& RfSol::operator[](RfVarP v)
    {
      return (*(solution.get()))[v];
    }

  }  // namespace Model

}  // namespace Rf
