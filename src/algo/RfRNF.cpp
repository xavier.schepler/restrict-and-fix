#include "rf/algo/RfRNF.hpp"

namespace Rf
{

  namespace Algo
  {

    RfRNF::RfRNF(RfMod ml, RfSPMode m, RfSolver s, double tl, double rgt, bool v)
        : rnf(std::make_shared<RfRestrictAndFix>(ml, m, s, tl, rgt, v))
    {
    }

    RfRNF RfRNF::addLocal(RfVarPS &L)
    {
      rnf->addLocal(L);
      return *this;
    }

    bool RfRNF::areAllFixed() const
    {
      return rnf->areAllFixed();
    }

    RfRNF RfRNF::createCurrent()
    {
      rnf->createCurrent();
      return *this;
    }

    RfRNF RfRNF::fix(RfVarP var, double val)
    {
      rnf->fix(var, val);
      return *this;
    }

    RfSubpro RfRNF::getCurrent() const
    {
      return rnf->getCurrent();
    }

    size_t RfRNF::getCurrentNum() const
    {
      return rnf->getCurrentNum();
    }

    RfSol RfRNF::getCurrentSol() const
    {
      return rnf->getCurrentSol();
    }

    size_t RfRNF::getCount() const
    {
      return rnf->getCount();
    }

    RfVarPDouble RfRNF::getFixeds() const
    {
      return rnf->getFixeds();
    }

    double RfRNF::getGapRelative() const
    {
      return rnf->getGapRelative();
    }

    double RfRNF::getGapRelative(size_t i) const
    {
      return rnf->getGapRelative(i);
    }

    double RfRNF::getGapRelativeLR() const
    {
      return rnf->getGapRelativeLR();
    }

    RfSol RfRNF::getLRSol() const
    {
      return rnf->getLRSol();
    }

    double RfRNF::getLRTimeSolving() const
    {
      return rnf->getLRTimeSolving();
    }

    double RfRNF::getLRValue() const
    {
      return rnf->getLRValue();
    }

    double RfRNF::getSolverParamD(RfParamD p) const
    {
      return rnf->getSolverParamD(p);
    }

    void *RfRNF::getSolverRaw()
    {
      return rnf->getSolverRaw();
    }

    double RfRNF::getTimeFixing() const
    {
      return rnf->getTimeFixing();
    }

    double RfRNF::getTimeFixing(size_t i) const
    {
      return rnf->getTimeFixing(i);
    }

    double RfRNF::getTimeFixingTotal() const
    {
      return rnf->getTimeFixingTotal();
    }

    double RfRNF::getTimeInit() const
    {
      return rnf->getTimeInit();
    }

    double RfRNF::getTimeInit(size_t i) const
    {
      return rnf->getTimeInit(i);
    }

    double RfRNF::getTimeInitTotal() const
    {
      return rnf->getTimeInitTotal();
    }

    double RfRNF::getTimeSolving() const
    {
      return rnf->getTimeSolving();
    }

    double RfRNF::getTimeSolving(size_t i) const
    {
      return rnf->getTimeSolving(i);
    }

    double RfRNF::getTimeSolvingTotal() const
    {
      return rnf->getTimeSolvingTotal();
    }

    double RfRNF::getTimeTotal(size_t i) const
    {
      return rnf->getTimeSolving(i);
    }

    double RfRNF::getTimeTotal() const
    {
      return rnf->getTimeTotal();
    }

    double RfRNF::getValue() const
    {
      return rnf->getValue();
    }

    double RfRNF::getValue(RfVarP v) const
    {
      return rnf->getValue(v);
    }

    double RfRNF::getValue(size_t i) const
    {
      return rnf->getValue(i);
    }

    std::size_t RfRNF::getVarsCount() const
    {
      return rnf->getVarsCount();
    }

    std::size_t RfRNF::getVarsFixedCount() const
    {
      return rnf->getVarsFixedCount();
    }

    bool RfRNF::hasCurrentSol() const
    {
      return rnf->hasCurrentSol();
    }

    bool RfRNF::hasSolverRebuildModel() const
    {
      return rnf->hasSolverRebuildModel();
    }

    bool RfRNF::isCurrentSolFeasible() const
    {
      return rnf->isCurrentSolFeasible();
    }

    RfRNF RfRNF::setExportLP(bool b, const std::string &bn)
    {
      rnf->setExportLP(b, bn);
      return *this;
    }

    RfRNF RfRNF::setFixeds(RfVarPDouble &V)
    {
      rnf->setFixeds(V);
      return *this;
    }

    RfRNF RfRNF::setGlobal(RfVarPS &V)
    {
      rnf->setGlobal(V);
      return *this;
    }

    RfRNF RfRNF::setSPMode(RfSPMode m)
    {
      rnf->setSPMode(m);
      return *this;
    }

    RfRNF RfRNF::setSolverParamD(RfParamD p, double v)
    {
      rnf->setSolverParamD(p, v);
      return *this;
    }

    RfRNF RfRNF::setSolverRebuildModel(bool b)
    {
      rnf->setSolverRebuildModel(b);
      return *this;
    }

    RfRNF RfRNF::setVerbose(bool b)
    {
      rnf->setVerbose(b);
      return *this;
    }

    bool RfRNF::solveAndFix()
    {
      return rnf->solveAndFix();
    }

    bool RfRNF::solveAndFixAll(bool s, double t)
    {
      return rnf->solveAndFixAll(s, t);
    }

    bool RfRNF::solveLR(double tl)
    {
      return rnf->solveLR(tl);
    }

    RfRNF RfRNF::unfix(RfVarP v)
    {
      rnf->unfix(v);
      return *this;
    }

  }  // namespace Algo

}  // namespace Rf
