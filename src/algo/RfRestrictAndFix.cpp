#include "rf/algo/RfRestrictAndFix.hpp"

#include <chrono>

#include <iomanip>
#include <numeric>

#define COL1W 40
#define COL2W 20

namespace Rf
{

  namespace Algo
  {

    using std::string;
    using std::to_string;

    void RfRestrictAndFix::p(const string &s, int w, std::ostream &o)
    {
      if (verbose)
      {
        if (w != -1)
          o << std::left << std::setw(w) << s;
        else
          o << s;
      }
    }

    void RfRestrictAndFix::pn(const string &s, int w, std::ostream &o)
    {
      if (verbose)
      {
        if (w != -1)
          o << std::left << std::setw(w) << s << std::endl;
        else
          o << s << std::endl;
      }
    }

    void RfRestrictAndFix::solvingSummary()
    {
      pn("");
      pn("All variables fixed");
      p("Solution", COL1W);
      if (isCurrentSolFeasible())
        pn("feasible", COL2W);
      else
        pn("infeasible", COL2W);
      p("Solution value", COL1W);
      pn(to_string(currentSol.getValue()), COL2W);
      if (LRSol.get() != nullptr)
      {
        p("Solution gap LR", COL1W);
        pn(to_string(getGapRelativeLR() * 100) + string(" %"), COL2W);
      }
      p("Total time", COL1W);
      pn(to_string(getTimeTotal()) + string(" s."), COL2W);
      p("Solving LR time", COL1W);
      pn(to_string(getLRTimeSolving()) + string(" s."), COL2W);
      p("Total initializing time", COL1W);
      pn(to_string(getTimeInitTotal()) + string(" s."), COL2W);
      p("Total solving time", COL1W);
      pn(to_string(getTimeSolvingTotal()) + string(" s."), COL2W);
      p("Total fixing time", COL1W);
      p(to_string(getTimeFixingTotal()) + string(" s."), COL2W);
    }

    RfRestrictAndFix::RfRestrictAndFix(RfMod ml, RfSPMode m, RfSolver s, double tl, double rgt, bool srm, bool v)
        : current(),
          currentHasSol(false),
          currentNum(static_cast<size_t>(-1)),
          currentSol(),
          exportLP(false),
          exportLPBaseName("SP_"),
          LRSol(),
          LRTimeSolving(0),
          mode(m),
          model(ml),
          solver(s),
          solverInterface(nullptr),
          solverRebuildModel(srm),
          verbose(v)
    {
      solverParamsD[RfTimeLimit] = tl;
      solverParamsD[RfRelGapTol] = rgt;
    }

    void RfRestrictAndFix::addLocal(RfVarPS &L)
    {
      varsLocals.push_back(L);
    }

    bool RfRestrictAndFix::areAllFixed() const
    {
      RfVarPS V = model.getVars();
      for (auto it = V.begin(); it != V.end(); it++)
        if (varsVals.find(*it) == varsVals.end())
          return false;
      return true;
    }

    void RfRestrictAndFix::createCurrent()
    {
      if (currentNum == static_cast<size_t>(-1))
        currentNum = 0;
      else
        currentNum++;
      auto start = std::chrono::system_clock::now();
      if (currentNum < getCount())
      {
        pn("SP " + to_string(currentNum) + string(" initialization..."));
        current = RfSubpro(model, varsLocals[currentNum], varsGlobal, mode);
      }
      else if (currentNum == getCount())
      {
        pn("SP residual initialization...");
        current = RfSubpro(model, model.getVars(), RfVarPS(), mode);
      }
      else
        return;
      for (auto it = varsVals.begin(); it != varsVals.end(); it++)
      {
        current.fix(it->first, it->second);
      }
      auto end = std::chrono::system_clock::now();
      timesInit.push_back(std::chrono::duration_cast<std::chrono::seconds>(end - start).count());
      p("SP initialization time:\t", COL1W);
      pn(to_string(timesInit[currentNum]) + string(" s."), COL2W);
    }

    void RfRestrictAndFix::createSolverInterface(RfSolvable &ts, bool cr)
    {
      solverInterface = std::make_unique<RfMPSolver>(ts, solver, cr);
    }

    void RfRestrictAndFix::fix(RfVarP var, double val)
    {
      varsVals[var] = val;
    }

    void RfRestrictAndFix::fixVars()
    {
      int c = 0;
      RfVarPS V = current.getVarsLocal();
      for (auto v : V)
      {
        if (current.isVolatile(v))
          continue;
        if (mode == RfRelax && v->isReal())
          continue;
        if (varsVals.find(v) == varsVals.end())
        {
          varsVals[v] = currentSol[v];
          c++;
        }
      }
      p(string("SP vars fixed"), COL1W);
      pn(to_string(c), COL2W);
    }

    size_t RfRestrictAndFix::getCount() const
    {
      return varsLocals.size();
    }

    RfSubpro RfRestrictAndFix::getCurrent() const
    {
      return current;
    }

    size_t RfRestrictAndFix::getCurrentNum() const
    {
      return currentNum;
    }

    RfSol RfRestrictAndFix::getCurrentSol() const
    {
      return currentSol;
    }

    RfVarPDouble RfRestrictAndFix::getFixeds() const
    {
      return varsVals;
    }

    double RfRestrictAndFix::getGapRelative() const
    {
      return gapsRelative.back();
    }

    double RfRestrictAndFix::getGapRelative(size_t i) const
    {
      return gapsRelative[i];
    }

    double RfRestrictAndFix::getGapRelativeLR() const
    {
      return abs(getLRValue() - getValue()) / abs(1e-10 + getValue());
    }

    RfSol RfRestrictAndFix::getLRSol() const
    {
      return LRSol;
    }

    double RfRestrictAndFix::getLRTimeSolving() const
    {
      return LRTimeSolving;
    }

    double RfRestrictAndFix::getLRValue() const
    {
      return LRSol.getValue();
    }

    double RfRestrictAndFix::getSolverParamD(RfParamD p) const
    {
      return solverParamsD.at(p);
    }

    void *RfRestrictAndFix::getSolverRaw()
    {
      return solverInterface->getSolverRaw();
    }

    double RfRestrictAndFix::getTimeFixing() const
    {
      return timesFixing.back();
    }

    double RfRestrictAndFix::getTimeFixing(size_t i) const
    {
      return timesFixing[i];
    }

    double RfRestrictAndFix::getTimeFixingTotal() const
    {
      return std::accumulate(timesFixing.begin(), timesFixing.end(), 0);
    }

    double RfRestrictAndFix::getTimeInit() const
    {
      return timesInit.back();
    }

    double RfRestrictAndFix::getTimeInit(size_t i) const
    {
      return timesInit[i];
    }

    double RfRestrictAndFix::getTimeInitTotal() const
    {
      return std::accumulate(timesInit.begin(), timesInit.end(), 0);
    }

    double RfRestrictAndFix::getTimeSolving() const
    {
      return timesSolving.back();
    }

    double RfRestrictAndFix::getTimeSolving(size_t i) const
    {
      return timesSolving[i];
    }

    double RfRestrictAndFix::getTimeSolvingTotal() const
    {
      return std::accumulate(timesSolving.begin(), timesSolving.end(), 0);
    }

    double RfRestrictAndFix::getTimeTotal() const
    {
      return getLRTimeSolving() + getTimeFixingTotal() + getTimeInitTotal() + getTimeSolvingTotal();
    }

    double RfRestrictAndFix::getTimeTotal(size_t i) const
    {
      return timesFixing[i] + timesInit[i] + timesSolving[i];
    }

    double RfRestrictAndFix::getValue() const
    {
      return currentSol.getValue();
    }

    double RfRestrictAndFix::getValue(RfVarP v) const
    {
      return varsVals.at(v);
    }

    double RfRestrictAndFix::getValue(size_t i) const
    {
      return values[i];
    }

    std::size_t RfRestrictAndFix::getVarsCount() const
    {
      return model.getVarsCount();
    }

    std::size_t RfRestrictAndFix::getVarsFixedCount() const
    {
      return varsVals.size();
    }

    bool RfRestrictAndFix::hasCurrentSol() const
    {
      return currentHasSol;
    }

    bool RfRestrictAndFix::hasSolverRebuildModel() const
    {
      return solverRebuildModel;
    }

    bool RfRestrictAndFix::isCurrentSolFeasible() const
    {
      return model.isFeasible(currentSol);
    }

    void RfRestrictAndFix::setExportLP(bool b, const string &bn)
    {
      exportLP = b;
      exportLPBaseName = bn;
    }

    void RfRestrictAndFix::setFixeds(RfVarPDouble &V)
    {
      varsVals = V;
    }

    void RfRestrictAndFix::setGlobal(RfVarPS &V)
    {
      varsGlobal = V;
    }

    void RfRestrictAndFix::setSolverParamD(RfParamD p, double v)
    {
      solverParamsD[p] = v;
    }

    void RfRestrictAndFix::setSolverRebuildModel(bool b)
    {
      solverRebuildModel = b;
    }

    void RfRestrictAndFix::setSPMode(RfSPMode m)
    {
      mode = m;
    }

    void RfRestrictAndFix::setVerbose(bool b)
    {
      verbose = b;
    }

    bool RfRestrictAndFix::solveAndFix()
    {
      static int i = 0;
      auto start = std::chrono::system_clock::now();
      pn("SP solving...");
      pn("");
      current.init();
      if (solverRebuildModel || solverInterface == nullptr)
        createSolverInterface(current);
      else
        solverInterface->setLR(false);
      for (auto it = solverParamsD.begin(); it != solverParamsD.end(); it++)
        solverInterface->setParamD(it->first, it->second);
      solverInterface->setVerbose(verbose);
      if (solverRebuildModel)
        currentHasSol = solverInterface->solve();
      else
        currentHasSol = solverInterface->solve(current);
      if (exportLP)
      {
        string n = exportLPBaseName + to_string(i++) + string(".lp");
        p("SP exported to ");
        pn(n);
        solverInterface->exportLP(n);
      }
      auto end = std::chrono::system_clock::now();
      timesSolving.push_back(std::chrono::duration_cast<std::chrono::seconds>(end - start).count());
      pn("");
      p("SP solving time", COL1W);
      pn(to_string(timesSolving[currentNum]) + string(" s."), COL2W);
      if (currentHasSol)
      {
        currentSol = solverInterface->getSolution();
        p("SP solution value", COL1W);
        pn(to_string(currentSol.getValue()), COL2W);
        gapsRelative.push_back(solverInterface->getGapRelative());
        values.push_back(solverInterface->getValue());
        start = std::chrono::system_clock::now();
        fixVars();
        end = std::chrono::system_clock::now();
        timesFixing.push_back(std::chrono::duration_cast<std::chrono::seconds>(end - start).count());
        p("Current solution", COL1W);
        if (isCurrentSolFeasible())
          pn("feasible", COL2W);
        else
          pn("infeasible", COL2W);
        p("Total vars fixed", COL1W);
        pn(to_string(getVarsFixedCount()) + "/" + to_string(getVarsCount()), COL2W);
        if (getVarsFixedCount() == getVarsCount())
        {
          solvingSummary();
        }
      }
      else
      {
        pn("SP no solution");
      }
      pn("");
      return currentHasSol;
    }

    bool RfRestrictAndFix::solveAndFixAll(bool s, double tl)
    {
      if (s)
      {
        solveLR(tl);
      }
      while ((currentNum == static_cast<size_t>(-1) || currentNum <= getCount()) && (getVarsCount() != getVarsFixedCount()))
      {
        createCurrent();
        solveAndFix();
        if (!currentHasSol)
        {
          pn("Unsolved");
          return false;
        }
      }
      return true;
    }

    bool RfRestrictAndFix::solveLR(double tl)
    {
      bool solved;
      auto start = std::chrono::system_clock::now();
      pn("Solving model's linear relaxation");
      pn("");
      createSolverInterface(model, true);
      solverInterface->setParamD(RfTimeLimit, tl);
      solverInterface->setVerbose(verbose);
      if (exportLP)
      {
        string n = exportLPBaseName + string("LR") + string(".lp");
        p("LR exported to ");
        pn(n);
        solverInterface->exportLP(n);
      }
      solved = solverInterface->solve();
      pn("");
      if (solved)
      {
        LRSol = solverInterface->getSolution();
        p("LR solution value", COL1W);
        pn(to_string(LRSol.getValue()), COL2W);
      }
      else
      {
        pn("No solution");
      }
      auto end = std::chrono::system_clock::now();
      LRTimeSolving = std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
      p("LR solving time", COL1W);
      pn(to_string(LRTimeSolving) + string(" s."), COL2W);
      pn("");
      return solved;
    }

    void RfRestrictAndFix::unfix(RfVarP v)
    {
      if (varsVals.find(v) != varsVals.end())
      {
        varsVals.erase(varsVals.find(v));
      }
    }

  }  // namespace Algo

}  // namespace Rf
