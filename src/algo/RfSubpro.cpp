#include "rf/algo/RfSubpro.hpp"

namespace Rf
{

  namespace Algo
  {

    RfSubpro::RfSubpro() : subpro(nullptr)
    {
      ;
    }

    RfSubpro::RfSubpro(RfMod ml, RfVarPS Vlocal, RfVarPS Vglobal, RfSPMode m)
        : subpro(std::make_shared<RfSubproblem>(ml, Vlocal, Vglobal, m))
    {
      ;
    }

    RfConPS RfSubpro::createCons()
    {
      return subpro->createCons();
    }

    RfVarPS RfSubpro::createVars()
    {
      return subpro->createVars();
    }

    RfSubpro RfSubpro::fix(RfVarP var, double val)
    {
      subpro->fix(var, val);
      return *this;
    }

    double RfSubpro::getBigM(RfConP c) const
    {
      return subpro->getBigM(c);
    }

    RfConPS RfSubpro::getCons() const
    {
      return subpro->getCons();
    }

    int RfSubpro::getDegree() const
    {
      return subpro->getDegree();
    }

    RfVarPDouble RfSubpro::getFixeds() const
    {
      return subpro->getFixeds();
    }

    double RfSubpro::getLB(RfVarP v) const
    {
      return subpro->getLB(v);
    }

    RfMod RfSubpro::getModel() const
    {
      return subpro->getModel();
    }

    RfExpV RfSubpro::getObj()
    {
      return subpro->getObj();
    }

    RfObjType RfSubpro::getObjType() const
    {
      return subpro->getObjType();
    }

    RfVarType RfSubpro::getType(RfVarP v) const
    {
      return subpro->getType(v);
    }

    double RfSubpro::getRHS(RfConP c) const
    {
      return subpro->getRHS(c);
    }

    double RfSubpro::getUB(RfVarP v) const
    {
      return subpro->getUB(v);
    }

    RfVarPS RfSubpro::getVars() const
    {
      return subpro->getVars();
    }

    RfVarPS RfSubpro::getVarsArtificial() const
    {
      return subpro->getVarsArtificial();
    }

    RfVarPS RfSubpro::getVarsFixed() const
    {
      return subpro->getVarsFixed();
    }

    RfVarPS RfSubpro::getVarsGlobal() const
    {
      return subpro->getVarsGlobal();
    }

    RfVarPS RfSubpro::getVarsLocal() const
    {
      return subpro->getVarsLocal();
    }

    RfVarPS RfSubpro::getVarsRelaxed() const
    {
      return subpro->getVarsRelaxed();
    }

    RfVarPS RfSubpro::getVarsZeroed() const
    {
      return subpro->getVarsZeroed();
    }

    bool RfSubpro::hasCon(RfConP c) const
    {
      return subpro->hasCon(c);
    }

    bool RfSubpro::hasGlobalVar(RfConP c) const
    {
      return subpro->hasGlobalVar(c);
    }

    bool RfSubpro::hasPresentVar(RfConP c) const
    {
      return subpro->hasPresentVar(c);
    }

    bool RfSubpro::hasVar(RfVarP v) const
    {
      return subpro->hasVar(v);
    }

    void RfSubpro::init()
    {
      subpro->init();
    }

    bool RfSubpro::isArtificial(RfVarP v) const
    {
      return subpro->isArtificial(v);
    }

    bool RfSubpro::isComplete(RfConP c) const
    {
      return subpro->isComplete(c);
    }

    bool RfSubpro::isFixed(RfVarP v) const
    {
      return subpro->isFixed(v);
    }

    bool RfSubpro::isGlobal(RfVarP v) const
    {
      return subpro->isGlobal(v);
    }

    bool RfSubpro::isNotEmpty(RfConP c) const
    {
      return subpro->isNotEmpty(c);
    }

    bool RfSubpro::isIncludedWithArtVar(RfConP c) const
    {
      return subpro->isIncludedWithArtVar(c);
    }

    bool RfSubpro::isInt(RfVarP v) const
    {
      return subpro->isInt(v);
    }

    bool RfSubpro::isLocal(RfVarP v) const
    {
      return subpro->isLocal(v);
    }

    bool RfSubpro::isMax() const
    {
      return subpro->isMax();
    }

    bool RfSubpro::isMin() const
    {
      return subpro->isMin();
    }

    bool RfSubpro::isPresent(RfVarP v) const
    {
      return subpro->isPresent(v);
    }

    bool RfSubpro::isRelaxed(RfConP c) const
    {
      return subpro->isRelaxed(c);
    }

    bool RfSubpro::isRelaxed(RfVarP v) const
    {
      return subpro->isRelaxed(v);
    }

    bool RfSubpro::isVolatile(RfVarP v) const
    {
      return subpro->isVolatile(v);
    }

    bool RfSubpro::isZeroed(RfVarP v) const
    {
      return subpro->isZeroed(v);
    }

    RfSubpro RfSubpro::relax(RfConP c)
    {
      subpro->relax(c);
      return *this;
    }

    RfSubpro RfSubpro::relax(RfConPS &C)
    {
      subpro->relax(C);
      return *this;
    }

    RfSubpro RfSubpro::relax(RfVarP v)
    {
      subpro->relax(v);
      return *this;
    }

    RfSubpro RfSubpro::relax(RfVarPS &V)
    {
      subpro->relax(V);
      return *this;
    }

    RfSubpro RfSubpro::setBigM(RfConP c, double b)
    {
      subpro->setBigM(c, b);
      return *this;
    }

    RfSubpro RfSubpro::setLB(RfVarP var, double val)
    {
      subpro->setLB(var, val);
      return *this;
    }

    RfSubpro RfSubpro::setRHS(RfConP c, double r)
    {
      subpro->setRHS(c, r);
      return *this;
    }

    RfSubpro RfSubpro::setUB(RfVarP var, double val)
    {
      subpro->setUB(var, val);
      return *this;
    }

    RfSubpro RfSubpro::setVolatile(RfVarP v)
    {
      subpro->setVolatile(v);
      return *this;
    }

    RfSubpro RfSubpro::setVolatile(RfVarPS &V)
    {
      subpro->setVolatile(V);
      return *this;
    }

    RfSubpro::operator std::string()
    {
      return static_cast<std::string>(*(subpro.get()));
    }

  }  // namespace Algo

}  // namespace Rf
