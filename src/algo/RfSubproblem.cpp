#include "rf/algo/RfSubproblem.hpp"

#include <limits>

namespace Rf
{

  namespace Algo
  {

    RfSubproblem::RfSubproblem(RfMod ml, RfVarPS Vlocal, RfVarPS Vglobal, RfSPMode m)
        : cons(std::make_unique<RfConPS>()),
          consBigMs(std::make_unique<RfConsNums>()),
          consRHSs(std::make_unique<RfConsNums>()),
          consGlobalPartial(std::make_unique<RfConPS>()),
          consGlobalPartialStorage(std::make_unique<RfConL>()),
          consRelaxed(std::make_unique<RfConPS>()),
          LBs(std::make_unique<RfVarPDouble>()),
          mode(m),
          model(ml),
          obj(ml.getObj()),
          UBs(std::make_unique<RfVarPDouble>()),
          varsArtificialBigMs(std::make_unique<RfVarPDouble>()),
          vars(std::make_unique<RfVarPS>()),
          varsArtificialStorage(std::make_unique<RfVarL>()),
          varsArtificial(std::make_unique<RfVarPS>()),
          varsFixed(std::make_unique<RfVarPS>()),
          varsGlobal(std::make_unique<RfVarPS>(Vglobal)),
          varsLocal(std::make_unique<RfVarPS>(Vlocal)),
          varsRelaxed(std::make_unique<RfVarPS>()),
          varsVals(std::make_unique<RfVarPDouble>()),
          varsVolatile(std::make_unique<RfVarPS>()),
          varsZeroed(std::make_unique<RfVarPS>())
    {
      if (mode == RfRelax)
      {
        handleModeRelax();
      }
    }

    RfVarPS RfSubproblem::createZeroedVars(RfConPS &C)
    {
      RfVarPS Z;
      for (auto c : C)
      {
        RfVarPS V = c->getVars();
        for (auto v : V)
        {
          if (!isPresent(v) && !isArtificial(v))
          {
            Z.insert(v);
          }
        }
      }
      RfVarPS V = obj.getVars();
      for (auto v : V)
      {
        if (!isPresent(v) && !isArtificial(v))
        {
          Z.insert(v);
        }
      }
      return Z;
    }

    void RfSubproblem::handleModeRelax()
    {
      RfVarPS V = model.getVars();
      for (auto v : V)
      {
        if (!isPresent(v))
        {
          varsGlobal->insert(v);
          varsRelaxed->insert(v);
        }
      }
    }

    void RfSubproblem::includeWithArtVars(RfConP c)
    {
      RfVarP v1;
      RfVarP v2;
      std::ostringstream oss;
      oss << "av_" << c->getName();
      varsArtificialStorage->emplace_back(oss.str(), RfVarType::RfReal, 0, +RfInfinity);
      v1 = varsArtificialStorage->back().get();
      varsArtificial->insert(v1);
      if (c->getType() == RfEqual)
      {
        oss = std::ostringstream();
        oss << "av2_" << c->getName();
        varsArtificialStorage->emplace_back(oss.str(), RfVarType::RfReal, 0, +RfInfinity);
        v2 = varsArtificialStorage->back().get();
        varsArtificial->insert(v2);
      }
      double bigM = 1e5;
      if (consBigMs->find(c) != consBigMs->end())
        bigM = consBigMs->at(c);
      double coeff = (model.getObjType() == RfMax) ? -bigM : bigM;
      (*varsArtificialBigMs)[v1] = coeff;
      RfCon cArt(c);
      cArt.get()->addArtVar(coeff, v1);
      if (c->getType() == RfEqual)
      {
        coeff = (model.getObjType() == RfMax) ? -bigM : bigM;
        (*varsArtificialBigMs)[v2] = coeff;
        cArt.get()->addArtVar(coeff, v2);
      }
      consGlobalPartialStorage->push_back(cArt);
      consGlobalPartial->insert(cArt.get());
    }

    RfConPS RfSubproblem::createCons()
    {
      varsArtificial->clear();
      varsArtificialStorage->clear();
      consGlobalPartial->clear();
      consGlobalPartialStorage->clear();
      RfConPS allC = model.getCons();
      RfConPS spC;
      for (auto c : allC)
      {
        if (isRelaxed(c))
        {
          continue;
        }
        else if (isComplete(c))
        {
          spC.insert(c);
        }
        else
        {
          if (mode == RfOmit)
          {
            continue;
          }
          if (mode == RfInclude && hasPresentVar(c))
          {
            includeWithArtVars(c);
          }
          if (mode == RfIncludeNoArtVars && hasPresentVar(c))
          {
            spC.insert(c);          
          }
        }
      }
      spC.insert(consGlobalPartial->begin(), consGlobalPartial->end());
      varsZeroed = std::make_unique<RfVarPS>(createZeroedVars(spC));
      return spC;
    }

    RfVarPS RfSubproblem::createVars()
    {
      RfVarPS V;
      V.insert(varsArtificial->begin(), varsArtificial->end());
      V.insert(varsFixed->begin(), varsFixed->end());
      V.insert(varsGlobal->begin(), varsGlobal->end());
      V.insert(varsLocal->begin(), varsLocal->end());
      V.insert(varsZeroed->begin(), varsZeroed->end());
      return V;
    }

    void RfSubproblem::fix(RfVarP var, double val)
    {
      (*varsVals)[var] = val;
      varsFixed->insert(var);
    }

    double RfSubproblem::getBigM(RfConP c) const
    {
      return consBigMs->at(c);
    }

    RfConPS RfSubproblem::getCons() const
    {
      return *cons;
    }

    int RfSubproblem::getDegree() const
    {
      return model.getDegree();
    }

    RfVarPDouble RfSubproblem::getFixeds() const
    {
      return *varsVals;
    }

    double RfSubproblem::getLB(RfVarP v) const
    {
      if (isFixed(v))
      {
        return varsVals->at(v);
      }
      else if (isZeroed(v))
      {
        return 0;
      }
      else if (LBs->find(v) != LBs->end())
      {
        return LBs->at(v);
      }
      else
      {
        return v->getLB();
      }
    }

    RfMod RfSubproblem::getModel() const
    {
      return model;
    }

    RfExpV RfSubproblem::getObj()
    {
      obj.clearArtVars();
      if (varsArtificial->size() == 0)
        return obj;
      for (auto v : *varsArtificial)
      {
        obj.addArtVar(varsArtificialBigMs->at(v), v);
      }
      return obj;
    }

    RfObjType RfSubproblem::getObjType() const
    {
      return model.getObjType();
    }

    RfVarType RfSubproblem::getType(RfVarP v) const
    {
      if (isRelaxed(v))
      {
        return RfVarType::RfReal;
      }
      return v->getType();
    }

    double RfSubproblem::getRHS(RfConP c) const
    {
      if (consRHSs->find(c) != consRHSs->end())
      {
        return consRHSs->at(c);
      }
      else
      {
        return c->getRHS();
      }
    }

    double RfSubproblem::getUB(RfVarP v) const
    {
      if (isFixed(v))
      {
        return varsVals->at(v);
      }
      else if (isZeroed(v))
      {
        return 0;
      }
      else if (UBs->find(v) != UBs->end())
      {
        return UBs->at(v);
      }
      else
      {
        return v->getUB();
      }
    }

    RfVarPS RfSubproblem::getVars() const
    {
      return *vars;
    }

    RfVarPS RfSubproblem::getVarsArtificial() const
    {
      return *varsArtificial;
    }

    RfVarPS RfSubproblem::getVarsFixed() const
    {
      return *varsFixed;
    }

    RfVarPS RfSubproblem::getVarsGlobal() const
    {
      return *varsGlobal;
    }

    RfVarPS RfSubproblem::getVarsLocal() const
    {
      return *varsLocal;
    }

    RfVarPS RfSubproblem::getVarsRelaxed() const
    {
      return *varsRelaxed;
    }

    RfVarPS RfSubproblem::getVarsZeroed() const
    {
      return *varsZeroed;
    }

    bool RfSubproblem::hasGlobalVar(RfConP c) const
    {
      RfVarPS V = c->getVars();
      for (auto v : V)
      {
        if (isGlobal(v))
        {
          return true;
        }
      }
      return false;
    }

    bool RfSubproblem::hasPresentVar(RfConP c) const
    {
      RfVarPS V = c->getVars();
      for (auto v : V)
      {
        if (isPresent(v))
        {
          return true;
        }
      }
      return false;
    }

    bool RfSubproblem::hasCon(RfConP c) const
    {
      return cons->find(c) != cons->end();
    }

    bool RfSubproblem::hasVar(RfVarP v) const
    {
      return (varsArtificial->find(v) != varsArtificial->end()) || (varsFixed->find(v) != varsFixed->end()) ||
             (varsGlobal->find(v) != varsGlobal->end()) || (varsLocal->find(v) != varsLocal->end());
    }

    void RfSubproblem::init()
    {
      cons = std::make_unique<RfConPS>(createCons());
      vars = std::make_unique<RfVarPS>(createVars());
    }

    bool RfSubproblem::isArtificial(RfVarP v) const
    {
      return varsArtificial->find(v) != varsArtificial->end();
    }

    bool RfSubproblem::isComplete(RfConP c) const
    {
      RfVarPS V = c->getVars();
      for (auto v : V)
      {
        if (!isPresent(v) && !isGlobal(v))
        {
          return false;
        }
      }
      return true;
    }

    bool RfSubproblem::isFixed(RfVarP v) const
    {
      return varsFixed->find(v) != varsFixed->end();
    }

    bool RfSubproblem::isGlobal(RfVarP v) const
    {
      return varsGlobal->find(v) != varsGlobal->end();
    }

    bool RfSubproblem::isIncludedWithArtVar(RfConP c) const
    {
      return consGlobalPartial->find(c) != consGlobalPartial->end();
    }

    bool RfSubproblem::isInt(RfVarP v) const
    {
      return v->isInt() && !isRelaxed(v);
    }

    bool RfSubproblem::isLocal(RfVarP v) const
    {
      return varsLocal->find(v) != varsLocal->end();
    }

    bool RfSubproblem::isMax() const
    {
      return getObjType() == RfMax;
    }

    bool RfSubproblem::isMin() const
    {
      return getObjType() == RfMin;
    }

    bool RfSubproblem::isNotEmpty(RfConP c) const
    {
      RfVarPS V = c->getVars();
      for (auto v : V)
      {
        if (isPresent(v))
        {
          return true;
        }
      }
      return false;
    }

    bool RfSubproblem::isPresent(RfVarP v) const
    {
      return isLocal(v) || isGlobal(v) || isFixed(v);
    }

    bool RfSubproblem::isRelaxed(RfConP c) const
    {
      return consRelaxed->find(c) != consRelaxed->end();
    }

    bool RfSubproblem::isRelaxed(RfVarP v) const
    {
      return varsRelaxed->find(v) != varsRelaxed->end();
    }

    bool RfSubproblem::isVolatile(RfVarP v) const
    {
      return varsVolatile->find(v) != varsVolatile->end();
    }

    bool RfSubproblem::isZeroed(RfVarP v) const
    {
      return varsZeroed->find(v) != varsZeroed->end() && !isGlobal(v);
    }

    void RfSubproblem::relax(RfConP c)
    {
      consRelaxed->insert(c);
    }

    void RfSubproblem::relax(RfConPS &C)
    {
      consRelaxed->insert(C.begin(), C.end());
    }

    void RfSubproblem::relax(RfVarP v)
    {
      varsRelaxed->insert(v);
    }

    void RfSubproblem::relax(RfVarPS &V)
    {
      varsRelaxed->insert(V.begin(), V.end());
    }

    void RfSubproblem::setBigM(RfConP c, double b)
    {
      (*consBigMs)[c] = b;
    }

    void RfSubproblem::setLB(RfVarP var, double val)
    {
      (*LBs)[var] = val;
    }

    void RfSubproblem::setRHS(RfConP c, double r)
    {
      (*consRHSs)[c] = r;
    }

    void RfSubproblem::setUB(RfVarP var, double val)
    {
      (*UBs)[var] = val;
    }

    void RfSubproblem::setVolatile(RfVarP v)
    {
      varsVolatile->insert(v);
    }

    void RfSubproblem::setVolatile(RfVarPS &V)
    {
      varsVolatile->insert(V.begin(), V.end());
    }

    RfSubproblem::operator std::string()
    {
      RfConPS C = createCons();
      RfVarPS V = createVars();
      std::ostringstream oss;
      oss << (isMin() ? "minimize: " : "maximize: ");
      oss << static_cast<std::string>(getObj());
      oss << std::endl;
      oss << "subject to:" << std::endl;
      for (auto c : C)
      {
        oss << static_cast<std::string>(*c);
        oss << std::endl;
      }
      for (auto v : V)
      {
        oss << getLB(v) << " <= " << v->getName() << " <= " << getUB(v);
        oss << " " << (isInt(v) ? "int" : "real");
        oss << std::endl;
      }
      return oss.str();
    }

  }  // namespace Algo

}  // namespace Rf
